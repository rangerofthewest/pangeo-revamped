package pangeo.main;

import java.util.Random;

import org.lwjgl.input.Keyboard;


public class EntityLiving extends Entity
{
	//STATIC VARS
	public static Random rand = new Random();
	
	//COMBAT VARS
	public boolean isShieldCrouching = false; //Is the player in defense mode with the shield? (Secondary on shield)
	public boolean isBlockingLeft = false; //Is the player blocking left? (Secondary on left melee weapon)
	public boolean isBlockingRight = false; //Is the player blocking right? (Secondary on right melee weapon)
	public boolean isStealthing = false; //Does the player have a stealth charm active?
	public boolean isHidden = false; //Is the player seen?
	//An assortment of multipliers for combat
	public float meleeMultiplier = 1.0f;
	public float rangedMultiplier = 1.0f;
	public float magicMultiplier = 1.0f;
	public float healingMultiplier = 1.0f;
	public float summonMultiplier = 1.0f;
	public float meleeCrit = 0.04f;
	public float rangedCrit = 0.04f;
	public float magicCrit = 0.04f;
	public float summonCrit = 0.04f;
	
	//NON-COMBAT VARS
	public int maxBreathMeter = 200; //200 ticks --> 10 seconds
	public int currentBreathMeter = 200;
	public boolean isWet = false;
	public boolean isLava = false;
	public boolean canRevive = false;
	public float moveSpeed = 1.0f;
	public boolean canStealth = false;
	public boolean negateStealth = false;
	public boolean noClip = true;
	public boolean jump = false;
	public int jumpCounter = 0;
	public int fallCounter = 0;
	public boolean landedJump = true;
	
	public int health; //health / maxhealth
	public int maxHealth;
	public int mana; //mana / maxmana
	public int maxMana;
	
	public PotionHandler buff = new PotionHandler();
	
	//DEFENSE VALUES (from 0 to 1.0f, 0% to 100%)
	public float defenseStab = 0.0f;
	public float defenseSlash = 0.0f;
	public float defenseProjectile = 0.0f;
	public float defensePierce = 0.0f;
	public float defenseSmash = 0.0f;
	
	//These two variables are for creatures and such
	public Item wieldLeft;
	public Item wieldRight;
	
	//STEALTH
	public float stealth = 1.0f;
	public float stealthFactor = 1.0f;
	public float lightingLevel = 15.0f;
	public float seenBy = 0;
	
	//NORMAL DEFENSE
	public int defense = 0;
	
	public EntityLiving(String n) 
	{
		super(n);
	}
	
	public void calcStealth()
	{
		stealth = (100 - Math.min(((lightingLevel * seenBy) / stealthFactor), 100.0f)) / 100.0f;
	}
	
	public void onUpdate()
	{
		if (health < 1)
		{
			if (canRevive)
			{
				health = 1;
				canRevive = false;
			}
			else
			{
				kill();
			}
		}
		calcStealth();
		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE) && !jump && landedJump && jumpCounter == 0)
		{
			jump = true;
			landedJump = false;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE) && jumpCounter < 30 && jump && !Tile.tiles[Main.world.world[(int)(pos.X) + 1][(int)pos.Y - 1]].solid  && !Tile.tiles[Main.world.world[(int)(pos.X)][(int)pos.Y - 1]].solid )
		{
			jumpCounter++;
			pos.Y -= 0.20f;
		}
		else
		{
			jumpCounter = 0;
			jump = false;
		}
		
		if ((!jump || jumpCounter >= 30) && !Tile.tiles[Main.world.world[(int)(pos.X) + 1][(int)pos.Y + 2]].solid  && !Tile.tiles[Main.world.world[(int)(pos.X)][(int)pos.Y + 2]].solid)
		{
			pos.Y += 0.5f;
			fallCounter++;
		}
		else if (!jump)
		{
			landedJump = true;
			dealFallDmg(fallCounter);
		}
	}
	
	public float calcDefMod(DamageType d)
	{
		switch (d)
		{
			case STAB:
				return this.defenseStab;
			case SMASH:
				return this.defenseSmash;
			case SLASH:
				return this.defenseSlash;
			case PIERCE:
				return this.defensePierce;
			case PROJECTILE:
				return this.defenseProjectile;
			default:
				return 0.0f;
		}
	}
	
	public float calcDefense(int def, float dmg, boolean armorpierce)
	{
		if (!armorpierce)
		{
			int defense = (int)Math.min(def, dmg - 1);
			return Math.min((((float)defense - ((dmg - ((dmg / (dmg - (float)defense))) / (float)defense) / (float)defense)) / (float)defense) * (dmg / (dmg - (float)defense)), dmg - 1);
		}
		else
		{
			return 0.0f;
		}
	}
	//This function is called when the player is hurt via a non-natural cause
	//Gravity param is if the player is falling when they hit someone else
	public void hurtCombat(DamageType damageType, ItemWeapon i, float dmg, boolean stealth, boolean gravity)
	{
		float total_dmg = dmg;
		total_dmg -= calcDefense(this.defense, dmg, i.hitEffect == EnumHitEffect.ARMORNEGATE);
		total_dmg -= (total_dmg * calcDefMod(damageType));
		if ((this.isBlockingLeft || this.isBlockingRight) && damageType != DamageType.PROJECTILE)
		{
			total_dmg -= (total_dmg / 8); //Negate 1/8th of the damage
		}
		if (this.isShieldCrouching && (damageType != DamageType.STAB || rand.nextFloat() > 0.3f))
		{
			if (rand.nextBoolean())
			{
				total_dmg -= (total_dmg / 2); //Negate 1/2th of the damage
			}
			else
			{
				total_dmg = 0;
			}
		}
		if (stealth)
		{
			if (damageType == DamageType.STAB)
			{
				total_dmg *= 2.0f; //double damage if stealthed and you have a stab weapon
			}
			if (damageType == DamageType.SLASH || damageType == DamageType.PROJECTILE)
			{
				total_dmg *= 1.5f; //increase damage by 50% if stealthed and sword or ranged
			}
		}
		if (gravity)
		{
			if (damageType != DamageType.PROJECTILE)
			{
				total_dmg *= 1.25f; //damage is increased by 25% if falling
			}
		}
		//Now we vary it all
		float vary_factor = 0.15f * rand.nextFloat() * (rand.nextBoolean() ? 1 : -1);
		total_dmg += vary_factor * total_dmg;
		health -= total_dmg;
	}
	
	public void kill()
	{
		//code for death
		//...
		//...
		onDeath();
	}
	
	public void onDeath()
	{
		
	}

}
