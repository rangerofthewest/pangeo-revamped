package pangeo.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Map;

public class World 
{
	public static int vegTypes = 11;
	public static int size_x = 27000;
	public static int size_y = 2400;
	public static Random rand = new Random();
	
	public short[][] world = new short[size_x][size_y];
	public byte[][] thickness = new byte[size_x][size_y];
	public byte[][] xSlot = new byte[size_x][size_y];
	public byte[][] ySlot = new byte[size_x][size_y];
	public short[][] wire = new short[size_x][size_y];
	public byte[][] direction = new byte[size_x][size_y];
	public byte[][] slopeStyle = new byte[size_x][size_y];
	public short[][] wall = new short[size_x][size_y];
	public short[][] liquid = new short[size_x][size_y];
	public double[][] voltage = new double[size_x][size_y];
	//has 256 data states:
	//0 is red wire (off)
	//1 is red wire (on)
	//2 is blue wire (off)
	//3 is blue wire (on)
	//...and so on
	public Color[][] paintColor = new Color[size_x][size_y];
	
	public Map<String, WorldTask> oreTable = new HashMap<String, WorldTask>(); //This table contains all the world generator tasks
	
	public void init()
	{
		//Set up the ore tables
		System.out.println("Calculating world gen tasks...");
		
		oreTable.put("GenCopperSurface", new WorldTask(3, 9, 4, 800, 1230, 250, this));
		oreTable.put("GenStone", new WorldTask(6, 18, 3, 800, 1730, 850, this));
		oreTable.put("GenDirtDeep", new WorldTask(6, 24, 1, 1100, 2200, 755, this));
		oreTable.put("GenAirPocket", new WorldTask(10, 30, 0, 850, 2200, 2055, this));
		oreTable.put("GenSmallCave", new WorldTask(70, 270, 0, 825, 2200, 3255, this));
		oreTable.put("GenMediumCave", new WorldTask(200, 670, 0, 825, 2200, 4555, this));
		oreTable.put("GenLargeCave", new WorldTask(700, 1470, 0, 825, 2200, 7555, this));
		oreTable.put("GenExtraLargeCave", new WorldTask(1700, 2470, 0, 800, 2200, 8555, this));
		oreTable.put("GenUltraLargeCave", new WorldTask(3500, 6770, 0, 800, 2200, 16555, this));
		oreTable.put("GenIron", new WorldTask(4, 9, 5, 810, 1290, 975, this));
		oreTable.put("GenCoal", new WorldTask(3, 16, 10, 810, 1590, 775, this));
		oreTable.put("GenSilver", new WorldTask(3, 10, 6, 860, 1350, 1280, this));
		oreTable.put("GenMegaIron", new WorldTask(8, 19, 5, 840, 1330, 1415, this));
		oreTable.put("GenMegaSilver", new WorldTask(7, 20, 6, 880, 1410, 1935, this));
		oreTable.put("GenGold", new WorldTask(5, 28, 7, 860, 1990, 1960, this));
		
		try 
		{
			genWorld();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void placeTile(Tile t, int x, int y)
	{
		t.onPlaceTile(x, y, this);
	}
	//Now we do the world generator task
	public void genWorld() throws FileNotFoundException, UnsupportedEncodingException
	{
		//0- Air
		//1- Dirt
		//2- Stone
		//3- Grass
		//4- Copper
		//5- Iron
		//6- Silver
		//7- Gold
		//8- Ash
		//9- Soot
		//10- Coal
		//15, 16, 17, 18, 19, 20, 21, 22- Vegetation
		//23- Sand
		for (int x = 0; x < size_x; x++)
		{
			for (int y = 0; y < size_y; y++)
			{
				slopeStyle[x][y] = 0; //Typical
				xSlot[x][y] = 0; 
				ySlot[x][y] = 0;
				wire[x][y] = -1; //No wire
				thickness[x][y] = 16; //typical thickness
				wall[x][y] = -1;
				liquid[x][y] = -1;
				direction[x][y] = -1; 
				voltage[x][y] = 0.0;
				if (y <= size_y / 3)
				{
					world[x][y] = 0; //Air
				}
				if (y > size_y / 3 && y <= (size_y / 3) + (size_y / 8))
				{
					world[x][y] = 1; //Dirt
				}
				if (y > (size_y / 3) + (size_y / 8))
				{
					world[x][y] = 2; //Stone
				}
			}
			System.out.println("Initializing world: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		//Now we add grass
		for (int x = 0; x < size_x; x++)
		{
			world[x][size_y / 3] = 3; //Grass
			System.out.println("Grassifying: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		//Holes are next, adding some texture to the terrain
		for (int x = 0; x < size_x; x++)
		{
			if (rand.nextInt(6) == 0)
			{
				if (rand.nextBoolean())
				{
					world[x][(size_y / 3) + 1] = 0; 
				}
				else
				{
					world[x][(size_y / 3) - 1] = 3;
					world[x][(size_y / 3)] = 1; 
				}
			}
			System.out.println("Making Divots and Bumps: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		//Now we create some raised portions
		boolean flagElevate = false;
		for (int x = 0; x < size_x; x++)
		{
			if (rand.nextInt(6) == 0)
			{
				flagElevate = !flagElevate;
			}
			if (flagElevate)
			{
				world[x][(size_y / 3) - 1] = 3;
				world[x][(size_y / 3)] = 1; 
			}
			System.out.println("Making Raised Terrain: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		//Generate some blobs of stone on surface
		for (int x = 0; x < size_x; x++)
		{
			if (rand.nextInt(27) == 0)
			{
				new WorldBlob(8, 27, 2, -1).gen(this, x, (size_y / 3));
			}
			System.out.println("Making Stone Blobs: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		//After generating stone blobs, do the ore tables!
		boolean executed = false;
		for (int x = 0; x < size_x; x++)
		{
			for (int y = 0; y < size_y; y++)
			{
				for(Entry<String, WorldTask> e : oreTable.entrySet()) 
				{
			        WorldTask value = e.getValue();
			        if (y >= value.min_depth && y <= value.max_depth && rand.nextInt(value.weight) == 0 && !executed)
			        {
			        	//System.out.println("Executing task " + e.getKey() + "!");
			        	value.execute(x, y);
			        	executed = true;
			        }
			        if (executed)
			        {
			        	break;
			        }
			    }
				executed = false;
			}
			System.out.println("WorldGen Tasks: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		for (int x = 1; x < size_x - 1; x++)
		{
			for (int y = 810; y < size_y - 275; y++)
			{
				if (world[x + 1][y] == 0 && world[x - 1][y] == 0 && world[x][y + 1] == 0 && world[x][y - 1] == 0)
				{
					world[x][y] = 0;
				}
			}
			System.out.println("Smoothing undeworld: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		//Now that we did the world gen tasks, add some terrain variance... for real
		for (int x = 0; x < size_x; x++)
		{
			for (int y = 0; y < size_y; y++)
			{
				if (y < 805 && y > 801)
				{
					if (rand.nextInt(4) == 0)
					{
						if (rand.nextInt(2) == 0)
						{
							world[x][y] = 0;
						}
						else
						{
							world[x][y] = 1;
						}
					}
				}
			}
			System.out.println("Scrambling Terrain: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		//All world generators here
		
		new WorldHell(2250).generate(this); //Start generating hell at 2250 Y
		for (int x = 0; x < size_x; x++)
		{
			int y = 800;
			while (world[x][y] != 0 && y > 730)
			{
				y--;
			}
			while (world[x][y + 1] != 1 && world[x][y + 1] != 3 && world[x][y + 1] < 900)
			{
				y++;
			}
			if (world[x][y + 1] == 1 || world[x][y + 1] == 3)
			{
				world[x][y + 1] = (short) (15 + rand.nextInt(World.vegTypes));
			}
			System.out.println("Adding Vegetation: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		for (int x = 0; x < size_x; x++)
		{
			for (int y = 0; y < size_y; y++)
			{
				if (world[x][y] >= 15 && world[x][y] <= 15 + (World.vegTypes - 1))
				{
					if (world[x][y + 1] != 1 && world[x][y + 1] != 3)
					{
						world[x][y] = 0;
					}
				}
			}
			System.out.println("Trimming Vegetation: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		for (int x = 0; x < size_x; x++)
		{
			for (int y = 800; y < size_y; y++)
			{
				if (rand.nextFloat() <= 0.85f)
				{
					setSlope(x, y);
				}
			}
			System.out.println("Adding slopes: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		for (int x = 0; x < size_x; x++)
		{
			for (int y = 790; y < size_y - 250; y++)
			{
				if (isValidDecoration(x, y + 1) && rand.nextBoolean())
				{
					world[x][y] = (short) (26 + rand.nextInt(Tile.decTypes));
				}
			}
			System.out.println("Adding terrain decoration: " + ((((float)x + 1) / size_x) * 100) + "%");
		}
		
		//Tree gen
		//Now for saving
		//saveWorld("world1.dat");
	}
	
	public void explode(int x, int y, int radius)
	{
		for (int i = (x - (radius / 2)); i < radius; i++)
		{
			for (int j = (y - (radius / 2)); j < radius; j++)
			{
				if (Math.sqrt(Math.pow((x - i), 2) + Math.pow((y - j), 2)) <= radius)
				{
					world[i][j] = 0;
				}
			}
		}
	}
	
	public boolean isDecoration(int x, int y)
	{
		return (world[x][y] >= 26 && world[x][y] < 26 + Tile.decTypes);
	}
	//measures if the space above is a valid spot for a decoration
	public boolean isValidDecoration(int x, int y)
	{
		return (Tile.tiles[world[x][y]].solid && world[x][y - 1] == 0 && !isVegetation(x, y)  && !isDecoration(x, y) && slopeStyle[x][y] != 3 && slopeStyle[x][y] != 4);
	}
	
	public boolean isVegetation(int x, int y)
	{
		return (world[x][y] >= 15 && world[x][y] < 15 + vegTypes);
	}
	
	public void setSlope(int x, int y)
	{
		try
		{
			if (rand.nextInt(1) == 0 && !isVegetation(x, y) && Tile.tiles[world[x][y]].canSlope)
			{
				if (world[x][y - 1] != 0 && world[x - 1][y] == 0 && world[x + 1][y] != 0 && world[x][y + 1] == 0)
				{
					slopeStyle[x][y] = 1;
				}
				if (world[x][y - 1] != 0 && world[x - 1][y] != 0 && world[x + 1][y] == 0 && world[x][y + 1] == 0)
				{
					slopeStyle[x][y] = 2;
				}
				//the up-slopes are only if there is no "vegetation"
				if (!isVegetation(x, y - 1))
				{
					if (world[x][y - 1] == 0 && world[x - 1][y] != 0 && world[x + 1][y] == 0 && world[x][y + 1] != 0)
					{
						slopeStyle[x][y] = 4;
					}
					if (world[x][y - 1] == 0 && world[x - 1][y] == 0 && world[x + 1][y] != 0 && world[x][y + 1] != 0)
					{
						slopeStyle[x][y] = 3;
					}
				}
			}
		}
		catch (Exception e)
		{
			slopeStyle[x][y] = 0;
		}
	}
	
	public void genTree(int x, int y)
	{
		int h = rand.nextInt(16) + 8;
		for (int a = 0; a < h; a++)
		{
			world[x][y - a] = 26;
		}
		x = Math.min(Math.min(x, 50), size_x - 50);
		int i = x;
		int j = y - h - 1;  
		world[i][j] = 27;
		world[i + 1][j] = 27;
		world[Math.min(0, i - 1)][j] = 27;
		world[i][j - 1] = 27;
		for (int k = 0; k < rand.nextInt(20) + 18; k++)
		{
			if (rand.nextBoolean())
			{
				if (rand.nextBoolean())
				{
					i++;
				}
				else
				{
					j++;
				}
			}
			else
			{
				if (rand.nextBoolean())
				{
					i--;
				}
				else
				{
					j--;
				}
			}
			world[Math.max(0, i)][j] = 27;
		}
	}
	int sand_counter = 0;
	public void checkChunks(int x, int y)
	{
		sand_counter++;
		for (int i = x - 100; i < x + 100; i++)
		{
			for (int j = y - 100; j < y + 100; j++)
			{ 
				if (i >= 0 && i < size_x && j >= 0 && j < size_y)
				{
					if (Tile.tiles[world[i][j]].gravity && !Tile.tiles[world[i][j + 1]].solid && sand_counter > 4)
					{
						world[i][j + 1] = world[i][j];
						world[i][j] = 0;
					}
					if (isDecoration(i, j) && !isValidDecoration(i, j + 1))
					{
						world[i][j] = 0;
					}
				}
			}
		}
		sand_counter = 0;
	}
	public void saveWorld(String name)
	{
		File dir = new File(name);
		dir.mkdir();
		File save = new File(name + "/" + name);
		try {
			save.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter writer = null;
		PrintWriter writer2 = null;
		PrintWriter writer3 = null;
		try {
			writer = new PrintWriter(name + "/" + name, "UTF-8");
			writer2 = new PrintWriter(name + "/" + "thick_" + name, "UTF-8");
			writer3 = new PrintWriter(name + "/" + "wire_" + name, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int y = 0; y < size_y; y++)
		{
			for (int x = 0; x < size_x; x++)
			{
				writer.print(world[x][y] + "\n");
				writer2.print(thickness[x][y] + "\n");
				writer3.print(wire[x][y] + "\n");
			}
			//\n
			writer.print("\n");
			System.out.println("Saving World: " + ((((float)y + 1) / size_y) * 100) + "%");
		}
		System.out.println("RAM Used: " + (((float)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())) / 1024 / 1024) + " MB");
		writer.close();
		writer2.close();
	}
}
