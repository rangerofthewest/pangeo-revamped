package pangeo.main;

public class ItemSpell extends ItemWeapon 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7298840024996330148L;
	public int manaUse;
	public int projectileId;
	public boolean consumable = false;

	public ItemSpell(String s, int d, int m, int p) 
	{
		super(s, d);
		manaUse = m;
		projectileId = p;
	}
	
	public ItemSpell setConsumable(boolean c)
	{
		consumable = c;
		return this;
	}
}
