package pangeo.main;

public class ItemAmmo extends Item
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8266038776897137812L;
	public EnumWeapon weaponType;
	public int damage;
	public ItemAmmo(String s, int d, EnumWeapon w) 
	{
		super(s);
		weaponType = w;
		damage = d;
		// TODO Auto-generated constructor stub
	}
}
