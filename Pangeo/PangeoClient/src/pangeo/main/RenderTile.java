package pangeo.main;

public enum RenderTile 
{	
	noBorder(16, 16, "No Border"),
	leftBorder(0, 16, "Left Border"),
	rightBorder(32, 16, "Right Border"),
	topBorder(16, 0, "Top Border"),
	bottomBorder(16, 32, "Bottom Border"),
	topLeftBorder(0, 0, "Top Left Border"),
	topRightBorder(32, 0, "Top Right Border"),
	bottomLeftBorder(0, 32, "Bottom Left Border"),
	bottomRightBorder(32, 32, "Bottom Right Border"),
	allBorder(16, 48, "All Border"),
	slopeDownRight(32, 64, "Slope Border"),
	slopeDownLeft(0, 64, "Slope Border"),
	slopeUpRight(32, 80, "Slope Border"),
	slopeUpLeft(0, 80, "Slope Border");
	
	private float xPos = 0;
	private float yPos = 0;
	private String renderName = "";
	
	private RenderTile(float x, float y, String name)
	{
		xPos = x;
		yPos = y;
		renderName = name;
	}
	
	public String getName()
	{
		return renderName;
	}
	
	public float getX()
	{
		return xPos;
	}
	public float getY()
	{
		return yPos;
	}
}
