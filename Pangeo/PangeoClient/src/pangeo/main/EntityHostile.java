package pangeo.main;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.Color;

public class EntityHostile extends EntityLiving implements Cloneable {
	// A hostile creature has certain attributes to it
	public boolean attrScent = false; // is this creature immune to stealthing?
	public boolean attrTameable = false; // is this creature tameable?
	public boolean attrStealth = false; // can this creature stealth?

	public float tameChance = 0.1f;
	public EntityPlayer owner = null;

	public Texture[] animation;
	public int frameCount;
	public int currentFrame = 0;

	public Map<String, AI> tasks = new HashMap<String, AI>();

	public EntityHostile(String n, String dir, int frames) {
		super(n);

		frameCount = frames;
		animation = new Texture[frames];

		for (int x = 0; x < frames; x++) {
			animation[frames] = Content.loadTexture(dir);
		}
	}

	public boolean checkTame() {
		if (attrTameable && World.rand.nextFloat() <= tameChance) {
			return true;
		}
		return false;
	}

	public void onUpdate() {

	}

	public void render() {
		if (pos.X <= Main.render_x + 41 && pos.X >= Main.render_x - 41
				&& pos.Y <= Main.render_y + 25 && pos.Y >= Main.render_y - 25) {
			Main.draw(animation[currentFrame],
					624 + (int) (Main.render_x - pos.X),
					364 + (int) (Main.render_y - pos.Y), Color.white);
		}
	}
}
