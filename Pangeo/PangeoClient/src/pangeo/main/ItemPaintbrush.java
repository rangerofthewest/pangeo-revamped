package pangeo.main;

public class ItemPaintbrush extends Item
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6572109450709600020L;
	public int paint_width = 1;
	public int paint_height = 1;
	public float paint_opacity = 0.8f;
	
	public ItemPaintbrush(String s, int w, int h, float o) 
	{
		super(s);
		paint_width = w;
		paint_height = h;
		paint_opacity = o;
	}

}
