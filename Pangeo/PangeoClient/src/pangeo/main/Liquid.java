package pangeo.main;

import org.newdawn.slick.opengl.Texture;

public class Liquid {
	public String name;
	public short id;
	public Texture t, t2;
	public double lightIntensity = 0;

	public static Liquid[] liquids = new Liquid[5000];

	public Liquid(int i, String s, double l) {
		id = (short) i;
		name = s;
		lightIntensity = l;
	}

	public static void init() {
		liquids[0] = new Liquid(0, "Water", 0.0);
		liquids[1] = new Liquid(1, "Lava", 3.8);
		liquids[2] = new Liquid(2, "Oil", 0.0);
		liquids[3] = new Liquid(3, "Poison", 0.0);
		liquids[4] = new Liquid(4, "Honey", 0.0);
		liquids[5] = new Liquid(5, "Blood", 0.0);
	}
	
	public static void calcPhysics(int x, int y, World w)
	{
		if (w.world[x][y + 1] == 0 && w.liquid[x][y + 1] == -1)
		{
			w.liquid[x][y + 1] = w.liquid[x][y];
			w.liquidDepth[x][y + 1] = w.liquidDepth[x][y];
			w.liquid[x][y] = -1;
			w.liquidDepth[x][y] = 0;
		}
		/*
		else if (w.world[x][y + 1] == 0 && w.liquid[x][y + 1] == w.liquid[x][y])
		{
			w.liquidDepth[x][y + 1] += w.liquid[x][y];
			if (w.liquidDepth[x][y + 1] > 15)
			{
				w.liquidDepth[x][y + 1] = 15;
			}
			w.liquidDepth[x][y] -= 15;
			if (w.liquidDepth[x][y] < 1)
			{
				w.liquidDepth[x][y] = 0;
				w.liquid[x][y] = -1;
			}
		}
		*/
		else
		{
			Util.print("Calculating liquid differential at " + x + "/" + y);
			int totalDepth = w.liquidDepth[x][y];
			int lowerDiff = 0;
			int higherDiff = 0;
			for (int a = 0; a < 8; a++)
			{
				if (!Tile.tiles[w.world[x + a][y]].solid && w.liquid[x + a][y] == -1)
				{
					totalDepth--;
					higherDiff = a;
				}
				else
				{
					break;
				}
			}
			for (int a = 0; a < 8; a++)
			{
				if (!Tile.tiles[w.world[x - a][y]].solid && w.liquid[x - a][y] == -1)
				{
					totalDepth--;
					lowerDiff = -a;
				}
				else
				{
					break;
				}
			}
			for (int a = lowerDiff; a < higherDiff; a++)
			{
				w.liquid[x + a][y] = w.liquid[x][y];
				w.liquidDepth[x + a][y] = (byte) totalDepth;
			}
			Util.print("Differentials at " + x + "/" + y + ": " + lowerDiff + "/" + higherDiff);
		}
	}
}
