package pangeo.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TileBook extends Tile {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4053018921504697011L;
	public String text = "";
	public String textPath;

	public TileBook(String s, int i, String title, String author) {
		super(title, i);
		this.setBook(true);
		this.setTitle(title);
		this.setAuthor(author);
		this.setSolid(false);
		this.setLore("A mysterious leatherbound object with runes in it...");
		this.spriteId = 65;
		this.textPath = "assets/books/b" + i + ".txt";
		if (new File(textPath).exists()) {
			readFile();
		}
	}

	public void onActivate(int x, int y, World w, EntityPlayer p) {
		// read the book here
	}

	public void readFile() {
		BufferedReader br = null;

		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(textPath));

			while ((sCurrentLine = br.readLine()) != null) {
				this.text += sCurrentLine;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		Util.print(text);
	}
}
