package pangeo.main;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.Color;

public class EntityProjectile extends Entity 
{
	public Texture tex;
	public Vector2 pos, startpos;
	public int timeAlive = 0;
	public float motionX, sMotionX;
	public float motionY, sMotionY;
	public boolean falling = false;
	public boolean gravity = true;
	public double rotation = 0.0;
	public boolean active = true;
	
	public EntityProjectile(String n)
	{
		super(n);
		active = false;
	}
	
	public EntityProjectile(String n, int startX, int startY, float angle) 
	{
		super(n);
		tex = Main.coin;
		pos = new Vector2(startX, startY);
		startpos = pos;
		rotation = angle;
		motionX = (float) Math.cos((double)(angle * (180 / Math.PI)));
		motionY = (float) Math.sin((double)(angle * (180 / Math.PI)));
	}
	
	public EntityProjectile(String n, int startX, int startY, int targetX, int targetY)
	{
		super(n);
		tex = Main.coin;
		pos = new Vector2(startX, startY);
		startpos = pos;
		double deltaX = targetX - startX;
		double deltaY = targetY - startY;
		if (deltaX == 0)
		{
			deltaX = 0.000001;
		}
		double angleInDegrees = (Math.atan(deltaY / deltaX) * 180 / Math.PI);
		rotation = angleInDegrees;
		if (targetX < startX)
		{
			angleInDegrees += 180;
		}
		motionX = (float) Math.cos(angleInDegrees * (Math.PI / 180));
		motionY = (float) Math.sin(angleInDegrees * (Math.PI / 180));
		Util.print("Projectile created: " + angleInDegrees + " degrees, " + motionX + "/" + motionY + " velocity");
	}
	
	public EntityProjectile(String n, float startX, float startY, float targetX, float targetY)
	{
		super(n);
		tex = Main.coin;
		pos = new Vector2(startX, startY);
		startpos = pos;
		double deltaX = targetX - startX;
		double deltaY = targetY - startY;
		if (deltaX == 0)
		{
			deltaX = 0.000001;
		}
		double angleInDegrees = (Math.atan(deltaY / deltaX) * 180 / Math.PI);
		rotation = angleInDegrees;
		if (targetX < startX)
		{
			angleInDegrees += 180;
		}
		motionX = (float) Math.cos(angleInDegrees * (Math.PI / 180));
		motionY = (float) Math.sin(angleInDegrees * (Math.PI / 180));
		sMotionX = motionX;
		sMotionY = motionY;
		Util.print("Projectile created: " + angleInDegrees + " degrees, " + motionX + "/" + motionY + " velocity");
	}
	
	public EntityProjectile setTexture(Texture t)
	{
		tex = t;
		return this;
	}
	
	public EntityProjectile setGrav(boolean b)
	{
		gravity = b;
		return this;
	}
	
	public EntityProjectile setVelocity(float x, float y)
	{
		this.motionX = x;
		this.motionY = y;
		return this;
	}
	
	public void onUpdate() 
	{
		if (!active)
		{
			return;
		}
		timeAlive++;
		if (gravity)
		{
			//motionY = (float)0.97 * motionY;
			if (!falling)
			{
				motionY = (float)0.91  * motionY;
				motionX = (float)0.96 * motionX;
				falling = (motionY < sMotionY / 30 && sMotionY > 0) || (motionY > sMotionY / 30 && sMotionY < 0);
			}
			else
			{
				motionY = (float)Math.abs(sMotionY - (float)((1.03 * (sMotionY / Math.abs(sMotionY))) + ((motionX - motionY) * 0.09)) * motionY) + (motionY / 8);
				motionX = (float)1.0004 * motionX;
			}
			//motionY = (((-4.9 * Math.pow(timeAlive, 2)) + (19.6 * timeAlive)) * (pos.Y - startpos.Y));
		}
		pos.X += motionX;
		pos.Y += motionY;
	}
	
	public void render()
	{
		Main.draw(tex, 640 + (int)Math.floor((pos.X - Main.player.pos.X) * 16), 380 + (int)Math.floor((pos.Y - Main.player.pos.Y) * 16), Color.white); //, rotation);
	}
}
