package pangeo.main;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.*;

import com.google.gson.Gson;

public class Util 
{
	public static Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

	public static Object deepCopy(Object oldObj) throws Exception
	{
		ObjectOutputStream oos = null;
		ObjectInputStream ois = null;
		try
		{
			ByteArrayOutputStream bos = 
					new ByteArrayOutputStream(); // A
					oos = new ObjectOutputStream(bos); // B
					// serialize and pass the object
					oos.writeObject(oldObj);   // C
					oos.flush();               // D
					ByteArrayInputStream bin = 
							new ByteArrayInputStream(bos.toByteArray()); // E
					ois = new ObjectInputStream(bin);                  // F
					// return the new object
					return ois.readObject(); // G
		}
		catch(Exception e)
		{
			System.out.println("Exception in ObjectCloner = " + e);
			throw(e);
		}
		finally
		{
			oos.close();
			ois.close();
		}
	}
	@SuppressWarnings("unchecked")
	public static  <T> T cloneThroughJson(T t) {
		Gson gson = new Gson();
		String json = gson.toJson(t);
		return (T) gson.fromJson(json, t.getClass());
	}

	public static void init()
	{
	}

	public static void setClipboard(String s)
	{
		clipboard.setContents(new StringSelection(s), null);
		Util.print("Clipboard set to " + s);
	}

	public static void print(String s)
	{
		System.out.println("sysmsg - " + s);
	}

<<<<<<< HEAD
    public static String MD5(String text) 
    { 
        MessageDigest md;
        try {
			md = MessageDigest.getInstance("MD5");
			byte[] md5 = new byte[64];
	        try {
				md.update(text.getBytes("iso-8859-1"), 0, text.length());
		        md5 = md.digest();
		        return convertedToHex(md5);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    } 
  
=======
	public static void error(String s)
	{
		System.out.println("error - " + s);
	}
>>>>>>> origin/rangerofthewest
}
