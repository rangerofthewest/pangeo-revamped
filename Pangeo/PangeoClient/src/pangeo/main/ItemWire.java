package pangeo.main;

public class ItemWire extends Item 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8858034652816515792L;
	public short placeId;
	
	public ItemWire(String s, int id, boolean special) 
	{
		super(s);
		if (!special)
		{
			placeId = (byte) Math.min(id, 127);
		}
		else
		{
			placeId = (byte) id;
		}
		this.isMech = true;
	}
	
	public void place(World w, int x, int y)
	{
		if (id <= 127)
		{
			w.wire[x][y] = (byte)(placeId * 2);
		}
		else
		{
			w.wire[x][y] = placeId;
		}
	}
}
