package pangeo.main;

public class ItemHealing extends Item {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5883845272700249147L;
	public int healing;

	public ItemHealing(String s, int h) {
		super(s);
		healing = h;
	}
}
