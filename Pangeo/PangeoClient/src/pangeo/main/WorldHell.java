package pangeo.main;

public class WorldHell extends WorldGenerator
{
	public int y_pos;
	
	public WorldHell(int a)
	{
		y_pos = a;
	}
	
	public void generate(World w)
	{
		//We first clear out hell and add an ash and soot ceiling
		for (int x = 0; x < w.size_x; x++)
		{
			for (int y = y_pos; y < w.size_y; y++)
			{
				if (y < y_pos + 5)
				{
					w.world[x][y] = (short) (8 + World.rand.nextInt(2)); //Generate an ash or soot block
				}
				else
				{
					w.world[x][y] = 0;
				}
				w.biome[x][y] = Biome.Hell;
			}
			Util.print("Clearing hell: " + ((((float)x + 1) / w.size_x) * 100) + "%");
		}
		//Hells cleared, so let's init a new gen task to create blobs of ash and soot
		WorldTask ashGen = new WorldTask(50, 160, 8, y_pos, w.size_y - 10, 600, w);
		WorldTask sootGen = new WorldTask(40, 170, 9, y_pos, w.size_y - 10, 720, w);
		for (int x = 0; x < w.size_x; x++)
		{
			for (int y = y_pos; y < w.size_y; y++)
			{
				if (World.rand.nextBoolean())
				{
					if (World.rand.nextInt(ashGen.weight) == 0)
					{
						ashGen.execute(x, y);
					}
				}
				else
				{
					if (World.rand.nextInt(sootGen.weight) == 0)
					{
						sootGen.execute(x, y);
					}
				}
			}
			Util.print("Creating ash islands: " + ((((float)x + 1) / w.size_x) * 100) + "%");
		}
		//Now we fill the bottom half of hell with lava
		int halfway = y_pos + (int)((float)(w.size_y - y_pos) / 2);
		for (int x = 0; x < w.size_x; x++)
		{
			for (int y = halfway; y < w.size_y; y++)
			{
				if (w.world[x][y] == 0)
				{
					w.liquid[x][y] = 1;
					w.liquidDepth[x][y] = 15;
				}
			}
			Util.print("Generating lava: " + ((((float)x + 1) / w.size_x) * 100) + "%");
		}
	}
	
	public int genPriority()
	{
		return 0; //highest priority
	}
}
