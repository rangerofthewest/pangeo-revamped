package pangeo.main;

public class TileBed extends Tile {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2707874547972116874L;

	public TileBed(String s, int i) {
		super(s, i);
	}

	public void onActivate(int x, int y, World w, EntityPlayer p) {
		p.spawnPoint = new Vector2(x, y);
	}
}
