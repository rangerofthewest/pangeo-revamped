package pangeo.main;

public class Wire
{
	public static String[] textures = 
		{
			"textures/wire/Wire_Red",
			"textures/wire/Wire_Green",
			"textures/wire/Wire_Purple",
			"textures/wire/Wire_Orange",
			"textures/wire/Wire_Blue",
			"textures/wire/Wire_Yellow",
			"textures/wire/Wire_Brown",
			"textures/wire/Wire_Black",
			"textures/wire/Wire_White",
			"textures/wire/Wire_Rainbow",
			"textures/wire/Cable",
			"textures/wire/Black_Cable",
			"textures/wire/Grey_Cable"
		};
	public static boolean isActive(int x, int y, World w)
	{
		return w.wire[x][y] % 2 != 0;
	}
	
	public static void toggleActive(int x, int y, World w)
	{
		if (isValid(x, y, w))
		{
			if (w.wire[x][y] >= 0 && w.wire[x][y] % 2 == 0)
			{
				w.wire[x][y]++;
				return;
			}
			if (w.wire[x][y] >= 0 && w.wire[x][y] % 2 != 0)
			{
				w.wire[x][y]--;
				return;
			}
		}
	}
	
	public static boolean isValid(int x, int y, World w)
	{
		if (w.wire[x][y] < 0 || x < 0 || y < 0  || x > w.size_x - 1 || y > w.size_y - 1)
		{
			return false;
		}
		return true;
	}
	
	public static boolean currentNull(int x, int y, World w)
	{
		return w.world[x][y] != 13;
	}
	public static void triggerActive(int x, int y, World w, double volt)
	{
		//w.voltage[x][y] = (double)volt;
		if (isActive(x, y, w))
		{
			//w.voltage[x][y] = 0;
		}
		else
		{
			if (volt >= 0.05)
			{
				triggerActive(x, y, w);
			}
			else
			{
				//w.voltage[x][y] = 0;
			}
		}
	}
	
	public static boolean isCable(int x, int y, World w)
	{
		if (w.wire[x][y] >= 10 && w.wire[x][y] <= 12)
		{
			return true;
		}
		return false;
	}
	
	public static void triggerActive(int x, int y, World w)
	{
		/*
		if (isValid(x, y, w))
		{
			toggleActive(x, y, w);
			if (Math.floor((double)w.wire[x - 1][y] / 2) == w.wire[x][y] || (w.wire[x - 1][y] == 300 && w.wire[x - 1][y] != 301 && !(w.wire[x][y] >= 10 && w.wire[x][y] <= 12)) || (w.wire[x - 1][y] == 302 && (w.wire[x][y] >= 10 && w.wire[x][y] <= 12)))
			{
				if (isCable(x, y, w))
				{
					triggerActive(x - 1, y, w, w.voltage[x][y] - 0.025);
				}
				else
				{
					triggerActive(x - 1, y, w, w.voltage[x][y] - 0.05);
				}
			}
			if (Math.floor((double)w.wire[x + 1][y] / 2) == w.wire[x][y] || (w.wire[x + 1][y] == 300 && w.wire[x + 1][y] != 301 && !(w.wire[x][y] >= 10 && w.wire[x][y] <= 12)) || (w.wire[x + 1][y] == 302 && (w.wire[x][y] >= 10 && w.wire[x][y] <= 12)))
			{
				if (isCable(x, y, w))
				{
					triggerActive(x + 1, y, w, w.voltage[x][y] - 0.025);
				}
				else
				{
					triggerActive(x + 1, y, w, w.voltage[x][y] - 0.05);
				}
			}
			if (Math.floor((double)w.wire[x][y + 1] / 2) == w.wire[x][y] || (w.wire[x][y + 1] == 300 && w.wire[x][y + 1] != 301 && !(w.wire[x][y] >= 10 && w.wire[x][y] <= 12)) || (w.wire[x][y + 1] == 302 && (w.wire[x][y] >= 10 && w.wire[x][y] <= 12)))
			{
				if (isCable(x, y, w))
				{
					triggerActive(x, y + 1, w, w.voltage[x][y] - 0.025);
				}
				else
				{
					triggerActive(x, y + 1, w, w.voltage[x][y] - 0.05);
				}
			}
			if (Math.floor((double)w.wire[x][y - 1] / 2) == w.wire[x][y] || (w.wire[x][y - 1] == 300 && w.wire[x][y - 1] != 301 && !(w.wire[x][y] >= 10 && w.wire[x][y] <= 12)) || (w.wire[x][y - 1] == 302 && (w.wire[x][y] >= 10 && w.wire[x][y] <= 12)))
			{
				if (isCable(x, y, w))
				{
					triggerActive(x, y - 1, w, w.voltage[x][y] - 0.025);
				}
				else
				{
					triggerActive(x, y - 1, w, w.voltage[x][y] - 0.05);
				}
			}
			if (Tile.tiles[w.world[x][y]].canBePowered)
			{
				if (isActive(x, y, w) && w.voltage[x][y] >= Tile.tiles[w.world[x][y]].minVoltage)
				{
					Tile.tiles[w.world[x][y]].onPower(x, y, w);
				}
				else
				{
					Tile.tiles[w.world[x][y]].onUnpower(x, y, w);
				}
			}
		}
		else
		{
			return;
		}
		*/
	}
}
