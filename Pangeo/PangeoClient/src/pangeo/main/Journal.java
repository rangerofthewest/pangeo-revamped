package pangeo.main;

import java.util.ArrayList;
import java.util.List;

public class Journal 
{
	public List<String> entries = new ArrayList<String>();
	public List<Integer> entriesX = new ArrayList<Integer>();
	public List<Integer> entriesY = new ArrayList<Integer>();
	public List<Integer> entryId = new ArrayList<Integer>();
	
	public void addEntry(String name, String txt, int id)
	{
		if (!Main.godMode && (!(entryId.contains(id) && entries.contains(name + ": " + txt)) || id == -1))
		{
			entries.add(name + ": " + txt);
			entriesX.add(World.rand.nextInt(498) + 2);
			entriesY.add(13);
			entryId.add(id);
		}
	}
}
