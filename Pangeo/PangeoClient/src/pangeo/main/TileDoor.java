package pangeo.main;

public class TileDoor extends Tile {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8999167511647560744L;
	public short idBecomes;
	public boolean doorOpen;
	public Item key = null;

	public TileDoor(String s, int i, int becomes, boolean open) {
		super(s, i);
		if (open) {
			solid = false;
			this.setSize(2, 3);
		} else {
			solid = true;
			this.setSize(1, 3);
		}
		doorOpen = open;
		idBecomes = (short) becomes;
	}

	public TileDoor setKey(Item i) {
		key = i;
		return this;
	}

	public void onActivate(int x, int y, World w, EntityPlayer p) {
		if (!doorOpen) {
			if (key == null) {
				Tile.tiles[idBecomes].onPlaceTile(x - w.xSlot[x][y], y
						- w.ySlot[x][y], w);
			}
		} else {
			Tile.tiles[idBecomes].onPlaceTile(x - w.xSlot[x][y], y
					- w.ySlot[x][y], w);
			w.world[x - w.xSlot[x][y] + 1][y - w.ySlot[x][y]] = 0;
			w.world[x - w.xSlot[x][y] + 1][y - w.ySlot[x][y] + 1] = 0;
			w.world[x - w.xSlot[x][y] + 1][y - w.ySlot[x][y] + 2] = 0;
		}
	}
}
