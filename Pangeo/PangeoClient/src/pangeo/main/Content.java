package pangeo.main;

import java.awt.Font;
import java.io.IOException;

import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Content 
{
	@SuppressWarnings("null")
	public static Texture loadTexture(String path)
	{
		Texture texture = null;
		try {
			texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("assets/textures/" + path));
			return texture;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Util.print("Texture loaded: "+texture);
			Util.print(">> Image width: "+texture.getImageWidth());
			Util.print(">> Image height: "+texture.getImageHeight());
			Util.print(">> Texture width: "+texture.getTextureWidth());
			Util.print(">> Texture height: "+texture.getTextureHeight());
			Util.print(">> Texture ID: "+texture.getTextureID());
			e.printStackTrace();
			return null;
		}
	}
	
	public static TrueTypeFont loadFont(String path, int font, int size)
	{
		return new TrueTypeFont(new Font("assets/fonts/" + path, font, size), true);
	}
}
