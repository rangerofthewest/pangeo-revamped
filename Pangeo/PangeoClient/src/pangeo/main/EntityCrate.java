package pangeo.main;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

public class EntityCrate extends Entity 
{
	public static List<EntityCrate> cratesActive = new ArrayList<EntityCrate>();
	
	public Texture crate;
	
	public EntityCrate(float posX, float posY, Texture tex) 
	{
		super("Crate");
		this.pos = new Vector2(posX, posY);
		this.crate = tex;
		cratesActive.add(this);
	}

	public void onUpdate() 
	{
		if (!Tile.tiles[Main.world.world[(int)Math.ceil(this.pos.X)][(int)Math.ceil(this.pos.Y)]].solid)
		{
			pos.Y += 0.24;
		}
	}
	public void draw()
	{
		Main.draw(crate, 640 + (int)Math.floor((pos.X - Main.player.pos.X) * 16), 380 + (int)Math.floor((pos.Y - Main.player.pos.Y) * 16), Color.white);
	}
}
