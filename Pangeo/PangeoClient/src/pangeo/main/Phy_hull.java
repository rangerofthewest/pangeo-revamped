package pangeo.main;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;


/**
 * justprog
    [1:13:31 AM] rangerofthewest: work on collision
    [1:13:37 AM] rangerofthewest: it needs to be based on texture size
    [1:13:45 AM] rangerofthewest: and must work for projectiles, tiles, and players
 * @author JustP
 *
 */
public class Phy_hull {
    
    java.util.LinkedList<java.util.ArrayList<Vector2>> debug_showMotion;
    /**   clockwise only  */
    java.util.ArrayList<Vector2> ps;// close shape only, [0]=[last]
  //  java.util.ArrayList<Float> ps_size1;
    Vector2 v;
    Vector2 shift;//real position of the polygon = ps+shift  ,  not used
    float mass=1;
    public Phy_hull(){
        ps=new ArrayList<Vector2>();
        v=new Vector2();
        shift=new Vector2();
        debug_showMotion=new LinkedList<ArrayList<Vector2>>();
    }
    public Phy_hull(java.util.ArrayList<Vector2> ps_new){
        this();
        ps=ps_new;
    }
    public Phy_hull(float x,float y,float w,float h){
        this();
        ps.add(new Vector2(x,y));
        ps.add(new Vector2(x+w,y));
        ps.add(new Vector2(x+w,y+h));
        ps.add(new Vector2(x,y+h));
        ps.add(new Vector2(x,y));
    }
    /** */
    private static ArrayList<Point>  spriteToPoly(BufferedImage buff){
    
        ArrayList<Point> pDetailed=spriteToPoly_getMiddleContour(buff);
        ArrayList<Point> pCoarse=spriteToPoly_simplifyPoly(pDetailed);
        return pCoarse;
    }
    
    
    private static boolean spriteToPoly_getMiddleContour__isIn(BufferedImage buff,int x,int y){
        boolean retur;
        if(x<0 || y<0 || x>=buff.getWidth() || y>=buff.getHeight()){
            retur=false;
        }else{
            java.awt.Color rgba=new java.awt.Color(buff.getRGB(x, y));
            retur=rgba.getAlpha()>200;
            
        }
        return retur;
    }
    private static java.util.ArrayList<Point>  spriteToPoly_getMiddleContour(BufferedImage buff){
        //Algorithm : find contour in the middle of image.
        // Abbreviation :  O = Oblique,  T = transparent
        //         4     3   6
        //            |----------------
        //         1  |  2   5
        //     --------
        // The first step is to find the oblique "1" which has transparent "2" in its right.
        //   "1" is assume to be a part of a big contour
        // "1" will be set as contour[0]  and  "currentState_tryTo_0right_1up_2left_3down" set as "trying to grow right" 
        // The second step is to growing.  "1" will try to grow in direction "2","3","4" respectively.   
        //     Notice this sequence 1O ---right--> 2T --up--> 3O --left--> 4O --down--> 1O , try to find pattern the match nT --> (n+1)O  first.
        //     In this case it is 2T --> 3O, so the next contour point after 1O is 3O.
        //     2T --> 3O is up, so "currentState_tryTo_0right_1up_2left_3down" = down (3) which opposite to up
        //  In the next interation, 3O --down--> 2T --right--> 5T --up--> 6O bra.... 
        //          ...  find pattern  5T --up--> 6O , so 6O is the next contour point, set state=down(3)
        boolean findOblique=false;
        int xF=0;
        for(int x=buff.getWidth()/2;true;x++){//.. xF
            boolean isOblique=spriteToPoly_getMiddleContour__isIn(buff,x, buff.getHeight()/2);
            if(!findOblique){
                if(isOblique){
                    findOblique=true;
                }
            }
            if(findOblique){
                if(!isOblique){
                    xF=x;break;
                }
            }
        }
        ArrayList<Point> contour=new ArrayList<Point>();
        contour.add(new Point(xF-1,buff.getHeight()/2));//add near-margin oblique point
        int currentState_tryTo_0right_1up_2left_3down=0;//up=-y,right=+x
        ArrayList<Point> operations=new ArrayList<Point>();
        operations.add(new Point(1,0));
        operations.add(new Point(0,-1));
        operations.add(new Point(-1,0));
        operations.add(new Point(0,1));
        while(true){
            int iniState=currentState_tryTo_0right_1up_2left_3down;
            Point iniPoint=contour.get(contour.size()-1);
            {
                Point l_current=iniPoint; Point currentNew=null;
                int n4found=-1;
                for(int n4=0;n4<4;n4++){
                    Point operation=operations.get((n4+iniState)%4);
                    Point l_next=new Point(l_current.x+operation.x,l_current.y+operation.y);
                    boolean f_current=spriteToPoly_getMiddleContour__isIn(buff,l_current.x,l_current.y);
                    boolean f_next=spriteToPoly_getMiddleContour__isIn(buff,l_next.x,l_next.y);
                    if(!f_current && f_next){
                        n4found=n4;currentNew=l_next;
                        break;
                    }
                    l_current=l_next;
                }
                currentState_tryTo_0right_1up_2left_3down=(n4found+2)%4; //opposite direction
                contour.add(currentNew);
                if(currentNew.equals(contour.get(0) )){
                    break;
                }
            }
        }
        
        return contour;
    }
    
    private static java.util.ArrayList<Point> spriteToPoly_simplifyPoly(java.util.ArrayList<Point> detailed){
        //modify algorithm from http://www.mathworks.com/matlabcentral/fileexchange/45342-polygon-simplification/content/reduce_poly.m
        boolean const_convexForcer=true;
        float const_badnessThreshold=9;
        ArrayList<Point> coarse=(ArrayList<Point>) detailed.clone();
        while(  true  ){
            int detailSize1=detailed.size()-1;
            float l_maxValue=1234567;
            float l_maxIndex=-123456;
            boolean badIsBadEnough=false;
            for(int n=0;n<detailSize1;n++){//.. find "l_maxValue" , "l_maxIndex"
                Point prev      = detailed.get( (n-1+detailSize1)%detailSize1      );
                Point current   = detailed.get( (n+0+detailSize1)%detailSize1      );
                Point next      = detailed.get( (n+1+detailSize1)%detailSize1      );
                Vector2 l1      = new Vector2( current.x-prev.x , current.y-prev.y );
                Vector2 l2      = new Vector2( next.x-current.x , next.y-current.y );
                float sin_l1size_l2size=Math.abs( l1.cross(l2) );
                float cos_l1size_l2size= l1.dot(l2)           ;
                float badness;// importance of a vertex
                if(cos_l1size_l2size<0){
                    badness=Math.abs(cos_l1size_l2size);
                }else{
                    badness=sin_l1size_l2size;
                }
                if(const_convexForcer&& l1.cross(l2)>0)badness=0;
                if(badness<const_badnessThreshold)badIsBadEnough=true;
                if(badness<l_maxValue){
                    l_maxValue=badness;  l_maxIndex=n;
                }
            }
            if(badIsBadEnough){
                coarse.remove(l_maxIndex);
            }else{
                break;
            }
        }
        return coarse;
    }
    
   
    
    
    /** very slow,   low quality beveling but should be enough for eliminating jig-jag moving (Ghost vertex in Box2D)  */
    public void bevelMe(){
        float const_radius=2.0f;
        float const_bevelRatio=0.1f;
        java.util.ArrayList<Vector2> array=new ArrayList<Vector2>();
        int amountCorner=ps.size()-1;
        for(int n=0;n<ps.size()-1;n++){
//            Vector2 normal=ps.get( (n+1+ps.size())%ps.size())      .minus(
//                                    ps.get( (n-1+ps.size())%ps.size())
//                            ).rotateCounterClockwise90degree().normalize_effectThis();

            Vector2 pointABit_Before=ps.get( (n+amountCorner)%amountCorner).minus(
                                            ps.get( (n+amountCorner)%amountCorner).minus(
                                                ps.get( (n-1+amountCorner)%amountCorner)
                                            ).normalize_effectThis().dot(const_radius)
                                      );
            Vector2 pointABit_After=ps.get( (n+amountCorner)%amountCorner).add(
                    ps.get( (n+1+amountCorner)%amountCorner).minus(
                        ps.get( (n+amountCorner)%amountCorner)
                    ).normalize_effectThis().dot(const_radius)
              );
            Vector2 pointAvg=pointABit_Before.add(pointABit_After).dot(0.5f);
            Vector2 pointNewCornerRisk=ps.get( (n+amountCorner)%amountCorner).dot(1-const_bevelRatio)  .add(pointAvg.dot(const_bevelRatio));
            array.add(pointABit_Before);
            array.add(pointNewCornerRisk);
            array.add(pointABit_After);
        }
        array.add((Vector2) array.get(0).clone());
        this.ps=array;
    }
    private  Phy_hull cloneShallow(){
        Phy_hull retur=new Phy_hull();
        retur.ps=this.ps;
        //ret
        retur.v=this.v;
        retur.shift=this.shift;
        retur.mass=this.mass;
        return retur;
    }
    
    /** point1-->point2  have to clockwise */
    private static void collisionDetection_linear___line_findZone(Vector2 point1,Vector2 point2,Phy_hull vertexs,float[] retur_len2){
        
        Vector2 line=point2.minus(point1);
        retur_len2[0]=1234567890123456.0f;
        retur_len2[1]=-1234567890123456.0f;
        for(int n=0;n<vertexs.ps.size();n++){
            Vector2 vert_minus_p1=vertexs.ps.get(n).minus(point1);
            float crossProduct=line .cross( vert_minus_p1 );
            retur_len2[0]=Math.min(retur_len2[0], crossProduct);
            retur_len2[1]=Math.max(retur_len2[1], crossProduct);
        }
    }
    private static float collisionDetection_linear___lineA_crashVertB(Phy_hull a,Phy_hull b, int[] indexLineA_len1){
        Vector2 velA_minus_velB=a.v.minus(b.v);
        float retur=0;
        if(velA_minus_velB.size2()>0.0001f){
            ArrayList<float[]>debug_keeper=new ArrayList<float[]>();
            float ratioToPassBest=-123456; //0 to 1
            boolean penetrateOccur0=false;
            for(int na=0;na<a.ps.size()-1;na++){
                if(na==3){
                    int aadsfadsafsdafsd=0;aadsfadsafsdafsd++;
                }
                Vector2 lp1=a.ps.get(na);
                Vector2 lp2=a.ps.get(na+1);
                float crossProductOfVel=lp2.minus(lp1).cross(velA_minus_velB);
                //                      /|   relaVel  |/  
                //   convexHull a       /|   ----->   |/   convexHull b
                //                      /|            |/
                // retur1[0]         retur1[1]      retur2[0]             retur2[0]
                // crossProduct    - - - 0 + + + + + + + + + + +
                float ratioToPass=-123456;
                if( crossProductOfVel>0)    { 
                    //==  According to only "line lp1--lp2" and "relativeVelocity" information, it is possible to crash
                  
//                    if(retur1[1]<retur2[0]){//no overlap
//                        ratioToPass=(retur2[0]-retur1[1])/crossProductOfVel;
//                        if(ratioToPass>1)ratioToPass=1;
//                        if(ratioToPass<0)ratioToPass=0;//Util.print("Phy_hull error#2175 ratioToPass="+ratioToPass);
//                    }else{//== overlaping (should not happen often)
//                        float retur1avg=(retur1[0]+retur1[1])/2;
//                        float retur2avg=(retur2[0]+retur2[1])/2;
//                        if(retur1avg>retur2avg){//already pass
//                            //ignore me    //ratioToPass=1;
//                        }else  ratioToPass=0;//penetrateOccur0=true;
//                    }
                    
                }else{ //impossible surely
                
                }
                
                float[] retur1=new float[2];
                float[] retur2=new float[2];
                
                collisionDetection_linear___line_findZone(lp1,lp2, a  , retur1  );
                collisionDetection_linear___line_findZone(lp1,lp2, b  , retur2  );
                if(crossProductOfVel>0){ratioToPass=(retur2[0]-retur1[1])/crossProductOfVel; if(retur1[0]>retur2[1])ratioToPass=1;  }
                else if(crossProductOfVel<0){ratioToPass=(retur2[1]-retur1[0])/crossProductOfVel;if(retur2[0]>retur1[1])ratioToPass=1;}
                else { 
                    float maxx=Math.max(retur1[0], retur2[0]);float minn=Math.min(retur1[1], retur2[1]); 
                    if(maxx<minn) ratioToPass=0;
                    else ratioToPass=1;
                }
                debug_keeper.add(retur1);debug_keeper.add(retur2);debug_keeper.add(new float[]{ratioToPass});
                
                if(ratioToPass==8){
                    int asfafasd=0;asfafasd++;
                    
                }
                //if(0<=ratioToPass && ratioToPass<=1){
                if(ratioToPass>ratioToPassBest)   {
                    ratioToPassBest=ratioToPass;
                    indexLineA_len1[0]=na;
                }
                   // ratioToPassBest=Math.max(ratioToPassBest, ratioToPass);
                   // indexLineA_len1[0]=na;
                //}
            }
            if(ratioToPassBest==-123456){
                ratioToPassBest=0;
                //Util.print("Phy_hull#5017 not expect this");
            }
            retur= ratioToPassBest;
        }else{
            retur= 1;
        }
        return retur;
    }
    
    
    public static float collisionDetection_linear(Phy_hull a,Phy_hull b,Vector2 normal_out){
        int[]trace1=new int[1];
        float f1=collisionDetection_linear___lineA_crashVertB(a,b,trace1);
       
        //---
        Phy_hull ponyA,ponyB;
        ponyA=a.cloneShallow();
        //ponyA.v=new Vector2(-a.v.X,-a.v.Y);
        int[]trace2=new int[1];
        ponyB=b.cloneShallow();
        //ponyB.v=new Vector2(-b.v.X,-b.v.Y);
        float f2=collisionDetection_linear___lineA_crashVertB(ponyB,ponyA,trace2);
        float retur=Math.max(f1, f2);
        if(f2<f1){
            Vector2 normalTmp= a.ps.get(trace1[0]+1).minus(a.ps.get(trace1[0]) ).  rotateCounterClockwise90degree();
            normal_out.X=normalTmp.X;  
            normal_out.Y=normalTmp.Y;
        }else{
            Vector2 normalTmp= b.ps.get(trace2[0]+1).minus(b.ps.get(trace2[0]) ).  rotateCounterClockwise90degree();
            normal_out.X=normalTmp.X;  
            normal_out.Y=normalTmp.Y;
        }
        //Util.print(""+f1+" "+f2);
        return retur;
    }
    public static void collisionResponse_linear(Phy_hull a,Phy_hull b,Vector2 normal,float coefficientOfRestitution,float coefficientOfFriction){
        //http://en.wikipedia.org/wiki/Coefficient_of_restitution
        float velA_normal=( a.v.dot(normal)  );// the real valid value is this value divided by normal.size1()
        float velB_normal=( b.v.dot(normal)  );// the real valid value is this value divided by normal.size1()
        float velA_will=(
                a.mass*velA_normal+b.mass*velB_normal+b.mass*coefficientOfRestitution*(velB_normal-velA_normal)
                )/(a.mass+b.mass);//// the real valid value is this value divided by normal.size1()
        float velB_will=coefficientOfRestitution*(velA_normal-velB_normal)+velA_will;//// the real valid value is this value divided by normal.size1()
        //.. friction
        float momentumTransferFromAToB=(velB_will-velB_normal)*b.mass;//// the real valid value is this value divided by normal.size1()
        float momentumMaxSizeFriction=Math.abs(momentumTransferFromAToB*coefficientOfFriction);
        Vector2 tangent=normal.rotateCounterClockwise90degree();
        float velA_tangent= ( a.v.dot(tangent)  );
        float velB_tangent= ( b.v.dot(tangent)  );
        float velA_tangent_will,velB_tangent_will;
        float momentumFriction_required_if_objectMoveTogether=
            Math.abs(
                    velB_tangent-  ( velB_tangent*b.mass +  velA_tangent*a.mass    ) / (b.mass+a.mass)  
            )*b.mass;
        float momentumFrictionSize_transferActuallyInThisCollision;
        if( momentumFriction_required_if_objectMoveTogether > momentumMaxSizeFriction){
            momentumFrictionSize_transferActuallyInThisCollision=momentumMaxSizeFriction;
        }else{
            momentumFrictionSize_transferActuallyInThisCollision=momentumFriction_required_if_objectMoveTogether;
        }
        if(velA_tangent>velB_tangent){
            //v    the second term is always positive.
            velA_tangent_will=velA_tangent-momentumFrictionSize_transferActuallyInThisCollision/a.mass;
            velB_tangent_will=velB_tangent+momentumFrictionSize_transferActuallyInThisCollision/b.mass;
        }else{//v   the second term is always positive.
            velA_tangent_will=velA_tangent+momentumFrictionSize_transferActuallyInThisCollision/a.mass;
            velB_tangent_will=velB_tangent-momentumFrictionSize_transferActuallyInThisCollision/b.mass;
        }
        Vector2 velA_finalVelocity       =  normal.dot(velA_will).add(tangent.dot(velA_tangent_will)).dot(1/normal.size2());
        Vector2 velB_finalVelocity       =  normal.dot(velB_will).add(tangent.dot(velB_tangent_will)).dot(1/normal.size2());
        a.v=velA_finalVelocity;
        b.v=velB_finalVelocity;
    }
    
    
    public  void paint_debug(Graphics2D g2){
        java.awt.Color coIn=g2.getColor();
        int k=0;
        for( Iterator<ArrayList<Vector2>> it = this.debug_showMotion.iterator();it.hasNext();k++){
            ArrayList<Vector2> ne = it.next();
//            java.awt.Color coK=new java.awt.Color(
//                    coIn.getRed()*k/this.debug_showMotion.size(),
//                    coIn.getGreen()*k/this.debug_showMotion.size(),
//                    coIn.getBlue()*k/this.debug_showMotion.size()
//            );
            java.awt.Color coK=new java.awt.Color(
                    coIn.getRed(),
                    coIn.getGreen(),
                    coIn.getBlue(),255*k/this.debug_showMotion.size()/2
            );
            g2.setColor(coK);
            paintAPoly(g2,ne);
        }
        
        g2.setColor(coIn);
        
        
    }
    public  void paint(Graphics2D g2){
        paintAPoly(g2,this.ps);
    
    }
    private void paintAPoly(Graphics2D g2,ArrayList<Vector2> posXYs){
        
        for(int n=0;n<posXYs.size()-1;n++){
            g2.drawLine((int)posXYs.get(n).X,(int) posXYs.get(n).Y, (int)posXYs.get(n+1).X, (int)posXYs.get(n+1).Y);
        }
    
    }
    public void doThis(){
        if(Phy_engine.timee%2==0){
            ArrayList<Vector2> cloned=(ArrayList<Vector2>) this.ps.clone();
            debug_showMotion.add(cloned);
           // if(debug_showMotion.size()>20)
            if(debug_showMotion.size()>80)debug_showMotion.removeFirst();
        }
        for(int n=0;n<this.ps.size();n++){
            Vector2 vec2=this.ps.get(n);
            vec2=vec2.add(this.v);
            this.ps.set(n, vec2);
        }
        
    }
    public void receiveAcceleration(float i, float j) {
        this.v.X+=i;
        this.v.Y+=j;
    }
    public void movePrototype(float i, float j) {
        Vector2 current=this.v;
        Vector2 newww=(new Vector2(i,j));
        this.v=newww;
        this.doThis();
        this.v=current;
    }
    
}
