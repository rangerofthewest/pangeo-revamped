package pangeo.main;

import org.newdawn.slick.Color;

public class ItemDye extends Item 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7339550353081076232L;
	public Color paintColor;
	
	public ItemDye(String s, Color c)
	{
		super("Dye (" + s + ")");
		paintColor = c;
		this.maskColor = c;
	}
}
