package pangeo.main;

import java.util.List;
import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;

public class DroppedItem
{
	public static List<DroppedItem> dropItems = new ArrayList<DroppedItem>();
	
	public Item i;
	public float posX;
	public float posY;
	public int amt;
	public int id;
	
	public static int lastId = 0;
	
	public DroppedItem(Item item, Vector2 pos, int stack)
	{
		i = item;
		posX = pos.X;
		posY = pos.Y;
		amt = stack;
		id = lastId;
		lastId++;
	}
	public static double getDecimal(double d)
	{
		return d - Math.floor(d);
	}
	public void draw()
	{
		Main.draw(i.t, 640 + (int)Math.floor((posX - Main.player.pos.X) * 16), 380 + (int)Math.floor((posY - Main.player.pos.Y) * 16), Color.white);
		if (!Main.observer && Mouse.getX() >= 640 + (int)Math.floor((posX - Main.player.pos.X) * 16) && Mouse.getX() <= 640 + (int)Math.floor((posX - Main.player.pos.X) * 16) + 16 && (760 - Mouse.getY()) >= 380 + (int)Math.floor((posY - Main.player.pos.Y) * 16) && (760 - Mouse.getY()) <= 380 + (int)Math.floor((posY - Main.player.pos.Y) * 16) + 16)
		{
			WorldEdit.font.drawString(Mouse.getX() + 8, 760 - Mouse.getY(), i.name + " (" + amt + ")");
		}
		if (Main.player.pos.distanceTo(new Vector2(posX, posY)) < 3.2)
		{
			dropItems.remove(this);
			Main.player.inventory.addToInventory(i, amt);
		}
		if (!Tile.tiles[Main.world.world[(int)posX][(int)posY + 1]].solid)
		{
			posY += 0.5f;
		}
	}
}
