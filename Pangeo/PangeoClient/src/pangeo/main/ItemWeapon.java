package pangeo.main;


public class ItemWeapon extends Item 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6667975064370249952L;
	public int damage = 0;
	public EnumHitEffect hitEffect = EnumHitEffect.NOTHING;
	public float hitEffectChance = 1.0f;
	public int weight = 0; //1 is the weight of a dagger
	public DamageType dmgType = DamageType.STAB;
	
	public ItemWeapon(String s, int d) 
	{
		super(s);
		damage = d;
		// TODO Auto-generated constructor stub
	}
	
	public ItemWeapon seDmgType(DamageType d)
	{
		this.dmgType = d;
		return this;
	}
	
	public ItemWeapon setHitEffect(EnumHitEffect e)
	{
		this.hitEffect = e;
		return this;
	}
	
	public ItemWeapon setHitEffectChance(float c)
	{
		this.hitEffectChance = c;
		return this;
	}
	
	public ItemWeapon setWeight(int w)
	{
		weight = w;
		return this;
	}

}
