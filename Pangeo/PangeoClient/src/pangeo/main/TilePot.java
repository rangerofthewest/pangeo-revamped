package pangeo.main;

public class TilePot extends Tile {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2290915596795981355L;
	public static int rareItems = 0;

	public TilePot(String s, int i) {
		super(s, i);
		lore = "The remains of an ancient civilization, left behind in the form of an earthen vessel...";
		durability = 0;
		// TODO Auto-generated constructor stub
	}

	public void onKillTile(int x, int y, World w) {
		super.onKillTile(x, y, w);
		// Decide what loot to drop
		float chance = World.rand.nextFloat();
		// 2% (varying) chance to drop nothing
		// 5% (varying) chance to drop a rare item
		// 14% chance to drop an uncommon item
		// 79% chance to drop a common item
		if (chance < 0.81f) {
			// Basic loot
			if (chance > 0.02f) {
				if (World.rand.nextInt(8) == 0) {
					if (World.rand.nextFloat() < 0.97f) {
						Main.player.addMoney(World.rand.nextInt(145) + 5);
					} else {
						Main.player.addMoney(World.rand.nextInt(340) + 10);
					}
				} else {
					if (World.rand.nextBoolean()) {
						Item.newItem(839, x, y, World.rand.nextInt(8) + 1);
					} else {
						if (World.rand.nextBoolean()) {
							if (World.rand.nextBoolean()) {
								if (World.rand.nextBoolean()) {
									Item.newItem(840, x, y,
											World.rand.nextInt(3) + 2);
								} else {
									if (World.rand.nextBoolean()) {
										Item.newItem(846, x, y,
												World.rand.nextInt(3) + 1);
									} else {
										Item.newItem(847, x, y,
												World.rand.nextInt(3) + 4);
									}
								}
							} else {
								if (World.rand.nextBoolean()) {
									Item.newItem(843, x, y,
											World.rand.nextInt(3) + 1);
								} else {
									if (World.rand.nextBoolean()) {
										Item.newItem(844, x, y,
												World.rand.nextInt(11) + 6);
									} else {
										Item.newItem(845, x, y,
												World.rand.nextInt(4) + 3);
									}
								}
							}
						} else {
							if (World.rand.nextBoolean()) {
								Item.newItem(841, x, y,
										World.rand.nextInt(3) + 1);
							} else {
								if (World.rand.nextBoolean()) {
									if (World.rand.nextBoolean()) {
										Item.newItem(842, x, y,
												World.rand.nextInt(2) + 1);
									} else {
										Item.newItem(853, x, y,
												World.rand.nextInt(2) + 1);
									}
								} else {
									Item.newItem(843, x, y,
											World.rand.nextInt(3) + 1);
								}
							}
						}
					}
				}
			}
		} else {
			if (chance < 0.95f) {
				// Uncommon loot
				if (World.rand.nextBoolean()) {
					if (World.rand.nextBoolean()) {
						if (World.rand.nextBoolean()) {
							Item.newItem(848, x, y, World.rand.nextInt(2) + 1);
						} else {
							Item.newItem(851, x, y, World.rand.nextInt(4) + 5);
						}
					} else {
						Item.newItem(850, x, y, World.rand.nextInt(2) + 1);
					}
				} else {
					if (World.rand.nextBoolean()) {
						Item.newItem(849, x, y, World.rand.nextInt(6) + 1);
					} else {
						Item.newItem(834, x, y, World.rand.nextInt(6) + 1);
					}
				}
			} else {
				// Rare loot
				if (World.rand.nextInt(TilePot.rareItems + 1) == 0) {
					if (World.rand.nextBoolean()) {
						Item.newItem(852, x, y, World.rand.nextInt(2) + 1);
					} else {
						Item.newItem(509, x, y, World.rand.nextInt(3) + 1);
					}
					TilePot.rareItems++;
				}
			}
		}
	}
}
