package pangeo.main;

public class SkillTree {
	public Skill skill;
	public EnumClasses archtype;
	public int maxLevel;

	public SkillTree(Skill sk, int lv, EnumClasses c) {
		skill = sk;
		maxLevel = lv;
		archtype = c;
	}
}
