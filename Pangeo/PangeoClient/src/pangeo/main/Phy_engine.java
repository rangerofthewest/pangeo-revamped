package pangeo.main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Phy_engine implements ActionListener {
    
    java.util.ArrayList<Phy_hull> hulls=new ArrayList<Phy_hull>();
    
    javax.swing.Timer timer;
    Mill mill;
    static int timee=0;
    
    public static void main(String[]args){
        Phy_engine engine=new Phy_engine();
        {//[0] ball
            Vector2 avv[]={new Vector2(0,0),new Vector2(0,20), new Vector2(10,25),new Vector2(20,20),    new Vector2(20,0),
                    new Vector2(0,0)};
            java.util.ArrayList<Vector2> avvArr=new ArrayList<Vector2>(java.util.Arrays.asList(avv));
            Phy_hull aHull=new Phy_hull(  avvArr );
            aHull.movePrototype(140,30);
            aHull.v.X=2;
            engine.hulls.add(aHull);
        }
        {//[1] ball
            Vector2 avv[]={new Vector2(0,0),new Vector2(0,20), new Vector2(10,21),new Vector2(20,20),    new Vector2(20,0),
                    new Vector2(10,-10), new Vector2(0,0)};
            java.util.ArrayList<Vector2> avvArr=new ArrayList<Vector2>(java.util.Arrays.asList(avv));
            Phy_hull aHull=new Phy_hull(  avvArr );
            aHull.movePrototype(180,100);
            aHull.v.X=4;
            engine.hulls.add(aHull);
        }
        {//[2] ball
            Vector2 avv[]={new Vector2(0,0),new Vector2(0,20), new Vector2(10,21),new Vector2(20,20),    new Vector2(20,0),
                    new Vector2(0,0)};
            java.util.ArrayList<Vector2> avvArr=new ArrayList<Vector2>(java.util.Arrays.asList(avv));
            Phy_hull aHull=new Phy_hull(  avvArr );
            aHull.movePrototype(200,100);
            aHull.v.X=-2;
            engine.hulls.add(aHull);
        }
        {//[3] .. floor 2nd floor
            Vector2 bvv[]={
                    new Vector2(0,0),new Vector2(0,20),new Vector2(200,20),new Vector2(200,0),
                    new Vector2(0,0)
            };
            java.util.ArrayList<Vector2> bvvArr=new ArrayList<Vector2>(java.util.Arrays.asList(bvv));
            Phy_hull bHull=new Phy_hull(  bvvArr );
            bHull.movePrototype(100,200);
            bHull.mass=100000;
            engine.hulls.add(bHull);
        }
        {//[4] .. 1st floor
            Vector2 bvv[]={
                    new Vector2(0,0),new Vector2(0,20),new Vector2(355,20),new Vector2(355,0),
                    new Vector2(0,0)
            };
            java.util.ArrayList<Vector2> bvvArr=new ArrayList<Vector2>(java.util.Arrays.asList(bvv));
            Phy_hull bHull=new Phy_hull(  bvvArr );
            bHull.movePrototype(35,400);
            bHull.mass=100000;
            engine.hulls.add(bHull);
        }
        {//[5] left wall
            Vector2 bvv[]={
                    new Vector2(0,0),new Vector2(0,500),new Vector2(10,500),new Vector2(10,0),
                    new Vector2(0,0)
            };
            java.util.ArrayList<Vector2> bvvArr=new ArrayList<Vector2>(java.util.Arrays.asList(bvv));
            Phy_hull bHull=new Phy_hull(  bvvArr );
            bHull.movePrototype(20,0);
            bHull.mass=100000;
            engine.hulls.add(bHull);
        }
        {//[6] right wall
            Vector2 bvv[]={
                    new Vector2(0,0),new Vector2(0,500),new Vector2(10,500),new Vector2(10,0),
                    new Vector2(0,0)
            };
            java.util.ArrayList<Vector2> bvvArr=new ArrayList<Vector2>(java.util.Arrays.asList(bvv));
            Phy_hull bHull=new Phy_hull(  bvvArr );
            bHull.movePrototype(400,0);
            bHull.mass=100000;
            engine.hulls.add(bHull);
        }
        {//[7] up-left
            Vector2 bvv[]={
                    new Vector2(0,0),new Vector2(0,50),new Vector2(150,0),new Vector2(0,0)
            };
            java.util.ArrayList<Vector2> bvvArr=new ArrayList<Vector2>(java.util.Arrays.asList(bvv));
            Phy_hull bHull=new Phy_hull(  bvvArr );
            bHull.movePrototype(35,0);
            bHull.mass=100000;
            engine.hulls.add(bHull);
        }
        {//[8] up-right
            Vector2 bvv[]={
                    new Vector2(-30,0),new Vector2(100,100),new Vector2(100,0),new Vector2(0,0)
            };
            java.util.ArrayList<Vector2> bvvArr=new ArrayList<Vector2>(java.util.Arrays.asList(bvv));
            Phy_hull bHull=new Phy_hull(  bvvArr );
            bHull.movePrototype(295,0);
            bHull.mass=100000;
            engine.hulls.add(bHull);
        }
        {//[9] down-left
            Vector2 bvv[]={
                    new Vector2(0,0),new Vector2(0,100),new Vector2(100,100),new Vector2(0,0)
            };
            java.util.ArrayList<Vector2> bvvArr=new ArrayList<Vector2>(java.util.Arrays.asList(bvv));
            Phy_hull bHull=new Phy_hull(  bvvArr );
            bHull.movePrototype(35,290);
            bHull.mass=100000;
            engine.hulls.add(bHull);
        }
        for(int n=0;n<engine.hulls.size();n++){
            engine.hulls.get(n).bevelMe();
            
        }
        
        JFrame frame=new JFrame("Phy_engine");
        engine.mill=engine.new Mill();
        frame.getContentPane().add(engine.mill);
        engine.timer=new javax.swing.Timer(30,engine);
        frame.setSize(new Dimension(600,600));
        frame.setVisible(true);
        engine.timer.start();
    }
    
    public void actionPerformed(ActionEvent arg0) {
        timee++;
        
        Util.print("timeee="+timee);
        if(timee==258){
            int asfasdf=0;
            asfasdf++;
        }
        this.doThis();
        mill.repaint();

    }
    
    public void doThis(){
        hulls.get(0).receiveAcceleration(0,0.1f);
        hulls.get(1).receiveAcceleration(0,0.1f);
        hulls.get(2).receiveAcceleration(0,0.1f);
//        Vector2 normalCo=new Vector2();
//        float retur=Phy_hull.collisionDetection_linear(hulls.get(0), hulls.get(1),normalCo);
//        if(retur<1){
//            Phy_hull.collisionResponse_linear(hulls.get(0), hulls.get(1), normalCo, 1.0f, 0.1f);
//            Util.print("crash  lambda1="+retur+" normalCC="+normalCo);
//        }
        
        
        for(int n=0;n<hulls.size();n++){
            for(int m=n+1;m<hulls.size();m++){
              Vector2 normalCo=new Vector2();
              if(n==0 && m==9 && timee==258){
                  int as=0;as++;
                  
              }
              float retur=Phy_hull.collisionDetection_linear(hulls.get(n), hulls.get(m),normalCo);
              if(retur<1){
                  float coef_resti=1.0f;float coef_friction=0.0f;
                  if(n==3 || m==3){
                      coef_resti=0.5f;
                  }
                  if(n==4 || m==4){
                      coef_resti=1.2f;coef_friction=0.01f;
                  }
                
                  Phy_hull.collisionResponse_linear(hulls.get(n), hulls.get(m), normalCo, coef_resti,coef_friction);
                  Util.print("crash n="+n +" m="+ m  +"  lambda1="+retur+" normalCC="+normalCo);
              }
            }
        }
        for(int n=0;n<hulls.size();n++){
            hulls.get(n).doThis();
        }
      
        //hulls.get(0).doThis();
       // hulls.get(1).doThis();
    }
    class Mill extends JPanel{
        @Override
        public void paintComponent(Graphics g) {
           
            super.paintComponent(g);
            this.setBackground(new Color(0,0,0));
            java.awt.Graphics2D g2=(Graphics2D) g;
//            g2.setStroke(new BasicStroke(2.0f,
//                    BasicStroke.CAP_BUTT,
//                    BasicStroke.JOIN_MITER
//                  ));//  10.0f, new float[]{10.0f}, 0.0f) );
            ArrayList<Color>colors6=new ArrayList<Color>();
            colors6.add(new Color(255,0,0));
            colors6.add(new Color(0,255,0));
            colors6.add(new Color(0,0,255));
            colors6.add(new Color(255,255,0));
            colors6.add(new Color(255,0,255));
            colors6.add(new Color(0,255,255));
            for(int n=0;n<hulls.size();n++){
                g2.setColor(colors6.get(n%colors6.size()));
                hulls.get(n).paint_debug(g2);
            }
            g2.setStroke(new BasicStroke(2.0f,
                    BasicStroke.CAP_BUTT,
                    BasicStroke.JOIN_MITER
                  ));
            for(int n=0;n<hulls.size();n++){
                g2.setColor(colors6.get(n%colors6.size()));
                hulls.get(n).paint(g2);
            }
            {
                float pos1=200; float pos2=400;
                int va1=255; int va2=0;
                float vaMe=( (timee-pos1)*va2 +(pos2-timee)*va1  )/(pos2-pos1);
                int alpha= (int)Math.min(Math.max(vaMe, va2),va1);
                g2.setColor(new Color(255,255,255,alpha));
            }
            
            
            //g2.setFont(getFont())
            int yLine=20; int yPerLine=15;
            g2.drawString("Note to RangerOfTheWest from JustProg: ", 5, yLine);yLine+=yPerLine;
            g2.drawString("  This support only convex polygon.", 5, yLine);yLine+=yPerLine;
            g2.drawString("  No rotation is allowed. (strictly prohibit)", 5, yLine);yLine+=yPerLine;
            g2.drawString("  I don't think rotation/concave polygon is worth implementation.", 5, yLine);yLine+=yPerLine;
         //   g2.drawString("  The current algorithm is (very) fast, low risk of bug and easy to maintain.", 5, yLine);yLine+=yPerLine;
          //  g2.drawString("  Currently, the physic is quite precise, e.g. elasticity, momentum conservation and friction.", 5, yLine);yLine+=yPerLine;
            g2.drawString("  There is a bit shaking, when object trying to rest on surface.", 5, yLine);yLine+=yPerLine;
            g2.drawString("     The shaking fix is possible, no big deal.", 5, yLine);yLine+=yPerLine;
            g2.drawString("Elasticity : the first floor = 1.2", 5, yLine);yLine+=yPerLine;
            g2.drawString("  the second floor = 0.5", 5, yLine);yLine+=yPerLine;
            g2.drawString("  other = 1.0", 5, yLine);yLine+=yPerLine;
            g2.drawString("Friction : the first floor = 0.01", 5, yLine);yLine+=yPerLine;
            g2.drawString("  other = 0.0", 5, yLine);yLine+=yPerLine;
            g2.drawString("Mass : all small object = 1 , big platform/wall = 100000", 5, yLine);yLine+=yPerLine;
            g2.drawString("Gravity : all small object = 0.1down , big platform/wall = 0", 5, yLine);yLine+=yPerLine; 
            
        }
        
    }
}

