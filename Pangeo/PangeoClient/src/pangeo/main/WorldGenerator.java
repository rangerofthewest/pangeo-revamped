package pangeo.main;

public abstract class WorldGenerator 
{
	public abstract void generate(World w);
	public abstract int genPriority();
}
