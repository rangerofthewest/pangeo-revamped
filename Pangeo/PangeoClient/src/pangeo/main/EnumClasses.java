package pangeo.main;

public enum EnumClasses {
	// Rogue subclasses
	SHADOW, EVANESCENCE, ZEPHYR,

	// Summoner subclasses
	REQUIEM, ELEMENTALIST, BEASTMASTER,

	// Healer subclasses
	CONCOTER, CASTER, MEDIC,

	// Mage subclasses
	WARLOCK, POTIONMASTER, AMULETEER,

	// Ranger subclasses
	HAWKEYE, YEOMAN, QUICKDRAW,

	// Warrior subclasses
	DIVINITY, BLACKHEART, ILLUSION
}
