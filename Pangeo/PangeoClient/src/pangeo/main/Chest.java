package pangeo.main;

import java.util.ArrayList;
import java.util.List;

public class Chest 
{
	public String name;
	
	public static List<Chest> chests = new ArrayList<Chest>();
	
	public Chest(int x, int y, String n) 
	{	
		Main.world.metaId[x][y] = (short) chests.size();
		name = n;
		chests.add(this);
		// TODO Auto-generated constructor stub
	}
}
