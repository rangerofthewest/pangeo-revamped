package pangeo.main;

public class WorldGemAltar extends WorldGenerator {
	public int material;
	public Vector2 pos;

	public WorldGemAltar(byte gem, Vector2 p) {
		material = 36 + gem;
		pos = p;
	}

	public void generate(World w) {
		for (int a = 0; a < 5; a++) {
			if (!Tile.tiles[w.world[(int) pos.X + a][(int) pos.Y + 1]].solid
					|| Tile.tiles[w.world[(int) pos.X + a][(int) pos.Y - 1]].solid
					|| !Tile.tiles[w.world[(int) pos.X + a][(int) pos.Y - 2]].solid) {
				return;
			}
		}
		for (int a = 0; a < 5; a++) {
			w.world[(int) pos.X + a][(int) pos.Y] = (short) material;
			w.world[(int) pos.X + a][(int) pos.Y - 1] = (short) material;
			w.slopeStyle[(int) pos.X][(int) pos.Y - 1] = 3;
			w.slopeStyle[(int) pos.X + 4][(int) pos.Y - 1] = 4;
		}
	}

	public int genPriority() {
		return 0;
	}
}
