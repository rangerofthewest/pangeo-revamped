package pangeo.main;

public class ItemShield extends Item {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5144899900674681602L;
	public int defense;

	public ItemShield(String s, int d) {
		super(s);
		defense = d;
	}

	public void onSecondary(EntityLiving p, int x, int y, boolean leftSlot) {
		if (!p.isShieldCrouching) {
			p.isBlockingLeft = false;
			p.isBlockingRight = false;
			p.isShieldCrouching = true;
		} else {
			p.isShieldCrouching = false;
		}
	}
}
