package pangeo.main;

public class WorldDungeon extends WorldGenerator 
{
	public int x;
	public int y;
	public int floor = 0;
	
	public WorldDungeon(int posX, int posY) 
	{
		x = posX;
		y = posY;
	}
	
	public void generate(World w) 
	{
		int entryWay = (World.rand.nextInt(20) * 4) + 36;
		int actualSize = World.rand.nextInt(4) + 3;
		int dungeonHeight = 38 + World.rand.nextInt(40);
		
		for (int a = 0; a <= entryWay * actualSize; a++)
		{
			w.world[x + a][y + 1] = 107;
			w.slopeStyle[x + a][y + 1] = 0;
			for (int b = 0; b < 30; b++)
			{
				w.world[x + a][y - b] = 0;
				w.slopeStyle[x + a][y - b] = 0;
			}
			if (a % 4 == 0 && a <= entryWay)
			{
				w.world[x + a][y] = 109;
				//w.calcLighting(x + a, y);
			}
			else
			{
				if (a >= entryWay + 3)
				{
					//Tile.tiles[107].onPlaceTile(x + a, y - 3 - (dungeonHeight - 1), w);
				}
			}
		}
		//boolean addedFloor = false;
		for (int i = entryWay + 4; i < (entryWay * (actualSize - 0)); i++)
		{
			for (int j = 0; j < y - 3 - (dungeonHeight - 1); j++)
			{
				Tile.tiles[107].onPlaceTile(x + entryWay + 3, y - j, w);
				Tile.tiles[107].onPlaceTile(x + (entryWay * actualSize), y - j, w);
				if (((y - j) + 1) % 3 == 0 && ((x + i) + 1) % 5 == 0 && World.rand.nextFloat() <= 0.49f)
				{
					w.wall[x + i][y - j] = 110;
				}
				else
				{
					if (World.rand.nextFloat() < 0.999f)
					{
						w.wall[x + i][y - j] = 108;
					}
				}
				Tile.tiles[107].onPlaceTile(x + i, y - ((y - 3 - (dungeonHeight - 1)) - 1), w);
				if ((y - j) % 16 == 0 && j > 3)
				{
					w.world[x + i][y - j] = 107;
				}
				if (i == (entryWay * (actualSize - 0)) / 3 && j == 16)
				{
					for (int k = 0; k <= 16; k++)
					{
						w.world[x + i][y - j + k] = 107;
					}
				}
			}
			floor++;
		}
		Tile.tiles[61].onPlaceTile(x + (entryWay * (actualSize - 0)) / 3, y - 2, w);
		Tile.tiles[64].onPlaceTile(x + entryWay + 8, y, w);
		Tile.tiles[111].onPlaceTile(x + entryWay + 8, y - 1, w);
		Tile.tiles[66].onPlaceTile(x  + entryWay + 9, y - 1, w);
		w.slopeStyle[x + entryWay + 3][y - ((y - 3 - (dungeonHeight - 1)) - 1)] = 3;
		w.slopeStyle[x + (entryWay * (actualSize - 0))][y - ((y - 3 - (dungeonHeight - 1)) - 1)] = 4;
		Tile.tiles[61].onPlaceTile(x + entryWay + 3, y - 2, w);
		Util.print(floor + " floors in dungeon generated!");
	}
	public int genPriority() 
	{
		return 0;
	}
}
