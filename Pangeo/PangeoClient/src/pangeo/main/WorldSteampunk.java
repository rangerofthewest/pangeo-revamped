package pangeo.main;

import java.util.Random;

public class WorldSteampunk 
{
	public int x = 0;
	public int y = 0;
	
	public WorldSteampunk(int x, int y) 
	{
		this.x = x;
		this.y = y;
	}
	
	public void gen(World w)
	{
		Random rand = new Random();
		//First calculate first floor dimensions
		int width = rand.nextInt(400) + 200;
		int height = rand.nextInt(180) + 200;
		while (height % 20 != 0)
		{
			height++;
		}
		int roomInterval = 12;
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				if (i % roomInterval == 0 && !(j % 17 == 0) && !(j % 18 == 0) && !(j % 19 == 0))
				{
					w.world[x + i][y + j] = 0;
					Tile.tiles[164].onPlaceTile(x + i, y + j, w);
				}
				w.biome[x + i][y + j] = Biome.Steampunk;
				if (i == 0 || i == width - 1 || j == 0 || j == height - 1 || j % 20 == 0)
				{
					w.world[x + i][y + j] = 0;
					Tile.tiles[164].onPlaceTile(x + i, y + j, w);
					//Mess with room width!
					if (j % 20 == 0)
					{
						roomInterval = rand.nextInt(8) + 10;
					}
				}
				else
				{
					//Uber rare. Leave it as a "glitch"
					if (w.world[x + i][y + j] != 0 || rand.nextFloat() <= 0.99999999999f)
					{
						w.world[x + i][y + j] = 0;
					}
					else
					{
						w.world[x + i][y + j] = 0;
						if (rand.nextFloat() <= 0.75f)
						{
							Tile.tiles[164].onPlaceTile(x + i, y + j, w);
						}
						else
						{
							Tile.tiles[165].onPlaceTile(x + i, y + j, w);
						}
					}
					w.wall[x + i][y + j] = 164;
					
				}
			}
			Util.print("Generating steampunk frame: %" + ((x / width) * 100));
		}
	}
}
