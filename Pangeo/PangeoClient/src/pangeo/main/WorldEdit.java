package pangeo.main;

import java.awt.Font;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.TrueTypeFont;

public class WorldEdit 
{
	public static int keyCounter = 0;
	
	public static boolean drawGrid = true;
	public static boolean drawCamPos = false;
	public static boolean drawLiquids = true;
	public static boolean superSpeed = false;
	public static boolean drawInv = false;
	public static boolean drawSlopes = true;
	
	public static TrueTypeFont font = Content.loadFont("font", Font.PLAIN, 10);
	public static TrueTypeFont fontItalic = Content.loadFont("font", Font.ITALIC, 10);
	
	public static void checkKeys(EntityPlayer p)
	{
		if (isKeyPressCtrl(Keyboard.KEY_G)) { drawGrid = !drawGrid; keyCounter = 8; }
		if (isKeyPressCtrl(Keyboard.KEY_Q)) { drawCamPos = !drawCamPos; keyCounter = 8; }
		if (isKeyPressCtrl(Keyboard.KEY_E)) { superSpeed = !superSpeed; keyCounter = 8; }
		if (isKeyPressCtrl(Keyboard.KEY_R)) { p.noClip = !p.noClip; keyCounter = 8; }
		if (isKeyPressCtrl(Keyboard.KEY_T)) { drawLiquids = !drawLiquids; keyCounter = 8; }
		if (isKeyPressCtrl(Keyboard.KEY_I)) { drawInv = !drawInv; keyCounter = 8; }
		//if (isKeyPressCtrl(Keyboard.KEY_F)) { drawSlopes = !drawSlopes; keyCounter = 8; }
	}
	
	public static boolean isKeyPress(int i)
	{
		return Keyboard.isKeyDown(i) && keyCounter < 1;
	}
	
	public static boolean isKeyPressCtrl(int i)
	{
		return (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL)) && Keyboard.isKeyDown(i) && keyCounter < 1;
	}
}
