package pangeo.main;

//import org.lwjgl.input.Keyboard;
import org.newdawn.slick.opengl.Texture;

public class EntityPlayer extends EntityLiving {
	public Inventory inventory;
	public int currentFrame = 0;
	public int archons = 0; // 0
	public boolean adjustableRender = false;
	public boolean canSki = false; // false
	public float renderMax = 0.25f; // 0.0f means you can't see more than that
									// distance from the player
	public float stealthCounter = 0.0f; // increases by 0.0165f every tick
	public float stealthSpeed = 0.0165f;
	public int shadewalkCounter = 0;
	public boolean shadeWalk = false;
	public boolean destabilized = false;
	public boolean canRead = false; // false
	public boolean glow = false;
	public boolean neitherLand = false; // false (Esundra)
	public boolean survival = true;
	public double maxHunger = 100.0;
	public double currHunger = 100.0;
	public double currTemp = 75.0;
	public double maxTemp = 135.0;
	public double currFatigue = 100.0;
	public double maxFatigue = 100.0;
	public Texture[] frames = new Texture[16];

	public Vector2 spawnPoint = new Vector2(15000, 800);

	public Journal journal = new Journal();

	public Item useItem;
	public double itemAngle = 90.0;
	public Vector2 targetPos;

	public long xp = 0;
	public long reqXp = 10;
	public long skillPoints = 0;
	public long level = 0;

	public SkillMaster skills;
	
	public Vector2 tent = null;

	public EntityPlayer(String _name) {
		super(_name);
		this.mana = 8;
		this.maxMana = 8;
		this.health = 100;
		this.maxHealth = 100;
		this.inventory = new Inventory(this);
		this.pos = new Vector2(15000, 800); // 15000, 800
		this.moveSpeed = 0.35f;
		this.noClip = true;
		// this.glow = true;
		this.canStealth = false;
		this.journal.addEntry("Thoughts",
				"Where am I? What is this place? WHO am I?", 0);
		this.journal.addEntry("Ponderings",
				"What am I? And most importantly...what am I doing here?", 1);
		this.skills = new SkillMaster(this);
		// this.inventory.addToInventory(Item.items[0], 3);
	}

	public void addMoney(int x) {
		// archons += x;
		Main.coinsDraw += x;
	}

	public void moveLeft(float speed) {
		if (!Tile.tiles[Main.world.world[(int) (pos.X - speed) - 1][(int) pos.Y]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X - speed) - 1][(int) pos.Y - 1]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X - speed) - 1][(int) pos.Y + 1]].solid) {
			pos.X -= speed;
		}
		if (Tile.tiles[Main.world.world[(int) (pos.X - speed - 1)][(int) pos.Y + 1]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X - speed - 1)][(int) pos.Y - 1]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X - speed - 1)][(int) pos.Y]].solid) {
			pos.X -= speed;
			pos.Y -= 1.0f;
		}
	}

	public void moveRight(float speed) {
		if (!Tile.tiles[Main.world.world[(int) (pos.X + speed)][(int) pos.Y]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X + speed)][(int) pos.Y - 1]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X + speed)][(int) pos.Y + 1]].solid) {
			pos.X += speed;
		}
		if (Tile.tiles[Main.world.world[(int) (pos.X + speed)][(int) pos.Y + 1]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X + speed)][(int) pos.Y - 1]].solid
				&& !Tile.tiles[Main.world.world[(int) (pos.X + speed)][(int) pos.Y]].solid) {
			pos.X += speed;
			pos.Y -= 1.0f;
		}
	}

	public void onUpdate() {
		super.onUpdate();
		if (xp >= reqXp) {
			skillPoints += 0.18 * (float) reqXp;
			xp -= reqXp;
			reqXp += (0.18 * (float) reqXp);
			level++;
			Util.print("XP is now " + reqXp + " and you have "
					+ skillPoints + " skill points. You have maxed out "
					+ level + " times!");
		}
	}

	public void onDeath() {
		super.onDeath();
	}
}
