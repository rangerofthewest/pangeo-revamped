package pangeo.main;

public class ItemArmor extends Item 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3809324250424178880L;
	public String armorSet = "";
	public EnumArmorSlot armorSlot;
	public EnumArmorSetEffect armorEffect;
	public int defense;
	
	public ItemArmor(String s, String a, int d, EnumArmorSlot e, EnumArmorSetEffect f) 
	{
		super(s);
		armorSet = a;
		armorSlot = e;
		armorEffect = f;
		defense = d;
	}
	

}
