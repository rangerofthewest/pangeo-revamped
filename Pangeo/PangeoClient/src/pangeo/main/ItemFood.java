package pangeo.main;

public class ItemFood extends Item 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3373848847025438004L;

	public ItemFood(String s, double d) 
	{
		super(s);
		this.foodRestore = d;
		this.food = true;
		this.consumable = true;
	}
}
