package pangeo.main;

public class Inventory 
{
	public Entity owner;
	
	//This is the main inventory
	public Item[][] items = new Item[10][5];
	public int[][] stack = new int[10][5];
	
	//Backpack slots
	public Item[][] backPack0 = new Item[10][1];
	public Item[][] backPack1 = new Item[10][1];
	public Item[][] backPack2 = new Item[10][1];
	public int[][] stackPack0 = new int[10][1];
	public int[][] stackPack1 = new int[10][1];
	public int[][] stackPack2 = new int[10][1];
	
	public boolean unlocked0 = false; //unlocked pack 0
	public boolean unlocked1 = false; //unlocked pack 1
	public boolean unlocked2 = false; //unlocked pack 2
	
	public Inventory(Entity e) 
	{
		this.owner = e;
	}
	
	public boolean hasItem(Item i, int min)
	{
		int amt = 0;
		for (int y = 0; y < 5; y++)
		{
			for (int x = 0; x < 10; x++)
			{
				if (items[x][y] == i || Item.items[items[x][y].owner] == i)
				{
					amt += stack[x][y];
				}
			}
		}
		return amt >= min;
	}
	
	public void removeItem(Item i, int amt)
	{
		int removed = 0;
		for (int y = 0; y < 5; y++)
		{
			for (int x = 0; x < 10; x++)
			{
				if (removed < amt)
				{
					if (items[x][y] == i)
					{
						if ((amt - removed) >= stack[x][y])
						{
							removed += amt - stack[x][y];
							stack[x][y] = 0;
							items[x][y] = null;
						}
						if (stack[x][y] >= (amt - removed))
						{
							stack[x][y] -= (amt - removed);
							removed += (amt - removed);
						}
					}
				}
			}
		}
	}
	
	public void addToInventory(Item i, int s)
	{
		if (s <= 0)
		{
			return;
		}
		for (int y = 0; y < 5; y++)
		{
			for (int x = 0; x < 10; x++)
			{
				if (items[x][y] == null)
				{
					items[x][y] = i;
					stack[x][y] = s;
					return;
				}
				if (items[x][y].id == i.id && s <= items[x][y].maxStack - stack[x][y])
				{
					int maxAmt = items[x][y].maxStack - stack[x][y];
					stack[x][y] += Math.min(s, maxAmt);
					if (s > maxAmt)
					{
						addToInventory(i, s - maxAmt);
					}
					else
					{
						//Item.newItem(i.id, (int)owner.pos.X, (int)owner.pos.Y, s - maxAmt);
						return;
					}
				}
			}
		}
	}
	
	public void clear()
	{
		items = new Item[10][5];
		stack = new int[10][5];
		backPack0 = new Item[10][1];
		backPack1 = new Item[10][1];
		backPack2 = new Item[10][1];
		stackPack0 = new int[10][1];
		stackPack1 = new int[10][1];
		stackPack2 = new int[10][1];
	}
}
