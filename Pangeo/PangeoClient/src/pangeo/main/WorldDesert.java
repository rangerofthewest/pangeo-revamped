package pangeo.main;

public class WorldDesert extends WorldGenerator
{
	
	public WorldDesert()
	{
	}
	
	public void generate(World w)
	{
		int xpos;
		if (World.rand.nextBoolean())
		{
			xpos = 14750 - World.rand.nextInt(1500);
		}
		else
		{
			xpos = 15250 + World.rand.nextInt(1500);
		}
		w.world[xpos][800] = 23;
		//new WorldBlob(2500, 9000, 23).gen(w, xpos, 800);
	}
	
	public int genPriority()
	{
		return 0; //highest priority
	}
}
