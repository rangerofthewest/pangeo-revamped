package pangeo.main;

public class Biome
{
	public static short maxBiomes = 6;
	
	public static short Forest = 0;
	public static short Snow = 1;
	public static short Desert = 2;
	public static short Steampunk = 3;
	public static short Underground = 4;
	public static short Hell = 5;
	
	public static short[] biomesNotAllowed = 
		{
			Biome.Steampunk,
			Biome.Underground,
			Biome.Hell
		};
	
	public static boolean isBiomeAllowed(short s)
	{
		for (int x = 0; x < biomesNotAllowed.length; x++)
		{
			if (s == biomesNotAllowed[x])
			{
				return false;
			}
		}
		return true;
	}
}
