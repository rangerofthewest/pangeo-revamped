package pangeo.main;

public class TileTent extends Tile 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7535767109142673963L;

	public TileTent(String s, int i) 
	{
		super(s, i);
	}
	
	public void onActivate(int x, int y, World w, EntityPlayer p)
	{
		if (p.survival)
		{
			p.tent = new Vector2(x - w.xSlot[x][y], y - w.ySlot[x][y]);			
			p.pos = p.tent;
		}
	}
}
