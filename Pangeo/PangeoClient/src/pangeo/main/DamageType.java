package pangeo.main;

public enum DamageType 
{
	//MELEE + RANGED DAMAGE TYPES
	STAB, //Stabbing damage type, daggers and knives
	SLASH, //Slashing damage, swords
	PROJECTILE, //Projectile damage, throwing knives, bows, crossbows
	PIERCE, //Piercing damage, spears, polearms, halberds
	SMASH, //Smashing damage, hammers, flails, maces
	PANGEON //Pangeon weapons
}
