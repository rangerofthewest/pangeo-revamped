package pangeo.main;

public class TileCampfire extends Tile 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5776481739180266252L;
	public static int burnTime = 14400;
	public static int maxBurnTime = 288000;
	public static double maxTemp = 450;
	public static double baseTemp = 85;
	
	public boolean active = false;
	public TileCampfire(String s, int i, boolean b) 
	{
		super(s, i);
		active = b;
		if (this.address == 157)
		{
			this.animationFrames = 4;
			this.animationLength = 3;
		}
	}
	
	public void onPlaceTile(int x, int y, World w)
	{
		super.onPlaceTile(x, y, w);
		if (Main.player.survival)
		{
			w.metaId[x][y] = 0;
		}
		else
		{
			Tile.tiles[157].onOverwriteTile(x - w.xSlot[x][y], y - w.ySlot[x][y], w);
		}
		//You need a match, a torch, or a lighter as well as some fuel
	}
	
	public void onActivate(int x, int y, World w, EntityPlayer p)
	{
		if (p.survival)
		{
			if (p.inventory.stack[Main.selectedSlot][0] > 0)
			{
				Util.print("Click recieved at " + x + "/" + y + " with slot " + Main.selectedSlot + " selected!");
				if (true)
				{
					//Wood (moderate fuel, moderate temperature)
					if (p.inventory.items[Main.selectedSlot][0] == Item.items[1042] || p.inventory.items[Main.selectedSlot][0].owner == 1042)
					{
						p.inventory.stack[Main.selectedSlot][0]--;
						if (p.inventory.stack[Main.selectedSlot][0] == 0)
						{
							p.inventory.items[Main.selectedSlot][0] = null;
						}
						w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] += 250;
						w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] += 0.4; //0.4 degree increase every piece of wood
						if (w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] > maxTemp)
						{
							w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] = maxTemp;
						}
						if (w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] > maxBurnTime)
						{
							w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] = maxBurnTime;
						}
						Util.print("Fuel added!");
					}
					
					//Brush (more fuel, less temperature)
					if (p.inventory.items[Main.selectedSlot][0] == Item.items[1594] || p.inventory.items[Main.selectedSlot][0] == Item.items[1595] || p.inventory.items[Main.selectedSlot][0] == Item.items[1596])
					{
						p.inventory.stack[Main.selectedSlot][0]--;
						if (p.inventory.stack[Main.selectedSlot][0] == 0)
						{
							p.inventory.items[Main.selectedSlot][0] = null;
						}
						w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] += 500;
						w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] += 0.1; //0.1 degree increase every piece of wood
						if (w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] > maxTemp)
						{
							w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] = maxTemp;
						}
						if (w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] > maxBurnTime)
						{
							w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] = maxBurnTime;
						}
						Util.print("Fuel added!");
					}
					 
					//Pitch (less fuel, more temperature)
					if (p.inventory.items[Main.selectedSlot][0] == Item.items[1597])
					{
						p.inventory.stack[Main.selectedSlot][0]--;
						if (p.inventory.stack[Main.selectedSlot][0] == 0)
						{
							p.inventory.items[Main.selectedSlot][0] = null;
						}
						w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] += 50;
						w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] += 1.2; //0.8 degree increase every piece of wood
						if (w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] > maxTemp)
						{
							w.altMetaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] = maxTemp;
						}
						if (w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] > maxBurnTime)
						{
							w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] = maxBurnTime;
						}
						Util.print("Fuel added!");
					}
				}
			}
			else
			{	
				if (w.metaId[x - w.xSlot[x][y]][y - w.ySlot[x][y]] > 0)
				{
					if (w.world[x][y] == 157)
					{
						Tile.tiles[158].onOverwriteTile(x - w.xSlot[x][y], y - w.ySlot[x][y], w);
					}
					else
					{	
						Tile.tiles[157].onOverwriteTile(x - w.xSlot[x][y], y - w.ySlot[x][y], w);
						if (w.altMetaId[x][y] <= baseTemp / 2)
						{
							w.altMetaId[x][y] = baseTemp * 0.65;
						}
						w.altMetaId[x][y] = baseTemp + (0.15 * baseTemp);
					}
				}
			}
		}
	}
}
