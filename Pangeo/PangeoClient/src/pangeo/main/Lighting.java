package pangeo.main;

public class Lighting extends Thread
{
	public int lightingCheck = 100;
	public void run()
	{
		while (true)
		{
			for (int x = -lightingCheck; x < lightingCheck; x++)
			{
				for (int y = -lightingCheck; y < lightingCheck; y++)
				{
					try
					{
						calcLighting((int)Main.player.pos.X + x, (int)Main.player.pos.Y + y, Main.world);
					}
					catch (Exception e)
					{
						
					}
				}
			}
		}
	}
	@SuppressWarnings("unused")
	public void calcLighting(int i, int j, World w)
	{
		if (i < 0 || i >= w.size_x || j < 0 || j >= w.size_y)
		{
			return;
		}
		double light = Math.abs(Tile.tiles[w.world[i][j]].lightIntensity);
		double adjLight = Tile.tiles[w.world[i][j]].lightIntensity;
		//int flickerSetting = rand.nextInt(4) + 1;
		try
		{
			byte cR = (byte) (Tile.tiles[w.world[i][j]].lightColor.r * 255);
			byte cG =  (byte) (Tile.tiles[w.world[i][j]].lightColor.g * 255);
			byte cB = (byte) (Tile.tiles[w.world[i][j]].lightColor.b * 255);
			if (w.world[i][j] == 0 && w.liquid[i][j] != -1)
			{
				light = Liquid.liquids[w.liquid[i][j]].lightIntensity;
			}
			for (double a = -light; a < light; a++)
			{
				for (double b = -light; b < light; b++)
				{
					if (new Vector2(i + a, j + b).distanceTo(new Vector2(i, j)) <= light)
					{
						if (adjLight >= 0)
						{
							if ((byte) (Main.lightingShades - (byte)((new Vector2(i + a, j + b).distanceTo(new Vector2(i, j)) / light) * Main.lightingShades)) != w.lighting[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))])
							{
								if (Tile.tiles[w.world[i][j]].lightFlicker && 2 == 1)
								{
									if (World.rand.nextInt(200) == 0)
									{
										w.lighting[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = (byte)((Math.max(w.lighting[(int)(i + a)][(int)(j + b)], Math.max(10, (byte) (Main.lightingShades - (byte)((new Vector2(i + a, j + b).distanceTo(new Vector2(i, j)) / light) * Main.lightingShades))))) / 1);
									}
									else
									{
										w.lighting[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = (byte)Math.max(w.lighting[(int)(i + a)][(int)(j + b)], Math.max(10, (byte) (Main.lightingShades - (byte)((new Vector2(i + a, j + b).distanceTo(new Vector2(i, j)) / light) * Main.lightingShades))));
									}
								}
								else
								{
									w.lighting[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = (byte)Math.max(w.lighting[(int)(i + a)][(int)(j + b)], Math.max(10, (byte) (Main.lightingShades - (byte)((new Vector2(i + a, j + b).distanceTo(new Vector2(i, j)) / light) * Main.lightingShades))));
								}
								w.lightR[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cR;
								w.lightG[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cG;
								w.lightB[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cB;
							}
						}
						else
						{
							if ((byte) ((byte)((new Vector2(i + a, j + b).distanceTo(new Vector2(i, j)) / light) * Main.lightingShades)) != w.lighting[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))])
							{
								w.lighting[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = (byte)Math.abs(Math.max(w.lighting[(int)(i + a)][(int)(j + b)], Math.max(10, (byte) ((byte)((new Vector2(i + a, j + b).distanceTo(new Vector2(i, j)) / light) * Main.lightingShades)))) - Main.lightingShades);
								w.lightR[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cR;
								w.lightG[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cG;
								w.lightB[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cB;
							}
						}
					}
					w.lightR[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cR;
					w.lightG[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cG;
					w.lightB[Math.max((int)light, Math.min((int)(i + a), w.size_x - (int)light))][Math.max((int)light, Math.min((int)(j + b), w.size_x - (int)light))] = cB;
				}
			}
			if (j >= 775 && j <= 815 && light == 0.0 && !w.isCalcLighting(i, j))
			{
				if (j >= 795)
				{
					w.lighting[i][j] = (byte) (Math.abs(815 - j) * World.deviationLight);
				}
				else
				{
					w.lighting[i][j] = (byte) (Math.abs(795 - j) * World.deviationLight);
				}
			}
		}
		catch (Exception e)
		{
			return;
		}
		for (int x = -10; x < 10; x++)
		{
			for (int y = -10; y < 10; y++)
			{
				if (Main.player.glow && Main.player.pos.distanceTo(new Vector2(Main.player.pos.X + x, Main.player.pos.Y + y)) < 10)
				{
					w.lighting[(int)Main.player.pos.X + x][(int)Main.player.pos.Y + y] = (byte)Math.max(w.lighting[(int)(Main.player.pos.X + x)][(int)(Main.player.pos.Y + y)], Math.max(10, (byte) (Main.lightingShades - (byte)((new Vector2(Main.player.pos.X + x, Main.player.pos.Y + y).distanceTo(new Vector2(x, y)) / light) * Main.lightingShades))));
				}
			}
		}
	}
}
