package pangeo.main;

public class WorldWizardTower extends WorldGenerator 
{	
	public short material;
	public short wall;
	public short platform;
	public Vector2 pos;
	
	public static int size = 50;
	public static int stairCase = 7;
	public static int stairsInFlight = 10;
	public static int stairFlights = 12;
	
	public WorldWizardTower(byte gem, Vector2 p) 
	{
		material = (short) (36 + gem);
		wall = (short) (45 + World.rand.nextInt(8));
		platform = (short) (53 + World.rand.nextInt(8)); 
		pos = p;
	}
	
	public int chooseRandBook()
	{
		int[] validBooks = 
			{
				66,
				70,
				71,
				73,
				75,
				77,
				78,
				79,
				80,
				81,
				82
			};
		return validBooks[World.rand.nextInt(validBooks.length)];
	}
	
	public void generate(World w) 
	{
		//This generates the base
		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < stairCase; y++)
			{
				if (x >= y && x < size - 1 - y)
				{
					w.world[(int)pos.X + x][(int)pos.Y - y] = material;
					w.slopeStyle[(int)pos.X + x][(int)pos.Y - y] = 0;
					w.slopeStyle[(int)pos.X + y][(int)pos.Y - y] = 3;
					w.slopeStyle[(int)pos.X + (size - 1 - y - 1)][(int)pos.Y - y] = 4;
				}
			}
		}
		Tile.tiles[64].onPlaceTile((int)pos.X + ((size - stairCase - 2) / 2) - 2, (int)pos.Y - stairCase, w);
		if (World.rand.nextBoolean())
		{
			w.world[(int)pos.X + ((size - stairCase - 2) / 2) - 2][(int)pos.Y - stairCase - 1] = (short) chooseRandBook();
		}
		if (World.rand.nextBoolean())
		{
			w.world[(int)pos.X + ((size - stairCase - 2) / 2) - 1][(int)pos.Y - stairCase - 1] = (short) chooseRandBook();
		}
		w.world[(int)pos.X + ((size - stairCase - 2) / 2)][(int)pos.Y - stairCase] = platform;
		for (int a = 0; a < stairFlights - 2; a++)
		{
			for (int b = 0; b < stairsInFlight; b++)
			{
				if (a % 2 == 0)
				{
					w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b + 1][(int)pos.Y - stairCase - ((stairsInFlight * a) + b) - 1] = platform;
					if (b == stairsInFlight - 1)
					{
						for (int c = stairCase; c < size - stairCase - 2; c++)
						{
							w.world[(int)pos.X + c][(int)pos.Y - stairCase - ((stairsInFlight * a) + b) - 1] = material;
						}
						w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b - 4][(int)pos.Y - stairCase - ((stairsInFlight * a) + b) - 1] = platform;
						w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b - 3][(int)pos.Y - stairCase - ((stairsInFlight * a) + b) - 1] = platform;
						w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b - 2][(int)pos.Y - stairCase - ((stairsInFlight * a) + b) - 1] = platform;
						w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b - 1][(int)pos.Y - stairCase - ((stairsInFlight * a) + b) - 1] = platform;
						w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b - 0][(int)pos.Y - stairCase - ((10 * a) + b) - 1] = platform;
						w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b + 1][(int)pos.Y - stairCase - ((10 * a) + b) - 1] = platform;
						//w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b][(int)pos.Y - stairCase - ((10 * a) + b) - 1] = 44;
						//w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b - 1][(int)pos.Y - stairCase - ((10 * a) + b) - 1] = 44;
						//w.world[(int)pos.X + ((size - stairCase - 2) / 2) + b - 2][(int)pos.Y - stairCase - ((10 * a) + b) - 1] = 44;
					}
				}
				else
				{
					if (a < stairFlights - 2)
					{
						w.world[(int)pos.X + ((size - stairCase - 2) / 2) + (stairsInFlight - b - 1)][(int)pos.Y - stairCase - ((stairsInFlight * a) + b) - 1] = platform;
					}
				}
			}
		}
		for (int j = stairCase; j < (int)(stairsInFlight * (stairFlights - 1)); j++)
		{
			for (int i = stairCase + 1; i < size - stairCase - 2; i++)
			{
				w.wall[(int)pos.X + i][(int)pos.Y - j] = wall;
			}
		}
		for (int i = stairCase; i < (int)(stairsInFlight * (stairFlights - 1)); i++)
		{
			w.world[(int)pos.X + stairCase][(int)pos.Y - i] = material;
			w.world[(int)pos.X + size - stairCase - 2][(int)pos.Y - i] = material;
			if (i == (int)(stairsInFlight * (stairFlights - 1)) - 1)
			{
				for (int j = stairCase; j < size - stairCase - 2; j++)
				{
					w.world[(int)pos.X + j][(int)pos.Y - i] = material;
					if (j == 0)
					{
						w.slopeStyle[(int)pos.X + j][(int)pos.Y - i] = 3; 
					}
					if (j ==  size - stairCase - 3)
					{
						w.slopeStyle[(int)pos.X + j][(int)pos.Y - i] = 4;
					}
				}
			}
			Tile.tiles[61].onPlaceTile((int)pos.X + stairCase, (int)pos.Y - stairCase - 2, w);
			Tile.tiles[61].onPlaceTile((int)pos.X + size - stairCase - 2, (int)pos.Y - stairCase - 2, w);
		}
	}

	public int genPriority() 
	{
		return 0;
	}
}
