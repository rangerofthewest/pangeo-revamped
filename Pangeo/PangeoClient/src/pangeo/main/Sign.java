package pangeo.main;

import java.util.ArrayList;
import java.util.List;

public class Sign 
{
	public Vector2 pos;
	public String msg = "This is a sign! -Mr. Signy";
	
	public static List<Sign> signs = new ArrayList<Sign>();
	
	public Sign(String text, Vector2 p, World w) 
	{
		msg = text;
		pos = p;
		w.metaId[(int)p.X][(int)p.Y] = (short) Math.min(signs.size(), 64000);
		signs.add(this);
		Tile.tiles[63].onPlaceTile((int)p.X, (int)p.Y, w);
	}
}
