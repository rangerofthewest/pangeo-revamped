package pangeo.main;

import java.util.Random;

//This just generates a blob o' ore
public class WorldBlob 
{
	public int min_size = 3;
	public int max_size = 8;
	public int material = 2; 
	public int wall = -1;
	
	public static Random rand = new Random();
	
	public WorldBlob(int _min, int _max, int _mat, int _wal)
	{
		min_size = _min;
		max_size = _max;
		material = _mat;
		wall = _wal;
	}
	
	public void gen(World w, int x, int y)
	{
		w.world[x][y] = (short)material;
		int x_c = 0;
		int y_c = 0;
		int size = (rand.nextInt(min_size) + ((max_size - min_size) + 1));
		
		for (int a = 0; a < size; a++)
		{
			if (rand.nextBoolean())
			{
				if (rand.nextBoolean())
				{
					x_c++;
				}
				else
				{
					y_c++;
				}
			}
			else
			{
				if (rand.nextBoolean())
				{
					x_c--;
				}
				else
				{
					y_c--;
				}
			}
			while (x + x_c < 0)
			{
				x_c++;
			}
			while (x + x_c >= w.size_x)
			{
				x_c--;
			}
			while (y + y_c < 0)
			{
				y_c++;
			}
			while (y + y_c >= w.size_y)
			{
				y_c--;
			}
			try
			{
				if (w.world[x + x_c][y + y_c] != (short)material)
				{
					w.world[x + x_c][y + y_c] = (short)material;
				}
				else
				{
					//a--;
				}
				if (wall > -1)
				{
					w.wall[x + x_c][y + y_c] = (short)wall;
				}
			}
			catch (ArrayIndexOutOfBoundsException e)
			{
				a--;
			}
			
			//Util.print("Generating blob of ID #" + material + ": %" + (((float)a / size) * 100));
		}
	}
}
