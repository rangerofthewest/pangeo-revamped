package pangeo.main;

import org.newdawn.slick.Color;

public class ItemPaint extends Item 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7846286272105100686L;
	public Color paintColor;
	
	public ItemPaint(String s, Color c)
	{
		super("Paint (" + s + ")");
		paintColor = c;
		this.maskColor = c;
	}
}
