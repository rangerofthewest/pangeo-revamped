package pangeo.main;

import pangeo.main.EntityPlayer;

public class SkillMaster 
{
	public EntityPlayer owner;
	
	public int skillMisc_crafting = 0; //Crafting skill
	public int skillMisc_brewing = 0; //Brewing skill
	public int skillMisc_cooking = 0; //Cooking skill
	
	public SkillMaster(EntityPlayer p) 
	{
		owner = p;
	}
	
}
