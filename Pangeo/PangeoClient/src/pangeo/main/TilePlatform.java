package pangeo.main;

public class TilePlatform extends Tile {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7740767274749593822L;

	public TilePlatform(String s, int i) 
	{
		super(s, i);
		this.solid = false;
		this.solidTop = true;
	}
}
