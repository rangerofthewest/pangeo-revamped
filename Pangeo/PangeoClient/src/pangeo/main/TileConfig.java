package pangeo.main;

public class TileConfig 
{
	//Spritesheet mappings
	public static int normalTileX = 0; //Base tile, unaffected
	public static int normalTileY = 0;
	
	public static int dirtLeftX = 0; //Base tile, dirt blending
	public static int dirtLeftY = 1;
	
	public static int dirtRightX = 0; //Base tile, dirt blending
	public static int dirtRightY = 2;
	
}
