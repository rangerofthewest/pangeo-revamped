package pangeo.main;

public enum EnumArmorSetEffect 
{
	//Rogue Armor effects
	STEALTH,
	FASTSTEALTH_FOREST,
	FASTSTEALTH_ICE,
	IMMUNE_POISON,
	SHORT_PHASE,
	LONG_PHASE,
	SHADEWALK,
	
	//Misc
	NONE
}
