package pangeo.main;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.lwjgl.Sys;

public class FTPHandler {
	public static int currentBuild = 46;
	public static String server = "www.creativeplusstudios.com";
	public static String userName = "creativ3";
	public static String password = "pangeo";
	public static String file = Config.autoUpdate_Branch + "/" + (currentBuild + 1) + ".jar";
	public static String saveDir = "updates";

	private static final int BUFFER_SIZE = 4096;

	private static void extractFile(ZipInputStream in, File outdir, String name)
			throws IOException {
		byte[] buffer = new byte[BUFFER_SIZE];
		BufferedOutputStream out = new BufferedOutputStream(
				new FileOutputStream(new File(outdir, name)));
		int count = -1;
		while ((count = in.read(buffer)) != -1)
			out.write(buffer, 0, count);
		out.close();
	}

	public static void mkdirs(File outdir, String path) {
		File d = new File(outdir, path);
		if (!d.exists())
			d.mkdirs();
	}

	private static String dirpart(String name) {
		int s = name.lastIndexOf(File.separatorChar);
		return s == -1 ? null : name.substring(0, s);
	}

	public static void extract(File zipfile, File outdir) {
		try {
			ZipInputStream zin = new ZipInputStream(
					new FileInputStream(zipfile));
			ZipEntry entry;
			String name, dir;
			while ((entry = zin.getNextEntry()) != null) {
				name = entry.getName();
				if (entry.isDirectory()) {
					mkdirs(outdir, name);
					continue;
				}
				/*
				 * this part is necessary because file entry can come before
				 * directory entry where is file located i.e.: /foo/foo.txt
				 * /foo/
				 */
				dir = dirpart(name);
				if (dir != null)
					mkdirs(outdir, dir);

				extractFile(zin, outdir, name);
			}
			zin.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private static String OS = System.getProperty("os.name").toLowerCase();
	public static boolean isWindows() {
		 
		return (OS.indexOf("win") >= 0);
 
	}
 
	public static boolean isMac() {
 
		return (OS.indexOf("mac") >= 0);
 
	}
	
	public static boolean downloadUpdate(int build) throws Exception {
		String os = OS;
		String fileName = build + "";
		String extension = "zip";
		if (isMac())
		{
			fileName = "mac/m_" + fileName + ".zip";
		}
		else if (isWindows())
		{
			fileName = "windows/w_" + fileName + ".zip";
		}
		else
		{
			throw new Exception("Not a valid OS!");
		}
		try {
			Sys.alert("Update Checker", "Attempting to download build " + build + " for " + os);
			URL url = new URL("ftp://" + userName + ":" + password + "@"
					+ server + "/public_html/pangeoBuilds/" + fileName
					+ ";type=i");
			// URL url = new
			// URL("ftp://"+userName+":"+password+"@"+server+"/"+fileName+";type=i");
			URLConnection con = url.openConnection();

			BufferedInputStream in = new BufferedInputStream(
					con.getInputStream());

			Util.print("Downloading file.");

<<<<<<< HEAD
			FileInputStream fis = new FileInputStream(new File(localFile));
			java.io.OutputStream os = urlc.getOutputStream();
			
=======
			// Downloads the selected file to the C drive
			new File("./updates/" + build).mkdir();
			FileOutputStream out = new FileOutputStream("./updates/" + OS + " build " + build + "." + extension);
>>>>>>> origin/rangerofthewest

			int i = 0;
			byte[] bytesIn = new byte[1024];
			while ((i = in.read(bytesIn)) >= 0) {
				out.write(bytesIn, 0, i);
			}
			out.close();
			in.close();
			Util.print("File downloaded.");
			Sys.alert("Update Checker",
					"Update downloaded to update directory! Installing update now...");
			try {
				// extract(new File("./updates/" + fileName), new
				// File(fileName.replace(".zip", "")));
				Sys.alert("Update Installer", "Update installed!");
				return true;
			} catch (Exception e) {
				Sys.alert("Update Installer",
						"Update could not be installed properly!");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Util.print("No update found");
			Sys.alert("Update Checker", "No update found!");
			return false;
		}
	}

	public static void attemptUpdate() {
		Sys.alert("Update Checker", "Attempting to update Pangeo...");
		Util.print("Attempting update...");
		Util.print("Connecting to FTP server...");
		// Connection String
		int currBuild = currentBuild + 1;
		try {
			while (downloadUpdate(currBuild)) {
				currBuild++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}