package pangeo.main;

public class PotionHandler 
{
	public EnumEffect[] effect = new EnumEffect[15];
	
	public void addEffect(EnumEffect e, int time)
	{
		for (int x = 0; x < effect.length; x++)
		{
			if (effect[x] == null)
			{
				effect[x] = e.setTime(time);
				return;
			}
			else if (effect[x].isSame(e))
			{
				effect[x].addTime(time);
				return;
			}
		}
		effect[effect.length - 1] = e.setTime(time);
	}
	
	public void onUpdate()
	{
		for (int x = 0; x < effect.length; x++)
		{
			effect[x].onUpdate();
		}
	}
	
	public void clear()
	{
		
	}
}
