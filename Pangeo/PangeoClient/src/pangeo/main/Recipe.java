package pangeo.main;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.Sys;

public class Recipe implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3445494716096942556L;
	public Tile craftingTable;
	public Item[] ingredients;
	public int[] ingredientAmt;
	public Item itemProduced;
	public int amtProduced;
	public int difficulty;
	
	public static List<Recipe> recipes = new ArrayList<Recipe>();
	
	public static void exportRecipes()
	{
		OutputStream file;			
		int amt = 0;
		try {
			file = new FileOutputStream("recipes.recipepkg");
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);
			//output.write(amt); //How many items to load
			for (int x = 0; x < recipes.size(); x++)
			{
				try
				{
					output.writeObject(recipes.get(x));
					Util.print("Recipe #" + x + " exported to recipe package");
					amt++;
				}
				catch (Exception e)
				{
					Sys.alert("Recipe Package", x + " recipes exported to recipe package");
					break;
				}
			}
			output.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sys.alert("Export", "Export complete! (" + amt + " recipes exported)");
	}
	
	public Recipe(Tile t, Item[] i, int[] stack, Item pr, int aP, int d) 
	{
		craftingTable = t;
		ingredients = i;
		ingredientAmt = stack;
		itemProduced = pr;
		amtProduced = aP;
		difficulty = d;
		if (Main.player.survival)
		{
			for (int x = 0; x < ingredientAmt.length; x++)
			{
				ingredientAmt[x] *= 1.25; //25% more required
				if (ingredients[x] == Item.items[1042])
				{
					ingredientAmt[x] *= 1.15; //an additional 15% more wood required
				}
			}
			amtProduced *= 1.05; //5% more produced
		}
	}
	
	public static void init()
	{
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[506] }, new int[] { 3 }, Item.items[842], 1, 1));		
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[509] }, new int[] { 3 }, Item.items[853], 1, 1));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[516] }, new int[] { 3 }, Item.items[850], 1, 1));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[518] }, new int[] { 3 }, Item.items[848], 1, 1));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[845], Item.items[946] }, new int[] { 5, 1 }, Item.items[948], 5, 0));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[845], Item.items[947] }, new int[] { 5, 1 }, Item.items[948], 5, 0));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040] }, new int[] { 4 }, Item.items[1041], 1, 1));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[1443], Item.items[845] }, new int[] { 15, 8 }, Item.items[1043], 1, 0));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 33 }, Item.items[858], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 27 }, Item.items[950], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 30 }, Item.items[1050], 1, 1));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[1042] }, new int[] { 3 }, Item.items[845], 5, 0));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040], Item.items[845] }, new int[] { 30, 8 }, Item.items[860], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040], Item.items[845] }, new int[] { 27, 7 }, Item.items[952], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040], Item.items[845] }, new int[] { 29, 7 }, Item.items[1052], 1, 1));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[505], Item.items[506] }, new int[] { 1, 2 }, Item.items[511], 2, 2));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[509], Item.items[1138] }, new int[] { 1, 4 }, Item.items[513], 2, 2));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[845], Item.items[1042] }, new int[] { 5, 3 }, Item.items[1301], 3, 0));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[548] }, new int[] { 3 }, Item.items[1391], 5, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 8 }, Item.items[1643], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[548] }, new int[] { 8 }, Item.items[1644], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1392] }, new int[] { 8 }, Item.items[1645], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1394] }, new int[] { 8 }, Item.items[1646], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1395] }, new int[] { 8 }, Item.items[1647], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1396] }, new int[] { 8 }, Item.items[1648], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1397] }, new int[] { 8 }, Item.items[1649], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1398] }, new int[] { 8 }, Item.items[1650], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1399] }, new int[] { 8 }, Item.items[1651], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1400] }, new int[] { 8 }, Item.items[1652], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1401] }, new int[] { 8 }, Item.items[1653], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1404] }, new int[] { 8 }, Item.items[1654], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1405] }, new int[] { 8 }, Item.items[1655], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1406] }, new int[] { 8 }, Item.items[1656], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1407] }, new int[] { 8 }, Item.items[1657], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1408] }, new int[] { 8 }, Item.items[1658], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1441] }, new int[] { 8 }, Item.items[1659], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1602] }, new int[] { 8 }, Item.items[1660], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1425] }, new int[] { 8 }, Item.items[1661], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1426] }, new int[] { 8 }, Item.items[1662], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1427] }, new int[] { 8 }, Item.items[1663], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1428] }, new int[] { 8 }, Item.items[1664], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1429] }, new int[] { 8 }, Item.items[1665], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1430] }, new int[] { 8 }, Item.items[1666], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1431] }, new int[] { 8 }, Item.items[1667], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1432] }, new int[] { 8 }, Item.items[1668], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1433] }, new int[] { 8 }, Item.items[1669], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1434] }, new int[] { 8 }, Item.items[1670], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1435] }, new int[] { 8 }, Item.items[1671], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1436] }, new int[] { 8 }, Item.items[1672], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1437] }, new int[] { 8 }, Item.items[1673], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1438] }, new int[] { 8 }, Item.items[1674], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1439] }, new int[] { 8 }, Item.items[1675], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1440] }, new int[] { 8 }, Item.items[1676], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1698] }, new int[] { 8 }, Item.items[1677], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1604] }, new int[] { 8 }, Item.items[1678], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1409] }, new int[] { 8 }, Item.items[1679], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1410] }, new int[] { 8 }, Item.items[1680], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1411] }, new int[] { 8 }, Item.items[1681], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1412] }, new int[] { 8 }, Item.items[1682], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1413] }, new int[] { 8 }, Item.items[1683], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1414] }, new int[] { 8 }, Item.items[1684], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1415] }, new int[] { 8 }, Item.items[1685], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1416] }, new int[] { 8 }, Item.items[1686], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1417] }, new int[] { 8 }, Item.items[1687], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1418] }, new int[] { 8 }, Item.items[1688], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1419] }, new int[] { 8 }, Item.items[1689], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1420] }, new int[] { 8 }, Item.items[1690], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1421] }, new int[] { 8 }, Item.items[1691], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1422] }, new int[] { 8 }, Item.items[1692], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1423] }, new int[] { 8 }, Item.items[1693], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1424] }, new int[] { 8 }, Item.items[1694], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1697] }, new int[] { 8 }, Item.items[1695], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1603] }, new int[] { 8 }, Item.items[1696], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 14 }, Item.items[1699], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[548] }, new int[] { 14 }, Item.items[1700], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1392] }, new int[] { 14 }, Item.items[1701], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1394] }, new int[] { 14 }, Item.items[1702], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1395] }, new int[] { 14 }, Item.items[1703], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1396] }, new int[] { 14 }, Item.items[1704], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1397] }, new int[] { 14 }, Item.items[1705], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1398] }, new int[] { 14 }, Item.items[1706], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1399] }, new int[] { 14 }, Item.items[1707], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1400] }, new int[] { 14 }, Item.items[1708], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1401] }, new int[] { 14 }, Item.items[1709], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1404] }, new int[] { 14 }, Item.items[1710], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1405] }, new int[] { 14 }, Item.items[1711], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1406] }, new int[] { 14 }, Item.items[1712], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1407] }, new int[] { 14 }, Item.items[1713], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1408] }, new int[] { 14 }, Item.items[1714], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1441] }, new int[] { 14 }, Item.items[1715], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1602] }, new int[] { 14 }, Item.items[1716], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1425] }, new int[] { 14 }, Item.items[1717], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1426] }, new int[] { 14 }, Item.items[1718], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1427] }, new int[] { 14 }, Item.items[1719], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1428] }, new int[] { 14 }, Item.items[1720], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1429] }, new int[] { 14 }, Item.items[1721], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1430] }, new int[] { 14 }, Item.items[1722], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1431] }, new int[] { 14 }, Item.items[1723], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1432] }, new int[] { 14 }, Item.items[1724], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1433] }, new int[] { 14 }, Item.items[1725], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1434] }, new int[] { 14 }, Item.items[1726], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1435] }, new int[] { 14 }, Item.items[1727], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1436] }, new int[] { 14 }, Item.items[1728], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1437] }, new int[] { 14 }, Item.items[1729], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1438] }, new int[] { 14 }, Item.items[1730], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1439] }, new int[] { 14 }, Item.items[1731], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1440] }, new int[] { 14 }, Item.items[1732], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1698] }, new int[] { 14 }, Item.items[1733], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1604] }, new int[] { 14 }, Item.items[1734], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1409] }, new int[] { 14 }, Item.items[1735], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1410] }, new int[] { 14 }, Item.items[1736], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1411] }, new int[] { 14 }, Item.items[1737], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1412] }, new int[] { 14 }, Item.items[1738], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1413] }, new int[] { 14 }, Item.items[1739], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1414] }, new int[] { 14 }, Item.items[1740], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1415] }, new int[] { 14 }, Item.items[1741], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1416] }, new int[] { 14 }, Item.items[1742], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1417] }, new int[] { 14 }, Item.items[1743], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1418] }, new int[] { 14 }, Item.items[1744], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1419] }, new int[] { 14 }, Item.items[1745], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1420] }, new int[] { 14 }, Item.items[1746], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1421] }, new int[] { 14 }, Item.items[1747], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1422] }, new int[] { 14 }, Item.items[1748], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1423] }, new int[] { 14 }, Item.items[1749], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1424] }, new int[] { 14 }, Item.items[1750], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1697] }, new int[] { 14 }, Item.items[1751], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1603] }, new int[] { 14 }, Item.items[1752], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 12 }, Item.items[1753], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[548] }, new int[] { 12 }, Item.items[1754], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1392] }, new int[] { 12 }, Item.items[1755], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1394] }, new int[] { 12 }, Item.items[1756], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1395] }, new int[] { 12 }, Item.items[1757], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1396] }, new int[] { 12 }, Item.items[1758], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1397] }, new int[] { 12 }, Item.items[1759], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1398] }, new int[] { 12 }, Item.items[1760], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1399] }, new int[] { 12 }, Item.items[1761], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1400] }, new int[] { 12 }, Item.items[1762], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1401] }, new int[] { 12 }, Item.items[1763], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1404] }, new int[] { 12 }, Item.items[1764], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1405] }, new int[] { 12 }, Item.items[1765], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1406] }, new int[] { 12 }, Item.items[1766], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1407] }, new int[] { 12 }, Item.items[1767], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1408] }, new int[] { 12 }, Item.items[1768], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1441] }, new int[] { 12 }, Item.items[1769], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1602] }, new int[] { 12 }, Item.items[1770], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1425] }, new int[] { 12 }, Item.items[1771], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1426] }, new int[] { 12 }, Item.items[1772], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1427] }, new int[] { 12 }, Item.items[1773], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1428] }, new int[] { 12 }, Item.items[1774], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1429] }, new int[] { 12 }, Item.items[1775], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1430] }, new int[] { 12 }, Item.items[1776], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1431] }, new int[] { 12 }, Item.items[1777], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1432] }, new int[] { 12 }, Item.items[1778], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1433] }, new int[] { 12 }, Item.items[1779], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1434] }, new int[] { 12 }, Item.items[1780], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1435] }, new int[] { 12 }, Item.items[1781], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1436] }, new int[] { 12 }, Item.items[1782], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1437] }, new int[] { 12 }, Item.items[1783], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1438] }, new int[] { 12 }, Item.items[1784], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1439] }, new int[] { 12 }, Item.items[1785], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1440] }, new int[] { 12 }, Item.items[1786], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1698] }, new int[] { 12 }, Item.items[1787], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1604] }, new int[] { 12 }, Item.items[1788], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1409] }, new int[] { 12 }, Item.items[1789], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1410] }, new int[] { 12 }, Item.items[1790], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1411] }, new int[] { 12 }, Item.items[1791], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1412] }, new int[] { 12 }, Item.items[1792], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1413] }, new int[] { 12 }, Item.items[1793], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1414] }, new int[] { 12 }, Item.items[1794], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1415] }, new int[] { 12 }, Item.items[1795], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1416] }, new int[] { 12 }, Item.items[1796], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1417] }, new int[] { 12 }, Item.items[1797], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1418] }, new int[] { 12 }, Item.items[1798], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1419] }, new int[] { 12 }, Item.items[1799], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1420] }, new int[] { 12 }, Item.items[1800], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1421] }, new int[] { 12 }, Item.items[1801], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1422] }, new int[] { 12 }, Item.items[1802], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1423] }, new int[] { 12 }, Item.items[1803], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1424] }, new int[] { 12 }, Item.items[1804], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1697] }, new int[] { 12 }, Item.items[1805], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1603] }, new int[] { 12 }, Item.items[1806], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1600] }, new int[] { 8 }, Item.items[1807], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1600] }, new int[] { 14 }, Item.items[1808], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1600] }, new int[] { 12 }, Item.items[1809], 1, 1));
	}
	
	public boolean canCraft(EntityPlayer p)
	{
		if (Main.canInfiniCraft)
		{
			return true;
		}
		for (int x = 0; x < ingredients.length; x++)
		{
			if (p.inventory.hasItem(ingredients[x], ingredientAmt[x]))
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return Tile.isNear(p, craftingTable.address);
	}
	
	public void craft(EntityPlayer p)
	{
		if (canCraft(p))
		{
			for (int x = 0; x < ingredients.length; x++)
			{
				if (!Main.canInfiniCraft)
				{
					p.inventory.removeItem(ingredients[x], ingredientAmt[x]);
				}
			}
		}
		Item.newItem(itemProduced, (int)p.pos.X, (int)p.pos.Y, amtProduced);
	}
}
