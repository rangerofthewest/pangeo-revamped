package pangeo.main;

import java.util.ArrayList;
import java.util.List;

public class ProjectileHandler extends Thread
{
	public static boolean update = true;
	
	public static List<EntityProjectile> projectiles = new ArrayList<EntityProjectile>();
	public static int projCounter = 0;
	public static int overwrite = 0;
	
	public void run()
	{
		while (true)
		{
			if (update)
			{
				update();
				update = false;
			}
		}
	}
	
	public static void update()
	{
		for (int a = 0; a < projectiles.toArray().length; a++)
		{
			projectiles.get(a).onUpdate();
			if (projectiles.get(a).pos.X >= Main.world.size_x || projectiles.get(a).pos.X < 0 || projectiles.get(a).pos.Y >= Main.world.size_y || projectiles.get(a).pos.Y < 0)
			{
				projectiles.remove(a);
			}
		}
		projCounter++;
		if (projCounter > 3600)
		{
			Util.print("There are " + projectiles.size() + " projectiles alive at the moment");
			projCounter = 0;
		}
	}
	
	public static void render()
	{
		for (int a = 0; a < projectiles.toArray().length; a++)
		{
			projectiles.get(a).render();
		}
	}
	
	public static void spawn(EntityProjectile proj)
	{
		if (projectiles.size() > Config.entity_maxProjectiles)
		{
			projectiles.set(overwrite, proj);
			overwrite++;
		}
		else
		{
			projectiles.add(proj);
			overwrite = 0;
		}
		if (overwrite > Config.entity_maxProjectiles)
		{
			overwrite = 0;
		}
		Util.print("There are " + projectiles.size() + " projectiles alive at the moment");
	}
}
