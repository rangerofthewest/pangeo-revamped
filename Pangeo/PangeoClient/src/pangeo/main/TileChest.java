package pangeo.main;

public class TileChest extends Tile {

	/**
	 * 
	 */
	private static final long serialVersionUID = -762270119421974217L;

	public TileChest(String s, int i) {
		super(s, i);
		this.chest = true;
		this.lore = "It's a chest. For holding things.";
		// TODO Auto-generated constructor stub
	}

	public void onPlaceTile(int x, int y, World w) {
		super.onPlaceTile(x, y, w);
		new Chest(x, y, "Chest");
	}
}
