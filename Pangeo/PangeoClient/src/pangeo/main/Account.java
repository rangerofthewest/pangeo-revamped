package pangeo.main;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.lwjgl.Sys;

public class Account implements Serializable
{
	public String userName = "";
	
	public AccountLevel accountLevel = AccountLevel.USER;
	public ResultSet dataSet;
	
	public static Account login(String user, String pass) throws InvalidLogin
	{
		Account acc = new Account();
		SQLHandler sql = new SQLHandler("www.creativeplusstudios.com","creativ3_coredata","creativ3_cadmin","pangeo");
		sql.connect();
		try {
			ResultSet s = sql.query("SELECT * FROM creativ3_coredata.users WHERE Username = '" + user + "'");
			if (s.next())
			{
				Util.pn(Util.MD5(pass) + " (testing) against " + s.getString("Password"));
				if (s.getString("Password").contains(Util.MD5(pass)))
				{
					Sys.alert("Login Success!", "You logged in as " + s.getString("Username") + "!");
					acc.userName = s.getString("Username");
					int level = s.getInt("Permission");
					if (level <= 5)
					{
						acc.accountLevel = AccountLevel.USER;
					}
					else if (level == 6)
					{
						acc.accountLevel = AccountLevel.MODERATOR;
					}
					else if (level >= 7)
					{
						acc.accountLevel = AccountLevel.ADMINISTRATOR;
					}
					if (s.getBoolean("PangeoTester"))
					{
						acc.accountLevel = AccountLevel.TESTER;
					}
					if (s.getBoolean("PangeoDeveloper"))
					{
						acc.accountLevel = AccountLevel.DEVELOPER;
					}
					acc.dataSet = s;
					return acc;
				}
				else
				{
					Sys.alert("Login Failure!", "Couldn't login...:(");
					throw new InvalidLogin();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sys.alert("Login Failure!", "Couldn't login because of errors...:(");
		throw new InvalidLogin();
	}
}
