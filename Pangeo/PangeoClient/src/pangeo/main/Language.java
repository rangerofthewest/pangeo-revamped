package pangeo.main;

import org.lwjgl.Sys;

public class Language 
{
	public static String calcPangeonTrans(String a)
	{
		if (a == "a") { return "ahk"; }
		if (a == "b") { return "bi"; }
		if (a == "c") { return "ea"; }
		if (a == "d") { return "dj"; }
		if (a == "e") { return "ru"; }
		if (a == "f") { return "mru"; }
		if (a == "g") { return "tyg"; }
		if (a == "h") { return "i"; }
		if (a == "i") { return "i'"; }
		if (a == "j") { return "yqa-"; }
		if (a == "k") { return "qaf"; }
		if (a == "l") { return "kyg"; }
		if (a == "m") { return "zal"; }
		if (a == "n") { return "inp"; }
		if (a == "o") { return "opi"; }
		if (a == "p") { return "h"; }
		if (a == "q") { return "ne"; }
		if (a == "r") { return "si"; }
		if (a == "s") { return "az"; }
		if (a == "t") { return "afs"; }
		if (a == "u") { return "nu"; }
		if (a == "v") { return "mn"; }
		if (a == "w") { return "wm"; }
		if (a == "x") { return "xaw"; }
		if (a == "y") { return "wia"; }
		if (a == "z") { return "ax"; }
		return a;
	}
	
	public static String translate(String phrase)
	{
		String[] combo = new String[phrase.length()];
		String re = "";
		for (int a = 0; a < phrase.length(); a++)
		{
			combo[a] = calcPangeonTrans(phrase.toCharArray()[a] + "");
		}
		for (int a = 0; a < combo.length; a++)
		{
			re += combo[a];
		}
		Sys.alert(phrase, re);
		return re;
	}
}
