package pangeo.main;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

import org.lwjgl.Sys;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.Color;

public class Item implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2363846246042669643L;
	public static int maxItems = 45000;
	public static Item[] items = new Item[maxItems];
	public static int id_counter = -1;

	public int owner = -1;

	public double maxAngle = 125.0;
	
	public int pickPower = 0;
	public int axePower = 0;
	public int hammerPower = 0;
	public int shiftPower = 0;
	public int range = 0;

	public int maxStack = 999;
	public int id = -1;
	public int useTime = 6;
	public boolean survival = false;
	public boolean isAccessory = false;
	public int manaCost = 0;

	public int value = 0;

	public boolean isMech = false;
	public boolean consumable = false;
	public Texture t;
	public Texture mask;
	public Color maskColor = Color.white;

	public int placeTile = -1;
	public boolean tileSprite = false;

	public String name;
	//public EntityProjectile projectile;

	public boolean food = false;
	public double foodRestore = 0.0;

	public Item clone()
	{
		Item i = new Item(this.name);
		i.axePower = this.axePower;
		i.consumable = this.consumable;
		i.food = this.food;
		i.foodRestore = this.foodRestore;
		i.hammerPower = this.hammerPower;
		i.id = this.id;
		i.isAccessory = this.isAccessory;
		i.isMech = this.isMech;
		i.manaCost = this.manaCost;
		i.mask = this.mask;
		i.maskColor = this.maskColor;
		i.maxAngle = this.maxAngle;
		i.maxStack = this.maxStack;
		i.owner = this.owner;
		i.pickPower = this.pickPower;
		i.placeTile = this.placeTile;
		i.range = this.range;
		i.shiftPower = this.shiftPower;
		i.t = this.t;
		i.tileSprite = this.tileSprite;
		i.useTime = this.useTime;
		i.value = this.value;
		return this;
	}
	
	public Item setMaskColor(Color c)
	{
		this.maskColor = c;
		return this;
	}
	
	public Item(String s) {
		id_counter++;
		id = id_counter;
		name = s;
	}

	public Item setUseTime(int i)
	{
		useTime = i;
		return this;
	}
	
	public Item setProj(EntityProjectile p) {
		//projectile = p;
		return this;
	}

	public Item setConsumable(boolean b) {
		consumable = b;
		return this;
	}

	public Item setSurvival(boolean b) {
		survival = b;
		return this;
	}

	public Item setMech(boolean b) {
		isMech = b;
		return this;
	}

	public Item setAxe(int i) {
		axePower = i;
		return this;
	}

	public Item setHammer(int i) {
		hammerPower = i;
		return this;
	}

	public Item setValue(int i) {
		value = i;
		return this;
	}

	public Item setManaCost(int m) {
		manaCost = m;
		return this;
	}

	public Item setPick(int i) {
		pickPower = i;
		return this;
	}

	public Item setId(int i) {
		id = i;
		return this;
	}

	public Item setAccessory(boolean b) {
		isAccessory = b;
		return this;
	}
		
	public static void importItems(String fileName)
	{
		int amt = 0;
		InputStream file;
		try {
			file = new FileInputStream(fileName + ".itmpkg");
			ObjectInputStream ois = new ObjectInputStream(file);
			try
			{
				for (int x = 0; x < Item.items.length; x++)
				{
					try
					{
						Object object = ois.readObject();
						if (object instanceof ItemAmmo) { items[x] = (ItemAmmo)object; }
						else if (object instanceof ItemArmor) { items[x] = (ItemArmor)object; }
						else if (object instanceof ItemDye) { items[x] = (ItemDye)object; }
						else if (object instanceof ItemFood) { items[x] = (ItemFood)object; }
						else if (object instanceof ItemHealing) { items[x] = (ItemHealing)object; }
						else if (object instanceof ItemPaint) { items[x] = (ItemPaint)object; }
						else if (object instanceof ItemPaintbrush) { items[x] = (ItemPaintbrush)object; }
						else if (object instanceof ItemShield) { items[x] = (ItemShield)object; }
						else if (object instanceof ItemSpell) { items[x] = (ItemSpell)object; }
						else if (object instanceof ItemThrowable) { items[x] = (ItemThrowable)object; }
						else if (object instanceof ItemTool) { items[x] = (ItemTool)object; }
						else if (object instanceof ItemVampireMelee) { items[x] = (ItemVampireMelee)object; }
						else if (object instanceof ItemWire) { items[x] = (ItemWire)object; }
						else if (object instanceof ItemRanged) { items[x] = (ItemRanged)object; }
						else if (object instanceof ItemMelee) { items[x] = (ItemMelee)object; }
						else if (object instanceof ItemWeapon) { items[x] = (ItemWeapon)object; }
						else { items[x] = (Item)object; }
						Util.print("Loaded item " + items[x].name + " id #" + items[x].id);
						amt++;
					}
					catch (Exception e)
					{
						e.printStackTrace();
						break;
					}
				}

				ois.close();
			}
			catch (Exception e){
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sys.alert("Import", amt + " items imported from " + fileName + ".itmpkg");
	}
	
	public static void exportItems()
	{
		OutputStream file;			
		int amt = 0;
		try {
			file = new FileOutputStream("items.itmpkg");
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);
			//output.write(amt); //How many items to load
			for (int x = 0; x < Item.items.length; x++)
			{
				try
				{
					output.writeObject(items[x]);
					Util.print(items[x].name + " exported to item package");
					amt++;
				}
				catch (Exception e)
				{
					Sys.alert("Item Package", x + " items exported to item package");
					break;
				}
			}
			output.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sys.alert("Export", "Export complete! (" + amt + " exported)");
	}

	public static void initItem() {
		// V 1.0
		// Paints
		items[0] = new ItemPaint("Air Force Blue", new Color(93, 138, 168))
				.setValue(1200);
		items[1] = new ItemPaint("Alice Blue", new Color(240, 248, 255))
				.setValue(1200);
		items[2] = new ItemPaint("Alizarin Crimson", new Color(227, 38, 54))
				.setValue(1200);
		items[3] = new ItemPaint("Almond", new Color(239, 222, 205))
				.setValue(1200);
		items[4] = new ItemPaint("Amaranth", new Color(229, 43, 80))
				.setValue(1200);
		items[5] = new ItemPaint("Amber", new Color(255, 191, 0))
				.setValue(1200);
		items[6] = new ItemPaint("American Rose", new Color(255, 3, 62))
				.setValue(1200);
		items[7] = new ItemPaint("Amethyst", new Color(153, 102, 204))
				.setValue(1200);
		items[8] = new ItemPaint("Android Green", new Color(164, 198, 57))
				.setValue(1200);
		items[9] = new ItemPaint("Antique Brass", new Color(205, 149, 117))
				.setValue(1200);
		items[10] = new ItemPaint("Antique Fuchsia", new Color(145, 92, 131))
				.setValue(1200);
		items[11] = new ItemPaint("Antique White", new Color(250, 235, 215))
				.setValue(1200);
		items[12] = new ItemPaint("Ao", new Color(0, 128, 0)).setValue(1200);
		items[13] = new ItemPaint("Apple Green", new Color(141, 182, 0))
				.setValue(1200);
		items[14] = new ItemPaint("Apricot", new Color(251, 206, 177))
				.setValue(1200);
		items[15] = new ItemPaint("Aqua", new Color(0, 255, 255))
				.setValue(1200);
		items[16] = new ItemPaint("Aquamarine", new Color(127, 255, 212))
				.setValue(1200);
		items[17] = new ItemPaint("Army Green", new Color(75, 83, 32))
				.setValue(1200);
		items[18] = new ItemPaint("Arylide Yellow", new Color(233, 214, 107))
				.setValue(1200);
		items[19] = new ItemPaint("Ash Grey", new Color(178, 190, 181))
				.setValue(1200);
		items[20] = new ItemPaint("Asparagus", new Color(135, 169, 107))
				.setValue(1200);
		items[21] = new ItemPaint("Atomic Tangerine", new Color(255, 153, 102))
				.setValue(1200);
		items[22] = new ItemPaint("Auburn", new Color(165, 42, 42))
				.setValue(1200);
		items[23] = new ItemPaint("Aureolin", new Color(253, 238, 0))
				.setValue(1200);
		items[24] = new ItemPaint("Azure", new Color(0, 127, 255))
				.setValue(1200);
		items[25] = new ItemPaint("Banana Mania", new Color(250, 231, 181))
				.setValue(1200);
		items[26] = new ItemPaint("Battleship Grey", new Color(132, 132, 130))
				.setValue(1200);
		items[27] = new ItemPaint("Bazaar", new Color(152, 119, 123))
				.setValue(1200);
		items[28] = new ItemPaint("Beau Blue", new Color(188, 212, 230))
				.setValue(1200);
		items[29] = new ItemPaint("Beaver", new Color(159, 129, 112))
				.setValue(1200);
		items[30] = new ItemPaint("Beige", new Color(245, 245, 220))
				.setValue(1200);
		items[31] = new ItemPaint("Bisque", new Color(255, 228, 196))
				.setValue(1200);
		items[32] = new ItemPaint("Bistre", new Color(61, 43, 31))
				.setValue(1200);
		items[33] = new ItemPaint("Bittersweet", new Color(254, 11, 94))
				.setValue(1200);
		items[34] = new ItemPaint("Black", new Color(0, 0, 0)).setValue(1200);
		items[35] = new ItemPaint("Bleu de France", new Color(49, 140, 231))
				.setValue(1200);
		items[36] = new ItemPaint("Blizzard Blue", new Color(172, 229, 238))
				.setValue(1200);
		items[37] = new ItemPaint("Blond", new Color(250, 240, 190))
				.setValue(1200);
		items[38] = new ItemPaint("Blue", new Color(0, 0, 255)).setValue(1200);
		items[39] = new ItemPaint("Bluebell", new Color(162, 162, 208))
				.setValue(1200);
		items[40] = new ItemPaint("Blue Grey", new Color(102, 153, 204))
				.setValue(1200);
		items[41] = new ItemPaint("Blue Green", new Color(13, 152, 186))
				.setValue(1200);
		items[42] = new ItemPaint("Blue Purple", new Color(138, 43, 226))
				.setValue(1200);
		items[43] = new ItemPaint("Blush", new Color(222, 93, 131))
				.setValue(1200);
		items[44] = new ItemPaint("Bole", new Color(0, 149, 182))
				.setValue(1200);
		items[45] = new ItemPaint("Bone", new Color(227, 218, 201))
				.setValue(1200);
		items[46] = new ItemPaint("Bottle Green", new Color(0, 106, 78))
				.setValue(1200);
		items[47] = new ItemPaint("Boysenberry", new Color(135, 50, 96))
				.setValue(1200);
		items[48] = new ItemPaint("Brass", new Color(181, 166, 66))
				.setValue(1200);
		items[49] = new ItemPaint("Brick Red", new Color(203, 65, 84))
				.setValue(1200);
		items[50] = new ItemPaint("Bright Cerulean", new Color(29, 172, 214))
				.setValue(1200);
		items[51] = new ItemPaint("Bright Green", new Color(102, 255, 0))
				.setValue(1200);
		items[52] = new ItemPaint("Bright Lavender", new Color(191, 148, 228))
				.setValue(1200);
		items[53] = new ItemPaint("Bright Maroon", new Color(195, 33, 72))
				.setValue(1200);
		items[54] = new ItemPaint("Bright Pink", new Color(255, 0, 127))
				.setValue(1200);
		items[55] = new ItemPaint("Bright Turquoise", new Color(8, 232, 222))
				.setValue(1200);
		items[56] = new ItemPaint("Brillant Lavender", new Color(244, 187, 255))
				.setValue(1200);
		items[57] = new ItemPaint("Brilliant Rose", new Color(255, 85, 163))
				.setValue(1200);
		items[58] = new ItemPaint("Bronze", new Color(205, 157, 50))
				.setValue(1200);
		items[59] = new ItemPaint("Brown", new Color(165, 42, 42))
				.setValue(1200);
		items[60] = new ItemPaint("Burgundy", new Color(128, 0, 32))
				.setValue(1200);
		items[61] = new ItemPaint("Burlywood", new Color(222, 184, 135))
				.setValue(1200);
		items[62] = new ItemPaint("Burnt Orange", new Color(204, 85, 0))
				.setValue(1200);
		items[63] = new ItemPaint("Burnt Sienna", new Color(233, 116, 81))
				.setValue(1200);
		items[64] = new ItemPaint("Burnt Umber", new Color(138, 51, 36))
				.setValue(1200);
		items[65] = new ItemPaint("Byzantine", new Color(189, 51, 164))
				.setValue(1200);
		items[66] = new ItemPaint("Byzantium", new Color(112, 41, 99))
				.setValue(1200);
		items[67] = new ItemPaint("Cadet", new Color(83, 104, 114))
				.setValue(1200);
		items[68] = new ItemPaint("Cadet Blue", new Color(95, 158, 160))
				.setValue(1200);
		items[69] = new ItemPaint("Cadet Grey", new Color(145, 163, 176))
				.setValue(1200);
		items[70] = new ItemPaint("Cadmium Green", new Color(0, 107, 60))
				.setValue(1200);
		items[71] = new ItemPaint("Cadmium Orange", new Color(237, 135, 45))
				.setValue(1200);
		items[72] = new ItemPaint("Cadmium Red", new Color(227, 0, 34))
				.setValue(1200);
		items[73] = new ItemPaint("Cadmium Yellow", new Color(255, 246, 0))
				.setValue(1200);
		items[74] = new ItemPaint("Cambridge Blue", new Color(163, 193, 173))
				.setValue(1200);
		items[75] = new ItemPaint("Camel", new Color(193, 154, 107))
				.setValue(1200);
		items[76] = new ItemPaint("Camoflauge Green", new Color(120, 134, 107))
				.setValue(1200);
		items[77] = new ItemPaint("Canary", new Color(255, 255, 153))
				.setValue(1200);
		items[78] = new ItemPaint("Canary Yellow", new Color(255, 239, 0))
				.setValue(1200);
		items[79] = new ItemPaint("Capri", new Color(0, 191, 255))
				.setValue(1200);
		items[80] = new ItemPaint("Cardinal", new Color(196, 30, 58))
				.setValue(1200);
		items[81] = new ItemPaint("Caribbean Green", new Color(0, 204, 153))
				.setValue(1200);
		items[82] = new ItemPaint("Carmine", new Color(255, 0, 64))
				.setValue(1200);
		items[83] = new ItemPaint("Carmine Pink", new Color(235, 76, 66))
				.setValue(1200);
		items[84] = new ItemPaint("Carmine Red", new Color(255, 0, 56))
				.setValue(1200);
		items[85] = new ItemPaint("Carrot", new Color(237, 145, 33))
				.setValue(1200);
		items[86] = new ItemPaint("Celadon", new Color(172, 225, 175))
				.setValue(1200);
		items[87] = new ItemPaint("Celeste", new Color(178, 255, 255))
				.setValue(1200);
		items[88] = new ItemPaint("Celestial Blue", new Color(73, 151, 208))
				.setValue(1200);
		items[89] = new ItemPaint("Cerise", new Color(222, 49, 99))
				.setValue(1200);
		items[90] = new ItemPaint("Cerise Pink", new Color(236, 59, 131))
				.setValue(1200);
		items[91] = new ItemPaint("Cerulean", new Color(0, 123, 167))
				.setValue(1200);
		items[92] = new ItemPaint("Cerulean Blue", new Color(42, 82, 192))
				.setValue(1200);
		items[93] = new ItemPaint("Chamoisee", new Color(160, 120, 90))
				.setValue(1200);
		items[94] = new ItemPaint("Champagne", new Color(250, 214, 165))
				.setValue(1200);
		items[95] = new ItemPaint("Charcoal", new Color(54, 69, 79))
				.setValue(1200);
		items[96] = new ItemPaint("Chartreuse", new Color(127, 255, 0))
				.setValue(1200);
		items[97] = new ItemPaint("Cherry", new Color(222, 49, 99))
				.setValue(1200);
		items[98] = new ItemPaint("Cherryblossom Pink",
				new Color(255, 183, 197)).setValue(1200);
		items[99] = new ItemPaint("Chestnut", new Color(205, 92, 92))
				.setValue(1200);
		items[100] = new ItemPaint("Chocolate", new Color(210, 105, 30))
				.setValue(1200);
		items[101] = new ItemPaint("Chrome Yellow", new Color(255, 167, 0))
				.setValue(1200);
		items[102] = new ItemPaint("Cinereous", new Color(152, 129, 123))
				.setValue(1200);
		items[103] = new ItemPaint("Cinnabar", new Color(227, 66, 52))
				.setValue(1200);
		items[104] = new ItemPaint("Cinnamon", new Color(210, 105, 30))
				.setValue(1200);
		items[105] = new ItemPaint("Citrine", new Color(228, 208, 10))
				.setValue(1200);
		items[106] = new ItemPaint("Cobalt Blue", new Color(0, 71, 171))
				.setValue(1200);
		items[107] = new ItemPaint("Coffee", new Color(111, 78, 55))
				.setValue(1200);
		items[108] = new ItemPaint("Columbia Blue", new Color(155, 221, 255))
				.setValue(1200);
		items[109] = new ItemPaint("Cool Black", new Color(0, 46, 99))
				.setValue(1200);
		items[110] = new ItemPaint("Cool Grey", new Color(140, 146, 172))
				.setValue(1200);
		items[111] = new ItemPaint("Copper", new Color(184, 115, 51))
				.setValue(1200);
		items[112] = new ItemPaint("Copper Rose", new Color(153, 102, 102))
				.setValue(1200);
		items[113] = new ItemPaint("Coquelicot", new Color(255, 56, 0))
				.setValue(1200);
		items[114] = new ItemPaint("Coral", new Color(255, 127, 80))
				.setValue(1200);
		items[115] = new ItemPaint("Coral Pink", new Color(248, 131, 121))
				.setValue(1200);
		items[116] = new ItemPaint("Coral Red", new Color(255, 64, 64))
				.setValue(1200);
		items[117] = new ItemPaint("Cordovan", new Color(137, 63, 63))
				.setValue(1200);
		items[118] = new ItemPaint("Corn", new Color(251, 236, 93))
				.setValue(1200);
		items[119] = new ItemPaint("Cornflower", new Color(154, 206, 235))
				.setValue(1200);
		items[120] = new ItemPaint("Cornflower Blue", new Color(100, 149, 237))
				.setValue(1200);
		items[121] = new ItemPaint("Cornsilk", new Color(255, 248, 220))
				.setValue(1200);
		items[122] = new ItemPaint("Crimson", new Color(220, 20, 60))
				.setValue(1200);
		items[123] = new ItemPaint("Crimson Red", new Color(153, 0, 0))
				.setValue(1200);
		items[124] = new ItemPaint("Crimson Glory", new Color(190, 0, 50))
				.setValue(1200);
		items[125] = new ItemPaint("Cyan", new Color(0, 255, 255))
				.setValue(1200);
		items[126] = new ItemPaint("Daffodil", new Color(255, 255, 49))
				.setValue(1200);
		items[127] = new ItemPaint("Dandelion", new Color(240, 255, 48))
				.setValue(1200);
		items[128] = new ItemPaint("Dark Blue", new Color(0, 0, 139))
				.setValue(1200);
		items[129] = new ItemPaint("Dark Brown", new Color(101, 67, 33))
				.setValue(1200);
		items[130] = new ItemPaint("Dark Byzantium", new Color(93, 57, 64))
				.setValue(1200);
		items[131] = new ItemPaint("Dark Cerulean", new Color(8, 69, 126))
				.setValue(1200);
		items[132] = new ItemPaint("Dark Chestnut", new Color(152, 105, 96))
				.setValue(1200);
		items[133] = new ItemPaint("Dark Coral", new Color(205, 91, 69))
				.setValue(1200);
		items[134] = new ItemPaint("Dark Cyan", new Color(0, 139, 139))
				.setValue(1200);
		items[135] = new ItemPaint("Dark Electric Blue",
				new Color(83, 104, 120)).setValue(1200);
		items[136] = new ItemPaint("Dark Goldenrod", new Color(184, 134, 11))
				.setValue(1200);
		items[137] = new ItemPaint("Dark Grey", new Color(169, 169, 169))
				.setValue(1200);
		items[138] = new ItemPaint("Dark Green", new Color(1, 50, 32))
				.setValue(1200);
		items[139] = new ItemPaint("Dark Jungle Green", new Color(26, 36, 33))
				.setValue(1200);
		items[140] = new ItemPaint("Dark Khaki", new Color(189, 183, 107))
				.setValue(1200);
		items[141] = new ItemPaint("Dark Lava", new Color(72, 60, 50))
				.setValue(1200);
		items[142] = new ItemPaint("Dark Lavender", new Color(115, 79, 150))
				.setValue(1200);
		items[143] = new ItemPaint("Dark Magenta", new Color(139, 0, 139))
				.setValue(1200);
		items[144] = new ItemPaint("Dark Midnight Blue", new Color(0, 51, 102))
				.setValue(1200);
		items[145] = new ItemPaint("Dark Olive Green", new Color(85, 107, 47))
				.setValue(1200);
		items[146] = new ItemPaint("Dark Orange", new Color(255, 140, 0))
				.setValue(1200);
		items[147] = new ItemPaint("Dark Orchid", new Color(153, 50, 204))
				.setValue(1200);
		items[148] = new ItemPaint("Dark Pastel Blue", new Color(119, 158, 203))
				.setValue(1200);
		items[149] = new ItemPaint("Dark Pastel Green", new Color(3, 192, 60))
				.setValue(1200);
		items[150] = new ItemPaint("Dark Pastel Purple",
				new Color(150, 11, 214)).setValue(1200);
		items[151] = new ItemPaint("Dark Pastel Red", new Color(194, 59, 34))
				.setValue(1200);
		items[152] = new ItemPaint("Dark Pink", new Color(231, 84, 128))
				.setValue(1200);
		items[153] = new ItemPaint("Dark Powder Blue", new Color(0, 51, 153))
				.setValue(1200);
		items[154] = new ItemPaint("Dark Raspberry", new Color(135, 38, 87))
				.setValue(1200);
		items[155] = new ItemPaint("Dark Red", new Color(139, 0, 0))
				.setValue(1200);
		items[156] = new ItemPaint("Dark Salmon", new Color(233, 150, 122))
				.setValue(1200);
		items[157] = new ItemPaint("Dark Scarlet", new Color(86, 3, 25))
				.setValue(1200);
		items[158] = new ItemPaint("Dark Sea Green", new Color(143, 188, 143))
				.setValue(1200);
		items[159] = new ItemPaint("Dark Sienna", new Color(60, 20, 20))
				.setValue(1200);
		items[160] = new ItemPaint("Dark Slate Blue", new Color(72, 61, 139))
				.setValue(1200);
		items[161] = new ItemPaint("Dark Slate Grey", new Color(47, 79, 79))
				.setValue(1200);
		items[162] = new ItemPaint("Dark Spring Green", new Color(23, 114, 69))
				.setValue(1200);
		items[163] = new ItemPaint("Dark Tan", new Color(145, 129, 81))
				.setValue(1200);
		items[164] = new ItemPaint("Dark Tangerine", new Color(255, 168, 18))
				.setValue(1200);
		items[165] = new ItemPaint("Dark Taupe", new Color(72, 60, 50))
				.setValue(1200);
		items[166] = new ItemPaint("Dark Terracotta", new Color(204, 78, 92))
				.setValue(1200);
		items[167] = new ItemPaint("Dark Turquoise", new Color(0, 206, 209))
				.setValue(1200);
		items[168] = new ItemPaint("Dark Violet", new Color(148, 0, 211))
				.setValue(1200);
		items[169] = new ItemPaint("Deep Carmine", new Color(169, 32, 62))
				.setValue(1200);
		items[170] = new ItemPaint("Deep Carmine Pink", new Color(239, 48, 56))
				.setValue(1200);
		items[171] = new ItemPaint("Deep Carrot", new Color(233, 105, 44))
				.setValue(1200);
		items[172] = new ItemPaint("Deep Cerise", new Color(218, 50, 235))
				.setValue(1200);
		items[173] = new ItemPaint("Deep Champagne", new Color(250, 214, 165))
				.setValue(1200);
		items[174] = new ItemPaint("Deep Chestnut", new Color(185, 78, 72))
				.setValue(1200);
		items[175] = new ItemPaint("Deep Coffee", new Color(112, 66, 65))
				.setValue(1200);
		items[176] = new ItemPaint("Deep Fuschia", new Color(193, 84, 193))
				.setValue(1200);
		items[177] = new ItemPaint("Deep Jungle Green", new Color(0, 75, 73))
				.setValue(1200);
		items[178] = new ItemPaint("Deep Lilac", new Color(153, 85, 187))
				.setValue(1200);
		items[179] = new ItemPaint("Deep Magenta", new Color(204, 0, 204))
				.setValue(1200);
		items[180] = new ItemPaint("Deep Peach", new Color(255, 203, 164))
				.setValue(1200);
		items[181] = new ItemPaint("Deep Pink", new Color(255, 20, 147))
				.setValue(1200);
		items[182] = new ItemPaint("Deep Saffron", new Color(255, 153, 51))
				.setValue(1200);
		items[183] = new ItemPaint("Deep Sky Blue", new Color(0, 191, 255))
				.setValue(1200);
		items[184] = new ItemPaint("Desert", new Color(193, 154, 107))
				.setValue(1200);
		items[185] = new ItemPaint("Desert Sand", new Color(237, 201, 175))
				.setValue(1200);
		items[186] = new ItemPaint("Dim Grey", new Color(105, 105, 105))
				.setValue(1200);
		items[187] = new ItemPaint("Earth Yellow", new Color(225, 169, 95))
				.setValue(1200);
		items[188] = new ItemPaint("Ecru", new Color(194, 178, 128))
				.setValue(1200);
		items[189] = new ItemPaint("Eggplant", new Color(97, 64, 81))
				.setValue(1200);
		items[190] = new ItemPaint("Eggshell", new Color(240, 234, 214))
				.setValue(1200);
		items[191] = new ItemPaint("Egyptian Blue", new Color(16, 52, 166))
				.setValue(1200);
		items[192] = new ItemPaint("Electric Blue", new Color(125, 249, 255))
				.setValue(1200);
		items[193] = new ItemPaint("Electric Crimson", new Color(255, 0, 63))
				.setValue(1200);
		items[194] = new ItemPaint("Electric Cyan", new Color(0, 255, 255))
				.setValue(1200);
		items[195] = new ItemPaint("Electric Green", new Color(0, 255, 0))
				.setValue(1200);
		items[196] = new ItemPaint("Electric Indigo", new Color(111, 0, 255))
				.setValue(1200);
		items[197] = new ItemPaint("Electric Lavendar",
				new Color(244, 187, 255)).setValue(1200);
		items[198] = new ItemPaint("Electric Lime", new Color(204, 255, 0))
				.setValue(1200);
		items[199] = new ItemPaint("Electric Purple", new Color(191, 0, 255))
				.setValue(1200);
		items[200] = new ItemPaint("Electric Ultramarine",
				new Color(63, 0, 255)).setValue(1200);
		items[201] = new ItemPaint("Electric Violet", new Color(143, 0, 255))
				.setValue(1200);
		items[202] = new ItemPaint("Electric Yellow", new Color(255, 255, 0))
				.setValue(1200);
		items[203] = new ItemPaint("Emerald", new Color(80, 200, 120))
				.setValue(1200);
		items[204] = new ItemPaint("Eton Blue", new Color(150, 200, 162))
				.setValue(1200);
		items[205] = new ItemPaint("Fallow", new Color(193, 154, 107))
				.setValue(1200);
		items[206] = new ItemPaint("Fawn", new Color(229, 170, 112))
				.setValue(1200);
		items[207] = new ItemPaint("Fern", new Color(113, 188, 120))
				.setValue(1200);
		items[208] = new ItemPaint("Fern Green", new Color(79, 121, 66))
				.setValue(1200);
		items[209] = new ItemPaint("Firebrick", new Color(178, 34, 34))
				.setValue(1200);
		items[210] = new ItemPaint("Flame", new Color(226, 88, 34))
				.setValue(1200);
		items[211] = new ItemPaint("Fluorescent Orange", new Color(255, 191, 0))
				.setValue(1200);
		items[212] = new ItemPaint("Fluorescent Pink", new Color(255, 20, 147))
				.setValue(1200);
		items[213] = new ItemPaint("Fluorescent Yellow", new Color(204, 255, 0))
				.setValue(1200);
		items[214] = new ItemPaint("Forest Green", new Color(34, 139, 34))
				.setValue(1200);
		items[215] = new ItemPaint("French Beige", new Color(166, 123, 91))
				.setValue(1200);
		items[216] = new ItemPaint("French Blue", new Color(0, 114, 187))
				.setValue(1200);
		items[217] = new ItemPaint("French Lilac", new Color(134, 96, 142))
				.setValue(1200);
		items[218] = new ItemPaint("French Rose", new Color(246, 74, 138))
				.setValue(1200);
		items[219] = new ItemPaint("Fuchsia", new Color(255, 0, 255))
				.setValue(1200);
		items[220] = new ItemPaint("Fuchsia Pink", new Color(255, 119, 255))
				.setValue(1200);
		items[221] = new ItemPaint("Ginger", new Color(176, 101, 0))
				.setValue(1200);
		items[222] = new ItemPaint("Glaucous", new Color(96, 130, 192))
				.setValue(1200);
		items[223] = new ItemPaint("Gold", new Color(255, 215, 0))
				.setValue(1200);
		items[224] = new ItemPaint("Golden Brown", new Color(153, 101, 21))
				.setValue(1200);
		items[225] = new ItemPaint("Golden Poppy", new Color(252, 194, 0))
				.setValue(1200);
		items[226] = new ItemPaint("Golden Yellow", new Color(255, 223, 0))
				.setValue(1200);
		items[227] = new ItemPaint("Goldenrod", new Color(218, 165, 32))
				.setValue(1200);
		items[228] = new ItemPaint("Gray", new Color(128, 128, 128))
				.setValue(1200);
		items[229] = new ItemPaint("Green", new Color(0, 255, 0))
				.setValue(1200);
		items[230] = new ItemPaint("Green Blue", new Color(17, 100, 180))
				.setValue(1200);
		items[231] = new ItemPaint("Green Yellow", new Color(173, 255, 47))
				.setValue(1200);
		items[232] = new ItemPaint("Han Blue", new Color(68, 108, 207))
				.setValue(1200);
		items[233] = new ItemPaint("Han Purple", new Color(82, 24, 250))
				.setValue(1200);
		items[234] = new ItemPaint("Harvest Gold", new Color(218, 145, 0))
				.setValue(1200);
		items[235] = new ItemPaint("Heliotrope", new Color(223, 115, 255))
				.setValue(1200);
		items[236] = new ItemPaint("Hot Magenta", new Color(255, 29, 206))
				.setValue(1200);
		items[237] = new ItemPaint("Hot Pink", new Color(255, 105, 180))
				.setValue(1200);
		items[238] = new ItemPaint("Hunter Green", new Color(53, 94, 59))
				.setValue(1200);
		items[239] = new ItemPaint("Indigo", new Color(75, 0, 130))
				.setValue(1200);
		items[240] = new ItemPaint("Iris", new Color(90, 79, 207))
				.setValue(1200);
		items[241] = new ItemPaint("Jade", new Color(0, 168, 107))
				.setValue(1200);
		items[242] = new ItemPaint("Jasmine", new Color(248, 222, 126))
				.setValue(1200);
		items[243] = new ItemPaint("Jasper", new Color(215, 59, 62))
				.setValue(1200);
		items[244] = new ItemPaint("Jonquil", new Color(250, 218, 94))
				.setValue(1200);
		items[245] = new ItemPaint("Jungle Green", new Color(41, 171, 135))
				.setValue(1200);
		items[246] = new ItemPaint("Lapis Lazuli", new Color(38, 97, 156))
				.setValue(1200);
		items[247] = new ItemPaint("Lava", new Color(207, 16, 32))
				.setValue(1200);
		items[248] = new ItemPaint("Lavender", new Color(230, 230, 250))
				.setValue(1200);
		items[249] = new ItemPaint("Lavender Blue", new Color(204, 204, 255))
				.setValue(1200);

		// Dyes
		items[250] = new ItemDye("Air Force Blue", new Color(93, 138, 168))
				.setValue(1200);
		items[251] = new ItemDye("Alice Blue", new Color(240, 248, 255))
				.setValue(1200);
		items[252] = new ItemDye("Alizarin Crimson", new Color(227, 38, 54))
				.setValue(1200);
		items[253] = new ItemDye("Almond", new Color(239, 222, 205))
				.setValue(1200);
		items[254] = new ItemDye("Amaranth", new Color(229, 43, 80))
				.setValue(1200);
		items[255] = new ItemDye("Amber", new Color(255, 191, 0))
				.setValue(1200);
		items[256] = new ItemDye("American Rose", new Color(255, 3, 62))
				.setValue(1200);
		items[257] = new ItemDye("Amethyst", new Color(153, 102, 204))
				.setValue(1200);
		items[258] = new ItemDye("Android Green", new Color(164, 198, 57))
				.setValue(1200);
		items[259] = new ItemDye("Antique Brass", new Color(205, 149, 117))
				.setValue(1200);
		items[260] = new ItemDye("Antique Fuchsia", new Color(145, 92, 131))
				.setValue(1200);
		items[261] = new ItemDye("Antique White", new Color(250, 235, 215))
				.setValue(1200);
		items[262] = new ItemDye("Ao", new Color(0, 128, 0)).setValue(1200);
		items[263] = new ItemDye("Apple Green", new Color(141, 182, 0))
				.setValue(1200);
		items[264] = new ItemDye("Apricot", new Color(251, 206, 177))
				.setValue(1200);
		items[265] = new ItemDye("Aqua", new Color(0, 255, 255)).setValue(1200);
		items[266] = new ItemDye("Aquamarine", new Color(127, 255, 212))
				.setValue(1200);
		items[267] = new ItemDye("Army Green", new Color(75, 83, 32))
				.setValue(1200);
		items[268] = new ItemDye("Arylide Yellow", new Color(233, 214, 107))
				.setValue(1200);
		items[269] = new ItemDye("Ash Grey", new Color(178, 190, 181))
				.setValue(1200);
		items[270] = new ItemDye("Asparagus", new Color(135, 169, 107))
				.setValue(1200);
		items[271] = new ItemDye("Atomic Tangerine", new Color(255, 153, 102))
				.setValue(1200);
		items[272] = new ItemDye("Auburn", new Color(165, 42, 42))
				.setValue(1200);
		items[273] = new ItemDye("Aureolin", new Color(253, 238, 0))
				.setValue(1200);
		items[274] = new ItemDye("Azure", new Color(0, 127, 255))
				.setValue(1200);
		items[275] = new ItemDye("Banana Mania", new Color(250, 231, 181))
				.setValue(1200);
		items[276] = new ItemDye("Battleship Grey", new Color(132, 132, 130))
				.setValue(1200);
		items[277] = new ItemDye("Bazaar", new Color(152, 119, 123))
				.setValue(1200);
		items[278] = new ItemDye("Beau Blue", new Color(188, 212, 230))
				.setValue(1200);
		items[279] = new ItemDye("Beaver", new Color(159, 129, 112))
				.setValue(1200);
		items[280] = new ItemDye("Beige", new Color(245, 245, 220))
				.setValue(1200);
		items[281] = new ItemDye("Bisque", new Color(255, 228, 196))
				.setValue(1200);
		items[282] = new ItemDye("Bistre", new Color(61, 43, 31))
				.setValue(1200);
		items[283] = new ItemDye("Bittersweet", new Color(254, 11, 94))
				.setValue(1200);
		items[284] = new ItemDye("Black", new Color(0, 0, 0)).setValue(1200);
		items[285] = new ItemDye("Bleu de France", new Color(49, 140, 231))
				.setValue(1200);
		items[286] = new ItemDye("Blizzard Blue", new Color(172, 229, 238))
				.setValue(1200);
		items[287] = new ItemDye("Blond", new Color(250, 240, 190))
				.setValue(1200);
		items[288] = new ItemDye("Blue", new Color(0, 0, 255)).setValue(1200);
		items[289] = new ItemDye("Bluebell", new Color(162, 162, 208))
				.setValue(1200);
		items[290] = new ItemDye("Blue Grey", new Color(102, 153, 204))
				.setValue(1200);
		items[291] = new ItemDye("Blue Green", new Color(13, 152, 186))
				.setValue(1200);
		items[292] = new ItemDye("Blue Purple", new Color(138, 43, 226))
				.setValue(1200);
		items[293] = new ItemDye("Blush", new Color(222, 93, 131))
				.setValue(1200);
		items[294] = new ItemDye("Bole", new Color(0, 149, 182)).setValue(1200);
		items[295] = new ItemDye("Bone", new Color(227, 218, 201))
				.setValue(1200);
		items[296] = new ItemDye("Bottle Green", new Color(0, 106, 78))
				.setValue(1200);
		items[297] = new ItemDye("Boysenberry", new Color(135, 50, 96))
				.setValue(1200);
		items[298] = new ItemDye("Brass", new Color(181, 166, 66))
				.setValue(1200);
		items[299] = new ItemDye("Brick Red", new Color(203, 65, 84))
				.setValue(1200);
		items[300] = new ItemDye("Bright Cerulean", new Color(29, 172, 214))
				.setValue(1200);
		items[301] = new ItemDye("Bright Green", new Color(102, 255, 0))
				.setValue(1200);
		items[302] = new ItemDye("Bright Lavender", new Color(191, 148, 228))
				.setValue(1200);
		items[303] = new ItemDye("Bright Maroon", new Color(195, 33, 72))
				.setValue(1200);
		items[304] = new ItemDye("Bright Pink", new Color(255, 0, 127))
				.setValue(1200);
		items[305] = new ItemDye("Bright Turquoise", new Color(8, 232, 222))
				.setValue(1200);
		items[306] = new ItemDye("Brillant Lavender", new Color(244, 187, 255))
				.setValue(1200);
		items[307] = new ItemDye("Brilliant Rose", new Color(255, 85, 163))
				.setValue(1200);
		items[308] = new ItemDye("Bronze", new Color(205, 157, 50))
				.setValue(1200);
		items[309] = new ItemDye("Brown", new Color(165, 42, 42))
				.setValue(1200);
		items[310] = new ItemDye("Burgundy", new Color(128, 0, 32))
				.setValue(1200);
		items[311] = new ItemDye("Burlywood", new Color(222, 184, 135))
				.setValue(1200);
		items[312] = new ItemDye("Burnt Orange", new Color(204, 85, 0))
				.setValue(1200);
		items[313] = new ItemDye("Burnt Sienna", new Color(233, 116, 81))
				.setValue(1200);
		items[314] = new ItemDye("Burnt Umber", new Color(138, 51, 36))
				.setValue(1200);
		items[315] = new ItemDye("Byzantine", new Color(189, 51, 164))
				.setValue(1200);
		items[316] = new ItemDye("Byzantium", new Color(112, 41, 99))
				.setValue(1200);
		items[317] = new ItemDye("Cadet", new Color(83, 104, 114))
				.setValue(1200);
		items[318] = new ItemDye("Cadet Blue", new Color(95, 158, 160))
				.setValue(1200);
		items[319] = new ItemDye("Cadet Grey", new Color(145, 163, 176))
				.setValue(1200);
		items[320] = new ItemDye("Cadmium Green", new Color(0, 107, 60))
				.setValue(1200);
		items[321] = new ItemDye("Cadmium Orange", new Color(237, 135, 45))
				.setValue(1200);
		items[322] = new ItemDye("Cadmium Red", new Color(227, 0, 34))
				.setValue(1200);
		items[323] = new ItemDye("Cadmium Yellow", new Color(255, 246, 0))
				.setValue(1200);
		items[324] = new ItemDye("Cambridge Blue", new Color(163, 193, 173))
				.setValue(1200);
		items[325] = new ItemDye("Camel", new Color(193, 154, 107))
				.setValue(1200);
		items[326] = new ItemDye("Camoflauge Green", new Color(120, 134, 107))
				.setValue(1200);
		items[327] = new ItemDye("Canary", new Color(255, 255, 153))
				.setValue(1200);
		items[328] = new ItemDye("Canary Yellow", new Color(255, 239, 0))
				.setValue(1200);
		items[329] = new ItemDye("Capri", new Color(0, 191, 255))
				.setValue(1200);
		items[330] = new ItemDye("Cardinal", new Color(196, 30, 58))
				.setValue(1200);
		items[331] = new ItemDye("Caribbean Green", new Color(0, 204, 153))
				.setValue(1200);
		items[332] = new ItemDye("Carmine", new Color(255, 0, 64))
				.setValue(1200);
		items[333] = new ItemDye("Carmine Pink", new Color(235, 76, 66))
				.setValue(1200);
		items[334] = new ItemDye("Carmine Red", new Color(255, 0, 56))
				.setValue(1200);
		items[335] = new ItemDye("Carrot", new Color(237, 145, 33))
				.setValue(1200);
		items[336] = new ItemDye("Celadon", new Color(172, 225, 175))
				.setValue(1200);
		items[337] = new ItemDye("Celeste", new Color(178, 255, 255))
				.setValue(1200);
		items[338] = new ItemDye("Celestial Blue", new Color(73, 151, 208))
				.setValue(1200);
		items[339] = new ItemDye("Cerise", new Color(222, 49, 99))
				.setValue(1200);
		items[340] = new ItemDye("Cerise Pink", new Color(236, 59, 131))
				.setValue(1200);
		items[341] = new ItemDye("Cerulean", new Color(0, 123, 167))
				.setValue(1200);
		items[342] = new ItemDye("Cerulean Blue", new Color(42, 82, 192))
				.setValue(1200);
		items[343] = new ItemDye("Chamoisee", new Color(160, 120, 90))
				.setValue(1200);
		items[344] = new ItemDye("Champagne", new Color(250, 214, 165))
				.setValue(1200);
		items[345] = new ItemDye("Charcoal", new Color(54, 69, 79))
				.setValue(1200);
		items[346] = new ItemDye("Chartreuse", new Color(127, 255, 0))
				.setValue(1200);
		items[347] = new ItemDye("Cherry", new Color(222, 49, 99))
				.setValue(1200);
		items[348] = new ItemDye("Cherryblossom Pink", new Color(255, 183, 197))
				.setValue(1200);
		items[349] = new ItemDye("Chestnut", new Color(205, 92, 92))
				.setValue(1200);
		items[350] = new ItemDye("Chocolate", new Color(210, 105, 30))
				.setValue(1200);
		items[351] = new ItemDye("Chrome Yellow", new Color(255, 167, 0))
				.setValue(1200);
		items[352] = new ItemDye("Cinereous", new Color(152, 129, 123))
				.setValue(1200);
		items[353] = new ItemDye("Cinnabar", new Color(227, 66, 52))
				.setValue(1200);
		items[354] = new ItemDye("Cinnamon", new Color(210, 105, 30))
				.setValue(1200);
		items[355] = new ItemDye("Citrine", new Color(228, 208, 10))
				.setValue(1200);
		items[356] = new ItemDye("Cobalt Blue", new Color(0, 71, 171))
				.setValue(1200);
		items[357] = new ItemDye("Coffee", new Color(111, 78, 55))
				.setValue(1200);
		items[358] = new ItemDye("Columbia Blue", new Color(155, 221, 255))
				.setValue(1200);
		items[359] = new ItemDye("Cool Black", new Color(0, 46, 99))
				.setValue(1200);
		items[360] = new ItemDye("Cool Grey", new Color(140, 146, 172))
				.setValue(1200);
		items[361] = new ItemDye("Copper", new Color(184, 115, 51))
				.setValue(1200);
		items[362] = new ItemDye("Copper Rose", new Color(153, 102, 102))
				.setValue(1200);
		items[363] = new ItemDye("Coquelicot", new Color(255, 56, 0))
				.setValue(1200);
		items[364] = new ItemDye("Coral", new Color(255, 127, 80))
				.setValue(1200);
		items[365] = new ItemDye("Coral Pink", new Color(248, 131, 121))
				.setValue(1200);
		items[366] = new ItemDye("Coral Red", new Color(255, 64, 64))
				.setValue(1200);
		items[367] = new ItemDye("Cordovan", new Color(137, 63, 63))
				.setValue(1200);
		items[368] = new ItemDye("Corn", new Color(251, 236, 93))
				.setValue(1200);
		items[369] = new ItemDye("Cornflower", new Color(154, 206, 235))
				.setValue(1200);
		items[370] = new ItemDye("Cornflower Blue", new Color(100, 149, 237))
				.setValue(1200);
		items[371] = new ItemDye("Cornsilk", new Color(255, 248, 220))
				.setValue(1200);
		items[372] = new ItemDye("Crimson", new Color(220, 20, 60))
				.setValue(1200);
		items[373] = new ItemDye("Crimson Red", new Color(153, 0, 0))
				.setValue(1200);
		items[374] = new ItemDye("Crimson Glory", new Color(190, 0, 50))
				.setValue(1200);
		items[375] = new ItemDye("Cyan", new Color(0, 255, 255)).setValue(1200);
		items[376] = new ItemDye("Daffodil", new Color(255, 255, 49))
				.setValue(1200);
		items[377] = new ItemDye("Dandelion", new Color(240, 255, 48))
				.setValue(1200);
		items[378] = new ItemDye("Dark Blue", new Color(0, 0, 139))
				.setValue(1200);
		items[379] = new ItemDye("Dark Brown", new Color(101, 67, 33))
				.setValue(1200);
		items[380] = new ItemDye("Dark Byzantium", new Color(93, 57, 64))
				.setValue(1200);
		items[381] = new ItemDye("Dark Cerulean", new Color(8, 69, 126))
				.setValue(1200);
		items[382] = new ItemDye("Dark Chestnut", new Color(152, 105, 96))
				.setValue(1200);
		items[383] = new ItemDye("Dark Coral", new Color(205, 91, 69))
				.setValue(1200);
		items[384] = new ItemDye("Dark Cyan", new Color(0, 139, 139))
				.setValue(1200);
		items[385] = new ItemDye("Dark Electric Blue", new Color(83, 104, 120))
				.setValue(1200);
		items[386] = new ItemDye("Dark Goldenrod", new Color(184, 134, 11))
				.setValue(1200);
		items[387] = new ItemDye("Dark Grey", new Color(169, 169, 169))
				.setValue(1200);
		items[388] = new ItemDye("Dark Green", new Color(1, 50, 32))
				.setValue(1200);
		items[389] = new ItemDye("Dark Jungle Green", new Color(26, 36, 33))
				.setValue(1200);
		items[390] = new ItemDye("Dark Khaki", new Color(189, 183, 107))
				.setValue(1200);
		items[391] = new ItemDye("Dark Lava", new Color(72, 60, 50))
				.setValue(1200);
		items[392] = new ItemDye("Dark Lavender", new Color(115, 79, 150))
				.setValue(1200);
		items[393] = new ItemDye("Dark Magenta", new Color(139, 0, 139))
				.setValue(1200);
		items[394] = new ItemDye("Dark Midnight Blue", new Color(0, 51, 102))
				.setValue(1200);
		items[395] = new ItemDye("Dark Olive Green", new Color(85, 107, 47))
				.setValue(1200);
		items[396] = new ItemDye("Dark Orange", new Color(255, 140, 0))
				.setValue(1200);
		items[397] = new ItemDye("Dark Orchid", new Color(153, 50, 204))
				.setValue(1200);
		items[398] = new ItemDye("Dark Pastel Blue", new Color(119, 158, 203))
				.setValue(1200);
		items[399] = new ItemDye("Dark Pastel Green", new Color(3, 192, 60))
				.setValue(1200);
		items[400] = new ItemDye("Dark Pastel Purple", new Color(150, 11, 214))
				.setValue(1200);
		items[401] = new ItemDye("Dark Pastel Red", new Color(194, 59, 34))
				.setValue(1200);
		items[402] = new ItemDye("Dark Pink", new Color(231, 84, 128))
				.setValue(1200);
		items[403] = new ItemDye("Dark Powder Blue", new Color(0, 51, 153))
				.setValue(1200);
		items[404] = new ItemDye("Dark Raspberry", new Color(135, 38, 87))
				.setValue(1200);
		items[405] = new ItemDye("Dark Red", new Color(139, 0, 0))
				.setValue(1200);
		items[406] = new ItemDye("Dark Salmon", new Color(233, 150, 122))
				.setValue(1200);
		items[407] = new ItemDye("Dark Scarlet", new Color(86, 3, 25))
				.setValue(1200);
		items[408] = new ItemDye("Dark Sea Green", new Color(143, 188, 143))
				.setValue(1200);
		items[409] = new ItemDye("Dark Sienna", new Color(60, 20, 20))
				.setValue(1200);
		items[410] = new ItemDye("Dark Slate Blue", new Color(72, 61, 139))
				.setValue(1200);
		items[411] = new ItemDye("Dark Slate Grey", new Color(47, 79, 79))
				.setValue(1200);
		items[412] = new ItemDye("Dark Spring Green", new Color(23, 114, 69))
				.setValue(1200);
		items[413] = new ItemDye("Dark Tan", new Color(145, 129, 81))
				.setValue(1200);
		items[414] = new ItemDye("Dark Tangerine", new Color(255, 168, 18))
				.setValue(1200);
		items[415] = new ItemDye("Dark Taupe", new Color(72, 60, 50))
				.setValue(1200);
		items[416] = new ItemDye("Dark Terracotta", new Color(204, 78, 92))
				.setValue(1200);
		items[417] = new ItemDye("Dark Turquoise", new Color(0, 206, 209))
				.setValue(1200);
		items[418] = new ItemDye("Dark Violet", new Color(148, 0, 211))
				.setValue(1200);
		items[419] = new ItemDye("Deep Carmine", new Color(169, 32, 62))
				.setValue(1200);
		items[420] = new ItemDye("Deep Carmine Pink", new Color(239, 48, 56))
				.setValue(1200);
		items[421] = new ItemDye("Deep Carrot", new Color(233, 105, 44))
				.setValue(1200);
		items[422] = new ItemDye("Deep Cerise", new Color(218, 50, 235))
				.setValue(1200);
		items[423] = new ItemDye("Deep Champagne", new Color(250, 214, 165))
				.setValue(1200);
		items[424] = new ItemDye("Deep Chestnut", new Color(185, 78, 72))
				.setValue(1200);
		items[425] = new ItemDye("Deep Coffee", new Color(112, 66, 65))
				.setValue(1200);
		items[426] = new ItemDye("Deep Fuschia", new Color(193, 84, 193))
				.setValue(1200);
		items[427] = new ItemDye("Deep Jungle Green", new Color(0, 75, 73))
				.setValue(1200);
		items[428] = new ItemDye("Deep Lilac", new Color(153, 85, 187))
				.setValue(1200);
		items[429] = new ItemDye("Deep Magenta", new Color(204, 0, 204))
				.setValue(1200);
		items[430] = new ItemDye("Deep Peach", new Color(255, 203, 164))
				.setValue(1200);
		items[431] = new ItemDye("Deep Pink", new Color(255, 20, 147))
				.setValue(1200);
		items[432] = new ItemDye("Deep Saffron", new Color(255, 153, 51))
				.setValue(1200);
		items[433] = new ItemDye("Deep Sky Blue", new Color(0, 191, 255))
				.setValue(1200);
		items[434] = new ItemDye("Desert", new Color(193, 154, 107))
				.setValue(1200);
		items[435] = new ItemDye("Desert Sand", new Color(237, 201, 175))
				.setValue(1200);
		items[436] = new ItemDye("Dim Grey", new Color(105, 105, 105))
				.setValue(1200);
		items[437] = new ItemDye("Earth Yellow", new Color(225, 169, 95))
				.setValue(1200);
		items[438] = new ItemDye("Ecru", new Color(194, 178, 128))
				.setValue(1200);
		items[439] = new ItemDye("Eggplant", new Color(97, 64, 81))
				.setValue(1200);
		items[440] = new ItemDye("Eggshell", new Color(240, 234, 214))
				.setValue(1200);
		items[441] = new ItemDye("Egyptian Blue", new Color(16, 52, 166))
				.setValue(1200);
		items[442] = new ItemDye("Electric Blue", new Color(125, 249, 255))
				.setValue(1200);
		items[443] = new ItemDye("Electric Crimson", new Color(255, 0, 63))
				.setValue(1200);
		items[444] = new ItemDye("Electric Cyan", new Color(0, 255, 255))
				.setValue(1200);
		items[445] = new ItemDye("Electric Green", new Color(0, 255, 0))
				.setValue(1200);
		items[446] = new ItemDye("Electric Indigo", new Color(111, 0, 255))
				.setValue(1200);
		items[447] = new ItemDye("Electric Lavendar", new Color(244, 187, 255))
				.setValue(1200);
		items[448] = new ItemDye("Electric Lime", new Color(204, 255, 0))
				.setValue(1200);
		items[449] = new ItemDye("Electric Purple", new Color(191, 0, 255))
				.setValue(1200);
		items[450] = new ItemDye("Electric Ultramarine", new Color(63, 0, 255))
				.setValue(1200);
		items[451] = new ItemDye("Electric Violet", new Color(143, 0, 255))
				.setValue(1200);
		items[452] = new ItemDye("Electric Yellow", new Color(255, 255, 0))
				.setValue(1200);
		items[453] = new ItemDye("Emerald", new Color(80, 200, 120))
				.setValue(1200);
		items[454] = new ItemDye("Eton Blue", new Color(150, 200, 162))
				.setValue(1200);
		items[455] = new ItemDye("Fallow", new Color(193, 154, 107))
				.setValue(1200);
		items[456] = new ItemDye("Fawn", new Color(229, 170, 112))
				.setValue(1200);
		items[457] = new ItemDye("Fern", new Color(113, 188, 120))
				.setValue(1200);
		items[458] = new ItemDye("Fern Green", new Color(79, 121, 66))
				.setValue(1200);
		items[459] = new ItemDye("Firebrick", new Color(178, 34, 34))
				.setValue(1200);
		items[460] = new ItemDye("Flame", new Color(226, 88, 34))
				.setValue(1200);
		items[461] = new ItemDye("Fluorescent Orange", new Color(255, 191, 0))
				.setValue(1200);
		items[462] = new ItemDye("Fluorescent Pink", new Color(255, 20, 147))
				.setValue(1200);
		items[463] = new ItemDye("Fluorescent Yellow", new Color(204, 255, 0))
				.setValue(1200);
		items[464] = new ItemDye("Forest Green", new Color(34, 139, 34))
				.setValue(1200);
		items[465] = new ItemDye("French Beige", new Color(166, 123, 91))
				.setValue(1200);
		items[466] = new ItemDye("French Blue", new Color(0, 114, 187))
				.setValue(1200);
		items[467] = new ItemDye("French Lilac", new Color(134, 96, 142))
				.setValue(1200);
		items[468] = new ItemDye("French Rose", new Color(246, 74, 138))
				.setValue(1200);
		items[469] = new ItemDye("Fuchsia", new Color(255, 0, 255))
				.setValue(1200);
		items[470] = new ItemDye("Fuchsia Pink", new Color(255, 119, 255))
				.setValue(1200);
		items[471] = new ItemDye("Ginger", new Color(176, 101, 0))
				.setValue(1200);
		items[472] = new ItemDye("Glaucous", new Color(96, 130, 192))
				.setValue(1200);
		items[473] = new ItemDye("Gold", new Color(255, 215, 0)).setValue(1200);
		items[474] = new ItemDye("Golden Brown", new Color(153, 101, 21))
				.setValue(1200);
		items[475] = new ItemDye("Golden Poppy", new Color(252, 194, 0))
				.setValue(1200);
		items[476] = new ItemDye("Golden Yellow", new Color(255, 223, 0))
				.setValue(1200);
		items[477] = new ItemDye("Goldenrod", new Color(218, 165, 32))
				.setValue(1200);
		items[478] = new ItemDye("Gray", new Color(128, 128, 128))
				.setValue(1200);
		items[479] = new ItemDye("Green", new Color(0, 255, 0)).setValue(1200);
		items[480] = new ItemDye("Green Blue", new Color(17, 100, 180))
				.setValue(1200);
		items[481] = new ItemDye("Green Yellow", new Color(173, 255, 47))
				.setValue(1200);
		items[482] = new ItemDye("Han Blue", new Color(68, 108, 207))
				.setValue(1200);
		items[483] = new ItemDye("Han Purple", new Color(82, 24, 250))
				.setValue(1200);
		items[484] = new ItemDye("Harvest Gold", new Color(218, 145, 0))
				.setValue(1200);
		items[485] = new ItemDye("Heliotrope", new Color(223, 115, 255))
				.setValue(1200);
		items[486] = new ItemDye("Hot Magenta", new Color(255, 29, 206))
				.setValue(1200);
		items[487] = new ItemDye("Hot Pink", new Color(255, 105, 180))
				.setValue(1200);
		items[488] = new ItemDye("Hunter Green", new Color(53, 94, 59))
				.setValue(1200);
		items[489] = new ItemDye("Indigo", new Color(75, 0, 130))
				.setValue(1200);
		items[490] = new ItemDye("Iris", new Color(90, 79, 207)).setValue(1200);
		items[491] = new ItemDye("Jade", new Color(0, 168, 107)).setValue(1200);
		items[492] = new ItemDye("Jasmine", new Color(248, 222, 126))
				.setValue(1200);
		items[493] = new ItemDye("Jasper", new Color(215, 59, 62))
				.setValue(1200);
		items[494] = new ItemDye("Jonquil", new Color(250, 218, 94))
				.setValue(1200);
		items[495] = new ItemDye("Jungle Green", new Color(41, 171, 135))
				.setValue(1200);
		items[496] = new ItemDye("Lapis Lazuli", new Color(38, 97, 156))
				.setValue(1200);
		items[497] = new ItemDye("Lava", new Color(207, 16, 32)).setValue(1200);
		items[498] = new ItemDye("Lavender", new Color(230, 230, 250))
				.setValue(1200);
		items[499] = new ItemDye("Lavender Blue", new Color(204, 204, 255))
				.setValue(1200);

		// Base tier materials that aren't bars
		items[500] = new Item("Mud");
		items[501] = new Item("Wood");
		items[502] = new Item("Cactus");
		items[503] = new Item("Stone");
		items[504] = new Item("Limestone");

		// Refined base tier materials
		items[505] = new Item("Tin Bar");
		items[506] = new Item("Copper Bar");
		items[507] = new Item("Rusty Iron Bar");
		items[508] = new Item("Refined Copper Bar");
		items[509] = new Item("Iron Bar");
		items[510] = new Item("Lead Bar");
		items[511] = new Item("Bronze Bar");
		items[512] = new Item("Refined Iron Bar");
		items[513] = new Item("Steel Bar");
		items[514] = new Item("Frostshards");
		items[515] = new Item("Refined Steel Bar");
		items[516] = new Item("Silver Bar");
		items[517] = new Item("Refined Silver Bar");
		items[518] = new Item("Gold Bar");
		items[519] = new Item("Refined Gold Bar");
		items[520] = new Item("Ivory Tusk");
		items[521] = new Item("Ebony Bar");
		items[522] = new Item("Refined Ebony Bar");
		items[523] = new Item("Stellatite Dust");
		items[524] = new Item("Starcopper Dust");
		items[525] = new Item("Stariron Dust");
		items[526] = new Item("Starsteel Dust");
		items[527] = new Item("Starsilver Dust");
		items[528] = new Item("Stargold Dust");
		items[529] = new Item("Honeycomb");
		items[530] = new Item("Amethyst");
		items[531] = new Item("Topaz");
		items[532] = new Item("Sapphire");
		items[533] = new Item("Emerald");
		items[534] = new Item("Ruby");
		items[535] = new Item("Diamond");
		items[536] = new Item("Onyx");
		items[537] = new Item("Opal");
		items[538] = new Item("Coffeeplate");
		items[539] = new Item("Hardened Candy");
		items[540] = new Item("Meteorite Bar");
		items[541] = new Item("Comet Bar");
		items[542] = new Item("Lokiite Bar");
		items[543] = new Item("Galvanized Steel Bar");
		items[544] = new Item("Zincite Bar");
		items[545] = new Item("Barite Bar");
		items[546] = new Item("Hellfire Soul");
		items[547] = new Item("Hive Essence");
		items[548] = new Item("Mystic Wood").setOwner(1042);
		items[549] = new Item("Bloody Bar");
		items[550] = new Item("Tombstone Dust");
		items[551] = new Item("Solidified Vaisinnen");
		items[552] = new Item("Chromite Bar");
		items[553] = new Item("Mandibilium Bar");
		items[554] = new Item("Cerussite Bar");
		items[555] = new Item("Galena Bar");
		items[556] = new Item("Cinnabar Bar");
		items[557] = new Item("Sphalerite Bar");
		items[558] = new Item("Cassiterite Bar");
		items[559] = new Item("Kunzite Bar");
		items[560] = new Item("Celestine Bar");
		items[561] = new Item("Andarrian Bar");
		items[562] = new Item("Columbite Bar");
		items[563] = new Item("Rhodonite Bar");
		items[564] = new Item("Rhodochrosite Bar");
		items[565] = new Item("Orpiment Bar");
		items[566] = new Item("Heartstone");
		items[567] = new Item("Cyclostone");
		items[568] = new Item("Luminescite Crystal");
		items[569] = new Item("Realgar Bar");
		items[570] = new Item("Stibnite Bar");
		items[571] = new Item("Autunite Bar");
		items[572] = new Item("Sphene Bar");
		items[573] = new Item("Vanadinite Bar");
		items[574] = new Item("Molybdenite Bar");
		items[575] = new Item("Powellite Bar");
		items[576] = new Item("Manganite Bar");
		items[577] = new Item("Malachite Bar");
		items[578] = new Item("Chalcopyrite Bar");
		items[579] = new Item("Turquoise Stone");
		items[580] = new Item("Peacock Bar");
		items[581] = new Item("Azurite Bar");
		items[582] = new Item("Dioptase Bar");
		items[583] = new Item("Marcasite Bar");
		items[584] = new Item("Enchanted Pyrite Bar");
		items[585] = new Item("Hematite Bar");
		items[586] = new Item("Goethite Bar");
		items[587] = new Item("Magnetite Bar");
		items[588] = new Item("Obsidian Bar");
		items[589] = new Item("Orichalcum Bar");
		items[590] = new Item("Cobalt Bar");
		items[591] = new Item("Blanite Fragment");
		items[592] = new Item("Pyrozium Shard");
		items[593] = new Item("Erythium Charge");
		items[594] = new Item("Charged Erythium Particle");
		items[595] = new Item("Cryptoril Fleck");
		items[596] = new Item("Sclerite Bar");
		items[597] = new Item("Solidified Aurasite");
		items[598] = new Item("Irium Bar");
		items[599] = new Item("Cryptogen Shard");
		items[600] = new Item("Axonite Axion");
		items[601] = new Item("Dragonbone Marrow");
		items[602] = new Item("Iridescent Bar");
		items[603] = new Item("Bloodsteel Rust");
		items[604] = new Item("Sunset Bar");
		items[605] = new Item("Sunrise Bar");
		items[606] = new Item("Ichor Drop");
		items[607] = new Item("Ambrosia Drop");
		items[608] = new Item("Golden Ichor Drop");
		items[609] = new Item("Void-forge Warpstar");
		items[610] = new Item("Tortured Soul");
		items[611] = new Item("Tormented Soul");
		items[612] = new Item("Soulium Essence");
		items[613] = new Item("Glorium Essence");
		items[614] = new Item("Reaper Soul");
		items[615] = new Item("Ectoplasma");
		items[616] = new Item("Cataclysmia");
		items[617] = new Item("Matrium Bar");
		items[618] = new Item("Quag");
		items[619] = new Item("Aeoch Essence");

		// ROGUE ITEMS 1.0
		items[620] = new ItemThrowable("Throwing Knife", 4).setWeight(1)
				.setWeight(1);
		items[621] = new ItemMelee("Iron Dagger", 2).setWeight(1).setWeight(1);
		items[622] = new ItemMelee("Lead Dagger", 3).setWeight(1);
		items[623] = new ItemMelee("Bronze Dagger", 5).setWeight(1);
		items[624] = new ItemMelee("Refined Iron Dagger", 6).setWeight(1);
		items[625] = new ItemMelee("Steel Dagger", 7).setWeight(1);
		items[626] = new ItemMelee("Frost Dagger", 6)
				.setHitEffect(EnumHitEffect.CHILLED).setHitEffectChance(0.5f)
				.setWeight(1);
		items[627] = new ItemMelee("Refined Steel Dagger", 11).setWeight(1);
		items[628] = new ItemMelee("Gold Dagger", 13).setWeight(1);
		items[629] = new ItemMelee("Refined Gold Dagger", 14).setWeight(1);
		items[630] = new ItemMelee("Ivory Dagger", 15).setWeight(1);
		items[631] = new ItemMelee("Ebony Dagger", 18).setWeight(1);
		items[632] = new ItemMelee("Poisoned Iron Dagger", 3)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[633] = new ItemMelee("Poisoned Lead Dagger", 4)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[634] = new ItemMelee("Poisoned Bronze Dagger", 6)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[635] = new ItemMelee("Poisoned Refined Iron Dagger", 7)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[636] = new ItemMelee("Poisoned Steel Dagger", 8)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[637] = new ItemMelee("Poisoned Refined Steel Dagger", 12)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[638] = new ItemMelee("Poisoned Gold Dagger", 14)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[639] = new ItemMelee("Poisoned Refined Gold Dagger", 15)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[640] = new ItemMelee("Poisoned Ivory Dagger", 16)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[641] = new ItemMelee("Poisoned Ebony Dagger", 19)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.25f)
				.setWeight(1);
		items[642] = new ItemMelee("Fiery Iron Dagger", 3)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[643] = new ItemMelee("Fiery Lead Dagger", 4)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[644] = new ItemMelee("Fiery Bronze Dagger", 6)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[645] = new ItemMelee("Fiery Refined Iron Dagger", 7)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[646] = new ItemMelee("Fiery Steel Dagger", 8)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[647] = new ItemMelee("Fiery Refined Steel Dagger", 12)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[648] = new ItemMelee("Fiery Gold Dagger", 14)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[649] = new ItemMelee("Fiery Refined Gold Dagger", 15)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[650] = new ItemMelee("Fiery Ivory Dagger", 16)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		items[651] = new ItemMelee("Fiery Ebony Dagger", 19)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.285f)
				.setWeight(1);
		// Darts
		items[652] = new ItemThrowable("Throwing Dart", 2).setWeight(1);
		items[653] = new ItemThrowable("Piercing Dart", 4)
				.setHitEffect(EnumHitEffect.PIERCING).setHitEffectChance(0.95f)
				.setWeight(1);
		items[654] = new ItemThrowable("Fire Dart", 5)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.95f)
				.setWeight(1);
		items[655] = new ItemThrowable("Poisoned Dart", 5)
				.setHitEffect(EnumHitEffect.POISON).setHitEffectChance(0.95f)
				.setWeight(1);
		items[656] = new ItemThrowable("Warp Dart", 2)
				.setHitEffect(EnumHitEffect.WARP).setHitEffectChance(1.0f)
				.setWeight(1);
		// Swords
		items[657] = new ItemMelee("Iron Shortblade", 5).setWeight(1);
		items[658] = new ItemMelee("Lead Shortblade", 6).setWeight(1);
		items[659] = new ItemMelee("Bronze Shortblade", 7).setWeight(1);
		items[660] = new ItemMelee("Steel Shortblade", 8).setWeight(1);
		items[661] = new ItemMelee("Frost Shortblade", 9)
				.setHitEffect(EnumHitEffect.CHILLED).setHitEffectChance(0.35f)
				.setWeight(1);
		items[662] = new ItemMelee("Silver Shortblade", 10).setWeight(1);
		items[663] = new ItemMelee("Gold Shortblade", 11).setWeight(1);
		items[664] = new ItemMelee("Ivory Shortblade", 12).setWeight(1);
		items[665] = new ItemMelee("Ebony Shortblade", 13).setWeight(1);
		items[666] = new ItemMelee("Ancient Shortblade", 14).setWeight(1);
		items[667] = new ItemMelee("Stellatite Shortblade", 16).setWeight(1);
		items[668] = new ItemMelee("Starcopper Shortblade", 17).setWeight(1);
		items[669] = new ItemMelee("Stariron Shortblade", 18).setWeight(1);
		items[670] = new ItemMelee("Starsteel Shortblade", 19).setWeight(1);
		items[671] = new ItemMelee("Starsilver Shortblade", 20).setWeight(1);
		items[672] = new ItemMelee("Stargold Shortblade", 21).setWeight(1);
		items[673] = new ItemMelee("Honeythrum", 23).setWeight(1);
		items[674] = new ItemMelee("Birthday Slicer", 23).setWeight(1);
		items[675] = new ItemMelee("Java Beaner", 24).setWeight(1);
		items[676] = new ItemMelee("Sugarwhopper", 25).setWeight(1);
		items[677] = new ItemMelee("Elf-forge Shortblade", 26).setWeight(1);
		items[678] = new ItemMelee("Dwarf-forge Shortblade", 27).setWeight(1);
		items[679] = new ItemMelee("Star-forge Shortblade", 29).setWeight(1);
		items[680] = new ItemMelee("Meteorite Shortblade", 30).setWeight(1);
		items[681] = new ItemMelee("Comet Shortblade", 30)
				.setHitEffect(EnumHitEffect.CHILLED).setHitEffectChance(0.35f)
				.setWeight(1);
		items[682] = new ItemMelee("Lokiite Vibroblade", 35).setWeight(1);
		items[683] = new ItemMelee("Galvanized Steel Vibroblade", 38)
				.setWeight(1);
		items[684] = new ItemMelee("Zincite Vibroblade", 40).setWeight(1);
		items[685] = new ItemMelee("Barite Vibroblade", 42).setWeight(1);
		items[686] = new ItemMelee("Hellfire Vibroblade", 44)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.55f)
				.setWeight(1);
		items[687] = new ItemMelee("Hive Vibroblade", 45).setWeight(1);
		items[688] = new ItemMelee("Sylvian Vibroblade", 47).setWeight(1);
		items[689] = new ItemMelee("Sanguine Vibroblade", 50)
				.setHitEffect(EnumHitEffect.BLEEDING).setHitEffectChance(0.50f)
				.setWeight(1);
		items[690] = new ItemMelee("Graveyard Vibroblade", 52)
				.setHitEffect(EnumHitEffect.NECROSIS).setHitEffectChance(0.25f)
				.setWeight(1);
		items[691] = new ItemMelee("Forsaken Vibroblade", 53).setWeight(1);
		items[692] = new ItemMelee("Vaisinnen Vibroblade", 55).setWeight(1);
		items[693] = new ItemMelee("Chromite Vibroblade", 58).setWeight(1);
		items[694] = new ItemMelee("Mandibilium Vibroblade", 60)
				.setHitEffect(EnumHitEffect.BLEEDING).setHitEffectChance(0.60f)
				.setWeight(1);
		items[695] = new ItemMelee("Cerussite Vibroblade", 64).setWeight(1);
		items[696] = new ItemMelee("Galena Vibroblade", 65).setWeight(1);
		items[697] = new ItemMelee("Cinnabar Vibroblade", 66).setWeight(1);
		items[698] = new ItemMelee("Sphalerite Vibroblade", 67).setWeight(1);
		items[699] = new ItemMelee("Cassiterite Vibroblade", 69).setWeight(1);
		items[700] = new ItemMelee("Kunzite Vibroblade", 70).setWeight(1);
		items[701] = new ItemMelee("Celestine Vibroblade", 73).setWeight(1);
		items[702] = new ItemMelee("Andarrian Vibroblade", 74).setWeight(1);
		items[703] = new ItemMelee("Columbite Vibroblade", 77).setWeight(1);
		items[704] = new ItemMelee("Rhodonite Vibroblade", 78).setWeight(1);
		items[705] = new ItemMelee("Rhodochrosite Vibroblade", 80).setWeight(1);
		items[706] = new ItemMelee("Orpiment Hyperblade", 86).setWeight(1);
		items[707] = new ItemMelee("Heartstone Hyperblade", 88).setWeight(1);
		items[708] = new ItemMelee("Cyclostone Hyperblade", 90).setWeight(1);
		items[709] = new ItemMelee("Luminescite Hyperblade", 93).setWeight(1);
		items[710] = new ItemMelee("Realgar Hyperblade", 94).setWeight(1);
		items[711] = new ItemMelee("Stibnite Hyperblade", 99).setWeight(1);
		items[712] = new ItemMelee("Autunite Hyperblade", 102).setWeight(1);
		items[713] = new ItemMelee("Sphene Hyperblade", 104).setWeight(1);
		items[714] = new ItemMelee("Vanadinite Hyperblade", 107).setWeight(1);
		items[715] = new ItemMelee("Molybdenite Hyperblade", 110).setWeight(1);
		items[716] = new ItemMelee("Powellite Hyperblade", 112).setWeight(1);
		items[717] = new ItemMelee("Manganite Hyperblade", 113).setWeight(1);
		items[718] = new ItemMelee("Malachite Hyperblade", 116).setWeight(1);
		items[719] = new ItemMelee("Chalcopyrite Hyperblade", 121).setWeight(1);
		items[720] = new ItemMelee("Turquoise Hyperblade", 124).setWeight(1);
		items[721] = new ItemMelee("Peacock Hyperblade", 126).setWeight(1);
		items[722] = new ItemMelee("Azurite Hyperblade", 133).setWeight(1);
		items[723] = new ItemMelee("Dioptase Hyperblade", 134).setWeight(1);
		items[724] = new ItemMelee("Marcasite Hyperblade", 138).setWeight(1);
		items[725] = new ItemMelee("Pyrite Hyperblade", 144).setWeight(1);
		items[726] = new ItemMelee("Hematite Hyperblade", 147).setWeight(1);
		items[727] = new ItemMelee("Goethite Hyperblade", 151).setWeight(1);
		items[728] = new ItemMelee("Mangenite Hyperblade", 155).setWeight(1);
		items[729] = new ItemMelee("Obsidian Hyperblade", 163).setHitEffect(
				EnumHitEffect.BLEEDING).setWeight(1);
		items[730] = new ItemMelee("Lichling Hyperblade", 167).setHitEffect(
				EnumHitEffect.NECROSIS).setWeight(1);
		items[731] = new ItemMelee("Pumpkin Lichling Hyperblade", 169)
				.setHitEffect(EnumHitEffect.NECROSIS).setWeight(1);
		items[732] = new ItemMelee("Lich King's Deathdagger", 180)
				.setHitEffect(EnumHitEffect.FIRE).setWeight(1);
		items[733] = new ItemMelee("Pumpkin King's Deathdagger", 186)
				.setHitEffect(EnumHitEffect.FIRE).setWeight(1);
		items[734] = new ItemMelee("Temporal Shifter", 192)
				.setHitEffect(EnumHitEffect.CONFUSION).setHitEffectChance(0.5f)
				.setWeight(1);
		items[735] = new ItemMelee("Orichalcum Shatterer", 196)
				.setHitEffect(EnumHitEffect.CONFUSION)
				.setHitEffectChance(0.75f).setWeight(1);
		items[736] = new ItemMelee("Cobalt Demise", 201).setWeight(1);
		items[737] = new ItemMelee("Blanite Destructor", 204)
				.setHitEffect(EnumHitEffect.BLINDNESS)
				.setHitEffectChance(0.33f).setWeight(1);
		items[738] = new ItemMelee("Lunar Scalder", 208).setWeight(1);
		items[739] = new ItemMelee("Solar Melter", 209).setWeight(1);
		items[740] = new ItemMelee("Eclipsing Shadow", 216).setWeight(1);
		items[741] = new ItemMelee("Ethereal Spirit", 219).setWeight(1);
		items[742] = new ItemMelee("Pyrozium Blaze", 221)
				.setHitEffect(EnumHitEffect.FIRE).setHitEffectChance(0.8f)
				.setWeight(1);
		items[743] = new ItemMelee("Erythium Shock", 226)
				.setHitEffect(EnumHitEffect.ELECTROCUTION)
				.setHitEffectChance(0.35f).setWeight(1);
		items[744] = new ItemMelee("Charged Erythium Shock", 234)
				.setHitEffect(EnumHitEffect.ELECTROCUTION)
				.setHitEffectChance(0.725f).setWeight(1);
		items[745] = new ItemMelee("Cryptoril Frostbite", 249).setHitEffect(
				EnumHitEffect.CHILLED).setWeight(1);
		items[746] = new ItemMelee("Sclerite Slicer", 258).setWeight(1);
		items[747] = new ItemMelee("Aurasite Laser", 263).setWeight(1);
		items[748] = new ItemMelee("Irium Condensor", 266).setWeight(1);
		items[749] = new ItemMelee("Cryptogen Fyre", 274).setWeight(1);
		items[750] = new ItemMelee("Axonite Core", 276).setWeight(1);
		items[751] = new ItemMelee("Dragonbone Razor", 283).setWeight(1);
		items[752] = new ItemMelee("Bloodsteel Drench", 289).setWeight(1);
		items[753] = new ItemMelee("Sunset Shine", 296).setWeight(1);
		items[754] = new ItemMelee("Sunrise Blaze", 297).setWeight(1);
		items[755] = new ItemMelee("Ichorium", 303)
				.setHitEffect(EnumHitEffect.ICHOR).setHitEffectChance(0.4f)
				.setWeight(1);
		items[756] = new ItemMelee("Ambrosium", 309)
				.setHitEffect(EnumHitEffect.ICHOR).setHitEffectChance(0.6f)
				.setWeight(1);
		items[757] = new ItemMelee("Golden Ichorium", 315)
				.setHitEffect(EnumHitEffect.ICHOR).setHitEffectChance(0.8f)
				.setWeight(1);
		items[758] = new ItemMelee("Void-forge Vinvox", 322).setWeight(1);
		items[759] = new ItemMelee("The Torturer", 328)
				.setHitEffect(EnumHitEffect.TORTURE).setHitEffectChance(0.5f)
				.setWeight(1);
		items[760] = new ItemMelee("The Tormenter", 335)
				.setHitEffect(EnumHitEffect.TORMENT).setHitEffectChance(0.4f)
				.setWeight(1);
		items[761] = new ItemMelee("Soulite Hyperion", 351).setWeight(1);
		items[762] = new ItemMelee("Gloria Excelsior", 365).setWeight(1);
		items[763] = new ItemMelee("Reaper's Delight", 378)
				.setHitEffect(EnumHitEffect.TORMENT).setHitEffectChance(0.75f)
				.setWeight(1);
		items[764] = new ItemMelee("Cataclysmia", 400)
				.setHitEffect(EnumHitEffect.CATACLYSM).setHitEffectChance(0.5f)
				.setWeight(2);
		items[765] = new ItemMelee("Aeochryn Shortblade", 456).setWeight(2);
		items[766] = new ItemMelee("Pangeon Shortblade", 513).setHitEffect(
				EnumHitEffect.ARMORNEGATE).setWeight(2);
		// Rogue Cloth Armor
		items[767] = new ItemArmor("Cloth Bandage", "RogueCloth", 1,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.STEALTH);
		items[768] = new ItemArmor("Cloth Robe", "RogueCloth", 1,
				EnumArmorSlot.BODY, EnumArmorSetEffect.STEALTH);
		items[769] = new ItemArmor("Cloth Pants", "RogueCloth", 0,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.STEALTH);
		items[770] = new ItemArmor("Cloth Slippers", "RogueCloth", 0,
				EnumArmorSlot.FEET, EnumArmorSetEffect.STEALTH);
		// Rogue Silk Armor
		items[771] = new ItemArmor("Silk Facemask", "RogueSilk", 1,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.STEALTH);
		items[772] = new ItemArmor("Silk Robe", "RogueSilk", 1,
				EnumArmorSlot.BODY, EnumArmorSetEffect.STEALTH);
		items[773] = new ItemArmor("Silk Pants", "RogueSilk", 1,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.STEALTH);
		items[774] = new ItemArmor("Silk Slippers", "RogueSilk", 1,
				EnumArmorSlot.FEET, EnumArmorSetEffect.STEALTH);
		// Rogue Vine Armor
		items[775] = new ItemArmor("Vine Mask", "RogueVine", 1,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.FASTSTEALTH_FOREST);
		items[776] = new ItemArmor("Vine Weave", "RogueVine", 2,
				EnumArmorSlot.BODY, EnumArmorSetEffect.FASTSTEALTH_FOREST);
		items[777] = new ItemArmor("Vine Pants", "RogueVine", 1,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.FASTSTEALTH_FOREST);
		items[778] = new ItemArmor("Vine Boots", "RogueVine", 1,
				EnumArmorSlot.FEET, EnumArmorSetEffect.FASTSTEALTH_FOREST);
		// Rogue Ice Armor
		items[775] = new ItemArmor("Frostbite Helm", "RogueIce", 2,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.FASTSTEALTH_ICE);
		items[776] = new ItemArmor("Frostbite Plate", "RogueIce", 2,
				EnumArmorSlot.BODY, EnumArmorSetEffect.FASTSTEALTH_ICE);
		items[777] = new ItemArmor("Frostbite Greaves", "RogueIce", 2,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.FASTSTEALTH_ICE);
		items[778] = new ItemArmor("Frostbite Boots", "RogueIce", 1,
				EnumArmorSlot.FEET, EnumArmorSetEffect.FASTSTEALTH_ICE);
		// Rogue Assassin Armor
		items[779] = new ItemArmor("Assassin's Facemask", "RogueAssassin", 2,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.IMMUNE_POISON);
		items[780] = new ItemArmor("Assassin's Robe", "RogueAssassin", 3,
				EnumArmorSlot.BODY, EnumArmorSetEffect.IMMUNE_POISON);
		items[781] = new ItemArmor("Assassin's Pants", "RogueAssassin", 3,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.IMMUNE_POISON);
		items[782] = new ItemArmor("Assassin's Boots", "RogueAssassin", 2,
				EnumArmorSlot.FEET, EnumArmorSetEffect.IMMUNE_POISON);
		// Rogue Ghost Armor
		items[783] = new ItemArmor("Ghostmask", "RogueGhost", 4,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.SHORT_PHASE);
		items[784] = new ItemArmor("Ghostrobe", "RogueGhost", 4,
				EnumArmorSlot.BODY, EnumArmorSetEffect.SHORT_PHASE);
		items[785] = new ItemArmor("Ghostliners", "RogueGhost", 4,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.SHORT_PHASE);
		items[786] = new ItemArmor("Ghoststriders", "RogueGhost", 3,
				EnumArmorSlot.FEET, EnumArmorSetEffect.SHORT_PHASE);
		// Rogue Phantom Armor
		items[787] = new ItemArmor("Phantom Mask", "RoguePhantom", 4,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.LONG_PHASE);
		items[788] = new ItemArmor("Phantom Cloak", "RoguePhantom", 6,
				EnumArmorSlot.BODY, EnumArmorSetEffect.LONG_PHASE);
		items[789] = new ItemArmor("Phantom Pants", "RoguePhantom", 5,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.LONG_PHASE);
		items[790] = new ItemArmor("Phantom Shoes", "RoguePhantom", 4,
				EnumArmorSlot.FEET, EnumArmorSetEffect.LONG_PHASE);

		items[791] = new ItemMelee("St. Valentine's Cross", 19).setHitEffect(
				EnumHitEffect.CONFUSION).setHitEffectChance(0.33f);
		items[792] = new ItemVampireMelee("Heartshatterer", 24, 0.04f);
		items[793] = new ItemHealing("Sweetheart Candy", 25);
		items[794] = new ItemHealing("Box o' Chocolate", 80);
		items[795] = new ItemHealing("Cherub's Kiss", 140);
		items[796] = new Item("Door Key");
		items[797] = new ItemRanged("Cupid's Bow", 12).setHitEffect(
				EnumHitEffect.CONFUSION).setHitEffectChance(0.33f);
		items[798] = new ItemHealing("Valentine's Card", 10);

		// Rogue Shadow Armor
		items[799] = new ItemArmor("Shadowguard", "RogueShadow", 6,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.SHADEWALK);
		items[800] = new ItemArmor("Shadowcloak", "RogueShadow", 6,
				EnumArmorSlot.BODY, EnumArmorSetEffect.SHADEWALK);
		items[801] = new ItemArmor("Shadowgarb", "RogueShadow", 6,
				EnumArmorSlot.LEGS, EnumArmorSetEffect.SHADEWALK);
		items[802] = new ItemArmor("Shadowboots", "RogueShadow", 5,
				EnumArmorSlot.FEET, EnumArmorSetEffect.SHADEWALK);

		items[803] = new Item("Skis");
		items[804] = new ItemArmor("Abe's Top Hat", "PresidentAbe", 0,
				EnumArmorSlot.HEAD, EnumArmorSetEffect.NONE);
		items[805] = new ItemRanged("Presidential Liberator", 26);
		items[806] = new Item("Inaugural Speech"); // used in crafting
													// president's day items
		items[807] = new ItemMelee("Freedom Day", 28);

		items[808] = new ItemMelee("Tin Sword", 0);
		items[809] = new ItemMelee("Tin Longsword", 0);
		items[810] = new ItemMelee("Tin Broadsword", 0);
		items[811] = new ItemThrowable("Tin Flechette", 0);
		items[812] = new ItemMelee("Tin Rapier", 0);
		items[813] = new ItemMelee("Tin Flamberge", 0);
		items[814] = new ItemMelee("Tin Battleaxe", 0);
		items[815] = new ItemMelee("Tin Warhammer", 0);
		items[816] = new ItemMelee("Tin Partisan", 0);
		items[817] = new ItemMelee("Tin Spear", 0);
		items[818] = new ItemMelee("Tin Halberd", 0);
		items[819] = new ItemMelee("Tin Kukri", 0);
		items[820] = new ItemMelee("Tin Flail", 0);
		items[821] = new ItemMelee("Tin Mace", 0);
		items[822] = new ItemMelee("Tin Whip", 0);
		items[823] = new ItemThrowable("Tin Boomerang", 0);
		items[824] = new ItemThrowable("Tin Chakaram", 0);
		items[825] = new ItemMelee("Tin Lance", 0);
		items[826] = new ItemMelee("Tin Spatha", 0);
		items[827] = new ItemMelee("Tin Trident", 0);
		items[828] = new ItemRanged("Tin Bow", 0);
		items[829] = new ItemRanged("Tin Crossbow", 0);
		items[830] = new ItemRanged("Tin Longbow", 0);
		items[831] = new ItemRanged("Tin Recurve Bow", 0);
		items[832] = new ItemRanged("Tin Compound Bow", 0);
		items[833] = new ItemRanged("Tin Sniper Bow", 0);
		items[834] = new ItemAmmo("Tin Arrow", 0, EnumWeapon.BOW);
		items[835] = new ItemAmmo("Tin Razor Arrow", 0, EnumWeapon.BOW);
		items[836] = new ItemAmmo("Tin Broadarrow", 0, EnumWeapon.BOW);
		items[837] = new ItemAmmo("Tin Penetrator", 0, EnumWeapon.BOW);
		items[838] = new ItemVampireMelee("Tin Drainer", 0, 0.01f);
		items[839] = new ItemAmmo("Wooden Arrow", 0, EnumWeapon.BOW);
		items[840] = new ItemAmmo("Wooden Broadtip Arrow", 0, EnumWeapon.BOW);
		items[841] = new ItemAmmo("Wooden Razor Arrow", 0, EnumWeapon.BOW);
		items[842] = new Item("Copper Goblet").setTile(118, false)
				.setConsumable(true);
		items[843] = new Item("Cloth");
		items[844] = new Item("Wheat Seed");
		items[845] = new Item("Stick");
		items[846] = new Item("String");
		items[847] = new Item("Paper");
		items[848] = new Item("Gold Goblet").setTile(121, false).setConsumable(
				true);
		items[849] = new Item("Iron Chain");
		items[850] = new Item("Silver Goblet").setTile(120, false)
				.setConsumable(true);
		items[851] = new Item("Grain");
		items[852] = new Item("Platinum Goblet").setTile(122, false)
				.setConsumable(true);
		items[853] = new Item("Iron Goblet").setTile(119, false).setConsumable(
				true);
		items[854] = new ItemMelee("Brimstone Fyre", 16);
		items[855] = new Item("Wand of Explosions");
		items[856] = new Item("Amalganator").setTile(123, true);
		items[857] = new Item("Metalforge").setTile(124, true);

		// Pickaxes, good sir, are butts
		items[857] = new Item("Mud Pickaxe").setPick(2);
		items[858] = new Item("Wood Pickaxe").setPick(4);
		items[859] = new Item("Cactus Pickaxe").setPick(6);
		items[860] = new Item("Stone Pickaxe").setPick(8);
		items[861] = new Item("Limestone Pickaxe").setPick(10);
		items[862] = new Item("Tin Pickaxe").setPick(12);
		items[863] = new Item("Copper Pickaxe").setPick(14);
		items[864] = new Item("Iron Pickaxe").setPick(16);
		items[865] = new Item("Lead Pickaxe").setPick(18);
		items[866] = new Item("Bronze Pickaxe").setPick(20);
		items[867] = new Item("Steel Pickaxe").setPick(25);
		items[868] = new Item("Frost Pickaxe").setPick(30);
		items[869] = new Item("Silver Pickaxe").setPick(35);
		items[870] = new Item("Gold Pickaxe").setPick(40);
		items[871] = new Item("Ivory Pickaxe").setPick(45);
		items[872] = new Item("Ancient Pickaxe").setPick(50);
		items[873] = new Item("Stellatite Pickaxe").setPick(55);
		items[874] = new Item("Starcopper Pickaxe").setPick(58);
		items[875] = new Item("Stariron Pickaxe").setPick(61);
		items[876] = new Item("Starsteel Pickaxe").setPick(64);
		items[877] = new Item("Starsilver Pickaxe").setPick(67);
		items[878] = new Item("Stargold Pickaxe").setPick(70);
		items[879] = new Item("Honey Pickaxe").setPick(70);
		items[880] = new Item("Gemstone Pickaxe").setPick(72);
		items[881] = new Item("Birthday Pickaxe").setPick(73);
		items[882] = new Item("Coffee Pickaxe").setPick(80);
		items[883] = new Item("Candy Pickaxe").setPick(85);
		items[884] = new Item("Dwarf-forge Pickaxe").setPick(90);
		items[885] = new Item("Star-forge Pickaxe").setPick(95);
		items[886] = new Item("Meteorite Pickaxe").setPick(97);
		items[887] = new Item("Comet Pickaxe").setPick(100);
		items[888] = new Item("Lokiite Pickaxe").setPick(120);
		items[889] = new Item("Zincite Pickaxe").setPick(130);
		items[890] = new Item("Barite Pickaxe").setPick(140);
		items[891] = new Item("Hellfire Pickaxe").setPick(150);
		items[892] = new Item("Hive Pickaxe").setPick(175);
		items[893] = new Item("Sanguine Pickaxe").setPick(200);
		items[894] = new Item("Graveyard Pickaxe").setPick(210);
		items[895] = new Item("Forsaken Pickaxe").setPick(220);
		items[896] = new Item("Chromite Pickaxe").setPick(230);
		items[897] = new Item("Mandibilium Pickaxe").setPick(240);
		items[898] = new Item("Cerussite Pickaxe").setPick(250);
		items[899] = new Item("Galena Pickaxe").setPick(260);
		items[900] = new Item("Cinnabar Pickaxe").setPick(270);
		items[901] = new Item("Kunzite Pickaxe").setPick(280);
		items[902] = new Item("Celestine Pickaxe").setPick(295);
		items[903] = new Item("Andarrian Pickaxe").setPick(310);
		items[904] = new Item("Columbite Pickaxe").setPick(325);
		items[905] = new Item("Rhodonite Pickaxe").setPick(340);
		items[906] = new Item("Heartstone Pickaxe").setPick(355);
		items[907] = new Item("Cyclostone Pickaxe").setPick(370);
		items[908] = new Item("Stibnite Pickaxe").setPick(385);
		items[909] = new Item("Autunite Pickaxe").setPick(400);
		items[910] = new Item("Sphene Pickaxe").setPick(415);
		items[911] = new Item("Vanadinite Pickaxe").setPick(430);
		items[912] = new Item("Molybdenite Pickaxe").setPick(445);
		items[913] = new Item("Powellite Pickaxe").setPick(460);
		items[914] = new Item("Manganite Pickaxe").setPick(480);
		items[915] = new Item("Malachite Pickaxe").setPick(500);
		items[916] = new Item("Hematite Pickaxe").setPick(520);
		items[917] = new Item("Goethite Pickaxe").setPick(540);
		items[918] = new Item("Obsidian Pickaxe").setPick(560);
		items[919] = new Item("Temporal Pickaxe").setPick(600);
		items[920] = new Item("Cobalt Pickaxe").setPick(620);
		items[921] = new Item("Blanite Pickaxe").setPick(640);
		items[922] = new Item("Lunar Pickaxe").setPick(659);
		items[923] = new Item("Solar Pickaxe").setPick(660);
		items[924] = new Item("Eclipse Pickaxe").setPick(661);
		items[925] = new Item("Ethereal Pickaxe").setPick(666);
		items[926] = new Item("Pyrozium Pickaxe").setPick(670);
		items[927] = new Item("Erythium Pickaxe").setPick(700);
		items[928] = new Item("Charged Erythium Pickaxe").setPick(730);
		items[929] = new Item("Dragonbone Pickaxe").setPick(760);
		items[930] = new Item("Ichor Pickaxe").setPick(780);
		items[931] = new Item("Ambrosia Pickaxe").setPick(800);
		items[932] = new Item("Nectar Pickaxe").setPick(810);
		items[933] = new Item("Golden Ichor Pickaxe").setPick(820);
		items[934] = new Item("Void-forge Pickaxe").setPick(900);
		items[935] = new Item("Tortured Pickaxe").setPick(920);
		items[936] = new Item("Tormented Pickaxe").setPick(940);
		items[937] = new Item("Soulium Pickaxe").setPick(960);
		items[938] = new Item("Glorium Pickaxe").setPick(980);
		items[939] = new Item("Reapera Pickaxe").setPick(1000);
		items[940] = new Item("Ectoplasmic Pickaxe").setPick(1250);
		items[941] = new Item("Matrii Pickaxe").setPick(1500);
		items[942] = new Item("Quaggon Pickaxe").setPick(1750);
		items[943] = new Item("Cataclysmic Pickaxe").setPick(2000);
		items[944] = new Item("Aeochryn Pickaxe").setPick(2800);
		items[945] = new Item("Pangeon Pickaxe").setPick(4000);

		// Not pickaxes
		items[946] = new Item("Coal");
		items[947] = new Item("Charcoal");
		items[948] = new Item("Torch").setTile(109, false);

		// Axes
		items[949] = new Item("Mud Axe").setAxe(2);
		items[950] = new Item("Wood Axe").setAxe(4);
		items[951] = new Item("Cactus Axe").setAxe(6);
		items[952] = new Item("Stone Axe").setAxe(8);
		items[953] = new Item("Limestone Axe").setAxe(10);
		items[954] = new Item("Tin Axe").setAxe(12);
		items[955] = new Item("Copper Axe").setAxe(14);
		items[956] = new Item("Iron Axe").setAxe(16);
		items[957] = new Item("Lead Axe").setAxe(18);
		items[958] = new Item("Bronze Axe").setAxe(20);
		items[959] = new Item("Steel Axe").setAxe(25);
		items[960] = new Item("Frost Axe").setAxe(30);
		items[961] = new Item("Silver Axe").setAxe(35);
		items[962] = new Item("Gold Axe").setAxe(40);
		items[963] = new Item("Ivory Axe").setAxe(45);
		items[964] = new Item("Ancient Axe").setAxe(50);
		items[965] = new Item("Stellatite Axe").setAxe(55);
		items[966] = new Item("Starcopper Axe").setAxe(58);
		items[967] = new Item("Stariron Axe").setAxe(61);
		items[968] = new Item("Starsteel Axe").setAxe(64);
		items[969] = new Item("Starsilver Axe").setAxe(67);
		items[970] = new Item("Stargold Axe").setAxe(70);
		items[971] = new Item("Honey Axe").setAxe(70);
		items[972] = new Item("Gemstone Axe").setAxe(72);
		items[973] = new Item("Birthday Axe").setAxe(73);
		items[974] = new Item("Coffee Axe").setAxe(80);
		items[975] = new Item("Candy Axe").setAxe(85);
		items[976] = new Item("Dwarf-forge Axe").setAxe(90);
		items[977] = new Item("Star-forge Axe").setAxe(95);
		items[978] = new Item("Meteorite Axe").setAxe(97);
		items[979] = new Item("Comet Axe").setAxe(100);
		items[980] = new Item("Lokiite Axe").setAxe(120);
		items[981] = new Item("Zincite Axe").setAxe(130);
		items[982] = new Item("Barite Axe").setAxe(140);
		items[983] = new Item("Hellfire Axe").setAxe(150);
		items[984] = new Item("Hive Axe").setAxe(175);
		items[985] = new Item("Sanguine Axe").setAxe(200);
		items[986] = new Item("Graveyard Axe").setAxe(210);
		items[987] = new Item("Forsaken Axe").setAxe(220);
		items[988] = new Item("Chromite Axe").setAxe(230);
		items[989] = new Item("Mandibilium Axe").setAxe(240);
		items[990] = new Item("Cerussite Axe").setAxe(250);
		items[991] = new Item("Galena Axe").setAxe(260);
		items[992] = new Item("Cinnabar Axe").setAxe(270);
		items[993] = new Item("Kunzite Axe").setAxe(280);
		items[994] = new Item("Celestine Axe").setAxe(295);
		items[995] = new Item("Andarrian Axe").setAxe(310);
		items[996] = new Item("Columbite Axe").setAxe(325);
		items[997] = new Item("Rhodonite Axe").setAxe(340);
		items[998] = new Item("Heartstone Axe").setAxe(355);
		items[999] = new Item("Cyclostone Axe").setAxe(370);
		items[1000] = new Item("Stibnite Axe").setAxe(385);
		items[1001] = new Item("Autunite Axe").setAxe(400);
		items[1002] = new Item("Sphene Axe").setAxe(415);
		items[1003] = new Item("Vanadinite Axe").setAxe(430);
		items[1004] = new Item("Molybdenite Axe").setAxe(445);
		items[1005] = new Item("Powellite Axe").setAxe(460);
		items[1006] = new Item("Manganite Axe").setAxe(480);
		items[1007] = new Item("Malachite Axe").setAxe(500);
		items[1008] = new Item("Hematite Axe").setAxe(520);
		items[1009] = new Item("Goethite Axe").setAxe(540);
		items[1010] = new Item("Obsidian Axe").setAxe(560);
		items[1011] = new Item("Temporal Axe").setAxe(600);
		items[1012] = new Item("Cobalt Axe").setAxe(620);
		items[1013] = new Item("Blanite Axe").setAxe(640);
		items[1014] = new Item("Lunar Axe").setAxe(659);
		items[1015] = new Item("Solar Axe").setAxe(660);
		items[1016] = new Item("Eclipse Axe").setAxe(661);
		items[1017] = new Item("Ethereal Axe").setAxe(666);
		items[1018] = new Item("Pyrozium Axe").setAxe(670);
		items[1019] = new Item("Erythium Axe").setAxe(700);
		items[1020] = new Item("Charged Erythium Axe").setAxe(730);
		items[1021] = new Item("Dragonbone Axe").setAxe(760);
		items[1022] = new Item("Ichor Axe").setAxe(780);
		items[1023] = new Item("Ambrosia Axe").setAxe(800);
		items[1024] = new Item("Nectar Axe").setAxe(810);
		items[1025] = new Item("Golden Ichor Axe").setAxe(820);
		items[1026] = new Item("Void-forge Axe").setAxe(900);
		items[1027] = new Item("Tortured Axe").setAxe(920);
		items[1028] = new Item("Tormented Axe").setAxe(940);
		items[1029] = new Item("Soulium Axe").setAxe(960);
		items[1030] = new Item("Glorium Axe").setAxe(980);
		items[1031] = new Item("Reapera Axe").setAxe(1000);
		items[1032] = new Item("Ectoplasmic Axe").setAxe(1250);
		items[1033] = new Item("Matrii Axe").setAxe(1500);
		items[1034] = new Item("Quaggon Axe").setAxe(1750);
		items[1035] = new Item("Cataclysmic Axe").setAxe(2000);
		items[1036] = new Item("Aeochryn Axe").setAxe(2800);
		items[1037] = new Item("Pangeon Axe").setAxe(4000);

		// Survival only content
		items[1038] = new ItemFood("Apple", 0.6);
		items[1039] = new ItemFood("Enhanced Apple", 1.2);

		// More content
		items[1040] = new Item("Stone").setTile(2, false);
		items[1041] = new Item("Stone Brick").setTile(107, false);
		items[1042] = new Item("Wood");
		items[1043] = new Item("Workbench").setTile(64, false);
		items[1044] = new Item("Dirt").setTile(1, false);
		items[1045] = new ItemFood("Bacon", 5.4);
		items[1046] = new ItemFood("Enhanced Bacon", 10.8);
		items[1047] = new ItemFood("Raw Pork", 3.2);
		items[1048] = new ItemFood("Raw Beef", 3.3);

		// Hammers
		items[1049] = new Item("Mud Hammer").setHammer(2);
		items[1050] = new Item("Wood Hammer").setHammer(4);
		items[1051] = new Item("Cactus Hammer").setHammer(6);
		items[1052] = new Item("Stone Hammer").setHammer(8);
		items[1053] = new Item("Limestone Hammer").setHammer(10);
		items[1054] = new Item("Tin Hammer").setHammer(12);
		items[1055] = new Item("Copper Hammer").setHammer(14);
		items[1056] = new Item("Iron Hammer").setHammer(16);
		items[1057] = new Item("Lead Hammer").setHammer(18);
		items[1058] = new Item("Bronze Hammer").setHammer(20);
		items[1059] = new Item("Steel Hammer").setHammer(25);
		items[1060] = new Item("Frost Hammer").setHammer(30);
		items[1061] = new Item("Silver Hammer").setHammer(35);
		items[1062] = new Item("Gold Hammer").setHammer(40);
		items[1063] = new Item("Ivory Hammer").setHammer(45);
		items[1064] = new Item("Ancient Hammer").setHammer(50);
		items[1065] = new Item("Stellatite Hammer").setHammer(55);
		items[1066] = new Item("Starcopper Hammer").setHammer(58);
		items[1067] = new Item("Stariron Hammer").setHammer(61);
		items[1068] = new Item("Starsteel Hammer").setHammer(64);
		items[1069] = new Item("Starsilver Hammer").setHammer(67);
		items[1070] = new Item("Stargold Hammer").setHammer(70);
		items[1071] = new Item("Honey Hammer").setHammer(70);
		items[1072] = new Item("Gemstone Hammer").setHammer(72);
		items[1073] = new Item("Birthday Hammer").setHammer(73);
		items[1074] = new Item("Coffee Hammer").setHammer(80);
		items[1075] = new Item("Candy Hammer").setHammer(85);
		items[1076] = new Item("Dwarf-forge Hammer").setHammer(90);
		items[1077] = new Item("Star-forge Hammer").setHammer(95);
		items[1078] = new Item("Meteorite Hammer").setHammer(97);
		items[1079] = new Item("Comet Hammer").setHammer(100);
		items[1080] = new Item("Lokiite Hammer").setHammer(120);
		items[1081] = new Item("Zincite Hammer").setHammer(130);
		items[1082] = new Item("Barite Hammer").setHammer(140);
		items[1083] = new Item("Hellfire Hammer").setHammer(150);
		items[1084] = new Item("Hive Hammer").setHammer(175);
		items[1085] = new Item("Sanguine Hammer").setHammer(200);
		items[1086] = new Item("Graveyard Hammer").setHammer(210);
		items[1087] = new Item("Forsaken Hammer").setHammer(220);
		items[1088] = new Item("Chromite Hammer").setHammer(230);
		items[1089] = new Item("Mandibilium Hammer").setHammer(240);
		items[1090] = new Item("Cerussite Hammer").setHammer(250);
		items[1091] = new Item("Galena Hammer").setHammer(260);
		items[1092] = new Item("Cinnabar Hammer").setHammer(270);
		items[1093] = new Item("Kunzite Hammer").setHammer(280);
		items[1094] = new Item("Celestine Hammer").setHammer(295);
		items[1095] = new Item("Andarrian Hammer").setHammer(310);
		items[1096] = new Item("Columbite Hammer").setHammer(325);
		items[1097] = new Item("Rhodonite Hammer").setHammer(340);
		items[1098] = new Item("Heartstone Hammer").setHammer(355);
		items[1099] = new Item("Cyclostone Hammer").setHammer(370);
		items[1100] = new Item("Stibnite Hammer").setHammer(385);
		items[1101] = new Item("Autunite Hammer").setHammer(400);
		items[1102] = new Item("Sphene Hammer").setHammer(415);
		items[1103] = new Item("Vanadinite Hammer").setHammer(430);
		items[1104] = new Item("Molybdenite Hammer").setHammer(445);
		items[1105] = new Item("Powellite Hammer").setHammer(460);
		items[1106] = new Item("Manganite Hammer").setHammer(480);
		items[1107] = new Item("Malachite Hammer").setHammer(500);
		items[1108] = new Item("Hematite Hammer").setHammer(520);
		items[1109] = new Item("Goethite Hammer").setHammer(540);
		items[1110] = new Item("Obsidian Hammer").setHammer(560);
		items[1111] = new Item("Temporal Hammer").setHammer(600);
		items[1112] = new Item("Cobalt Hammer").setHammer(620);
		items[1113] = new Item("Blanite Hammer").setHammer(640);
		items[1114] = new Item("Lunar Hammer").setHammer(659);
		items[1115] = new Item("Solar Hammer").setHammer(660);
		items[1116] = new Item("Eclipse Hammer").setHammer(661);
		items[1117] = new Item("Ethereal Hammer").setHammer(666);
		items[1118] = new Item("Pyrozium Hammer").setHammer(670);
		items[1119] = new Item("Erythium Hammer").setHammer(700);
		items[1120] = new Item("Charged Erythium Hammer").setHammer(730);
		items[1121] = new Item("Dragonbone Hammer").setHammer(760);
		items[1122] = new Item("Ichor Hammer").setHammer(780);
		items[1123] = new Item("Ambrosia Hammer").setHammer(800);
		items[1124] = new Item("Nectar Hammer").setHammer(810);
		items[1125] = new Item("Golden Ichor Hammer").setHammer(820);
		items[1126] = new Item("Void-forge Hammer").setHammer(900);
		items[1127] = new Item("Tortured Hammer").setHammer(920);
		items[1128] = new Item("Tormented Hammer").setHammer(940);
		items[1129] = new Item("Soulium Hammer").setHammer(960);
		items[1130] = new Item("Glorium Hammer").setHammer(980);
		items[1131] = new Item("Reapera Hammer").setHammer(1000);
		items[1132] = new Item("Ectoplasmic Hammer").setHammer(1250);
		items[1133] = new Item("Matrii Hammer").setHammer(1500);
		items[1134] = new Item("Quaggon Hammer").setHammer(1750);
		items[1135] = new Item("Cataclysmic Hammer").setHammer(2000);
		items[1136] = new Item("Aeochryn Hammer").setHammer(2800);
		items[1137] = new Item("Pangeon Hammer").setHammer(4000);

		items[1138] = new Item("Carbon");
		items[1139] = new Item("Mercury");
		items[1140] = new Item("Tin Amalgam");
		items[1141] = new Item("Copper Amalgam");
		items[1142] = new Item("Lead Amalgam");
		items[1143] = new Item("Iron Amalgam");
		items[1144] = new Item("Silver Amalgam");
		items[1145] = new Item("Gold Amalgam");
		items[1146] = new Item("Amethyst Dust");
		items[1147] = new Item("Emerald Dust");
		items[1148] = new Item("Topaz Dust");
		items[1149] = new Item("Sapphire Dust");
		items[1150] = new Item("Diamond Dust");
		items[1151] = new Item("Ruby Dust");
		items[1152] = new Item("Onyx Dust");
		items[1153] = new Item("Opal Dust");
		items[1154] = new Item("Fertilizer");
		items[1155] = new Item("Corn Seeds");
		items[1156] = new Item("Tomato Seeds");
		items[1157] = new ItemFood("Cherry", 0.5).setMaxStack(2499);
		items[1158] = new ItemFood("Apricot", 1.3);
		items[1159] = new ItemFood("Avocado", 0.9);
		items[1160] = new ItemFood("Blueberry", 0.2);
		items[1161] = new ItemFood("Grape", 0.1).setMaxStack(2499);
		items[1162] = new ItemFood("Raspberry", 0.15).setMaxStack(1499);
		items[1163] = new ItemFood("Strawberry", 0.1).setMaxStack(2499);
		items[1164] = new Item("Potato Seeds");
		items[1165] = new Item("Grass Seeds");
		items[1166] = new Item("Hop Seeds");
		items[1167] = new Item("Red Flower Seeds");
		items[1168] = new Item("Yellow Flower Seeds");
		items[1169] = new Item("Bucket");
		items[1170] = new Item("Water Bucket");
		items[1171] = new Item("Hoe");
		items[1172] = new ItemRanged("Wooden Bow", 5);
		items[1173] = new ItemRanged("Hunting Bow", 8);
		items[1174] = new Item("Snare");
		items[1175] = new Item("Cow Leather (Untanned)");
		items[1176] = new Item("Pig Leather (Untanned)");
		items[1177] = new ItemFood("Raw Uncut Venison", 1.74);
		items[1178] = new Item("Deer Leather (Untanned)");
		items[1179] = new Item("Bear Pelt");
		items[1180] = new Item("Bear Claw");
		items[1181] = new Item("Squirrel Fur");
		items[1182] = new Item("Elk Antler");
		items[1183] = new Item("Mountain Lion Pelt");
		items[1184] = new Item("Weasel Fur");
		items[1185] = new Item("Eagle Feather");
		items[1186] = new ItemFood("Cookie", 0.4).setMaxStack(1499);
		items[1187] = new ItemFood("Enhanced Cookie", 0.8).setMaxStack(1499);
		items[1188] = new Item("Wolf Pelt");
		items[1189] = new Item("Grizzly Bear Pelt");
		items[1190] = new Item("Bobcat Pelt");
		items[1191] = new Item("Moose Antler");
		items[1192] = new Item("Deer Antler");
		items[1193] = new ItemWire("Red Wire", 0, true);
		items[1194] = new ItemWire("Green Wire", 1, true);
		items[1195] = new ItemWire("Purple Wire", 2, true);
		items[1196] = new ItemWire("Orange Wire", 3, true);
		items[1197] = new ItemWire("Blue Wire", 4, true);
		items[1198] = new ItemWire("Yellow Wire", 5, true);
		items[1199] = new ItemWire("Brown Wire", 6, true);
		items[1200] = new ItemWire("Black Wire", 7, true);
		items[1201] = new ItemWire("White Wire", 8, true);
		items[1202] = new ItemWire("Rainbow Wire", 9, true);
		items[1203] = new ItemWire("Connector", 300, false);
		items[1204] = new ItemWire("Wire Cap", 301, false);
		items[1205] = new ItemWire("Cable", 10, true);
		items[1206] = new ItemWire("Cable Connector", 302, false);
		items[1207] = new ItemWire("Black Cable", 11, true);
		items[1208] = new ItemWire("Grey Cable", 12, true);
		items[1209] = new Item("Lynx Pelt");
		items[1210] = new Item("Coyote Pelt");
		items[1211] = new Item("Beaver Fur");
		items[1212] = new Item("Wolf Fang");
		items[1213] = new Item("Bear Tooth");
		items[1214] = new Item("Rabbit Pelt");
		items[1215] = new Item("Glass Bottle");
		items[1216] = new Item("Glass Flask");
		items[1217] = new Item("Glass Jar");
		items[1218] = new Item("Metal Flask");
		items[1219] = new Item("Potion Bench");
		items[1220] = new Item("Cauldron");
		items[1221] = new Item("Flask of Water");
		items[1222] = new Item("Bottle of Water");
		items[1223] = new Item("Flash Powder");
		items[1224] = new Item("Fire Essence");
		items[1225] = new Item("Water Essence");
		items[1226] = new Item("Air Essence");
		items[1227] = new Item("Earth Essence");
		items[1228] = new Item("Santher Acid");
		items[1229] = new Item("Piffel Acid");
		items[1230] = new Item("Yaxor Acid");
		items[1231] = new Item("Tincture of Bellanox");
		items[1232] = new Item("Tincture of Sippela");
		items[1233] = new Item("Tincture of Myrok");
		items[1234] = new Item("Jahoonic Acid");
		items[1235] = new Item("Flintoc Acid");
		items[1236] = new Item("Tincture of Yggsdrin");
		items[1237] = new Item("Aqua Reaga");
		items[1238] = new Item("Copper Dust");
		items[1239] = new Item("Tin Dust");
		items[1240] = new Item("Lead Dust");
		items[1241] = new Item("Iron Dust");
		items[1242] = new Item("Silver Dust");
		items[1243] = new Item("Gold Dust");
		items[1244] = new Item("Salt of Copper");
		items[1245] = new Item("Salt of Tin");
		items[1246] = new Item("Salt of Lead");
		items[1247] = new Item("Salt of Iron");
		items[1248] = new Item("Salt of Silver");
		items[1249] = new Item("Salt of Gold");
		items[1250] = new Item("Magnesium Dust");
		items[1251] = new Item("Milk of Magnesium");
		items[1252] = new Item("Salt of Magnesium");
		items[1253] = new Item("Calcium Powder");
		items[1254] = new Item("Talc Powder");
		items[1255] = new Item("Erydium Shavings");
		items[1256] = new Item("Lithonite");
		items[1257] = new Item("Sodomiite");
		items[1258] = new Item("Erydiniite");
		items[1259] = new Item("Fyrno Reaga");
		items[1260] = new Item("Ventro Reaga");
		items[1261] = new Item("Geo Reaga");
		items[1262] = new Item("Spectra Reaga");
		items[1263] = new Item("Sulfous Vial");
		items[1264] = new Item("Hydros Vial");
		items[1265] = new Item("Nitrous Vial");
		items[1266] = new Item("Cyanidic Vial");
		items[1267] = new Item("Ferrous Vial");
		items[1268] = new Item("Vouxus Essence");

		// Herbs and potion ingredients
		items[1269] = new Item("Alpine Lichen");
		items[1270] = new Item("Alpine Fir Bark");
		items[1271] = new Item("Alpine Wolf Fur");
		items[1272] = new Item("Bloodclaw");
		items[1273] = new Item("Sapphire Butterfly Wing");
		items[1274] = new Item("Mountainroot");
		items[1275] = new Item("Wortleroot");
		items[1276] = new Item("Skagit Egg");
		items[1277] = new Item("Syrax Leaf");
		items[1278] = new Item("Aloe Leaf");
		items[1279] = new Item("Bee Wing");
		items[1280] = new Item("Lavendar");
		items[1281] = new Item("Frostnip");
		items[1282] = new Item("Sprawler");
		items[1283] = new Item("Mountain Butterfly Wing");
		items[1284] = new Item("Ruby Butterfly Wing");
		items[1285] = new Item("Geode");
		items[1286] = new Item("Firebulb");
		items[1287] = new Item("Frosty Nymph");
		items[1288] = new Item("Fiery Nymph");
		items[1289] = new Item("Aqua Nymph");
		items[1290] = new Item("Veracita");
		items[1291] = new Item("Amorellina Mushroom");
		items[1292] = new Item("Belladox Mushroom");
		items[1293] = new Item("Connery Stem");
		items[1294] = new Item("Aribarzin Stem");
		items[1295] = new Item("Yahaxin Mushroom");
		items[1296] = new Item("Yhivl Leaf");
		items[1297] = new Item("Frost Salt");
		items[1298] = new Item("Fire Salt");
		items[1299] = new Item("Void Salt");
		items[1300] = new Item("Jasmine");

		items[1301] = new Item("Wooden Ladder").setTile(129, false);

		// Shifters
		items[1302] = new Item("Mud Shifter").setShifter(2);
		items[1303] = new Item("Wood Shifter").setShifter(4);
		items[1304] = new Item("Cactus Shifter").setShifter(6);
		items[1305] = new Item("Stone Shifter").setShifter(8);
		items[1306] = new Item("Limestone Shifter").setShifter(10);
		items[1307] = new Item("Tin Shifter").setShifter(12);
		items[1308] = new Item("Copper Shifter").setShifter(14);
		items[1309] = new Item("Iron Shifter").setShifter(16);
		items[1310] = new Item("Lead Shifter").setShifter(18);
		items[1311] = new Item("Bronze Shifter").setShifter(20);
		items[1312] = new Item("Steel Shifter").setShifter(25);
		items[1313] = new Item("Frost Shifter").setShifter(30);
		items[1314] = new Item("Silver Shifter").setShifter(35);
		items[1315] = new Item("Gold Shifter").setShifter(40);
		items[1316] = new Item("Ivory Shifter").setShifter(45);
		items[1317] = new Item("Ancient Shifter").setShifter(50);
		items[1318] = new Item("Stellatite Shifter").setShifter(55);
		items[1319] = new Item("Starcopper Shifter").setShifter(58);
		items[1320] = new Item("Stariron Shifter").setShifter(61);
		items[1321] = new Item("Starsteel Shifter").setShifter(64);
		items[1322] = new Item("Starsilver Shifter").setShifter(67);
		items[1323] = new Item("Stargold Shifter").setShifter(70);
		items[1324] = new Item("Honey Shifter").setShifter(70);
		items[1325] = new Item("Gemstone Shifter").setShifter(72);
		items[1326] = new Item("Birthday Shifter").setShifter(73);
		items[1327] = new Item("Coffee Shifter").setShifter(80);
		items[1328] = new Item("Candy Shifter").setShifter(85);
		items[1329] = new Item("Dwarf-forge Shifter").setShifter(90);
		items[1330] = new Item("Star-forge Shifter").setShifter(95);
		items[1331] = new Item("Meteorite Shifter").setShifter(97);
		items[1332] = new Item("Comet Shifter").setShifter(100);
		items[1333] = new Item("Lokiite Shifter").setShifter(120);
		items[1334] = new Item("Zincite Shifter").setShifter(130);
		items[1335] = new Item("Barite Shifter").setShifter(140);
		items[1336] = new Item("Hellfire Shifter").setShifter(150);
		items[1337] = new Item("Hive Shifter").setShifter(175);
		items[1338] = new Item("Sanguine Shifter").setShifter(200);
		items[1339] = new Item("Graveyard Shifter").setShifter(210);
		items[1340] = new Item("Forsaken Shifter").setShifter(220);
		items[1341] = new Item("Chromite Shifter").setShifter(230);
		items[1342] = new Item("Mandibilium Shifter").setShifter(240);
		items[1343] = new Item("Cerussite Shifter").setShifter(250);
		items[1344] = new Item("Galena Shifter").setShifter(260);
		items[1345] = new Item("Cinnabar Shifter").setShifter(270);
		items[1346] = new Item("Kunzite Shifter").setShifter(280);
		items[1347] = new Item("Celestine Shifter").setShifter(295);
		items[1348] = new Item("Andarrian Shifter").setShifter(310);
		items[1349] = new Item("Columbite Shifter").setShifter(325);
		items[1350] = new Item("Rhodonite Shifter").setShifter(340);
		items[1351] = new Item("Heartstone Shifter").setShifter(355);
		items[1352] = new Item("Cyclostone Shifter").setShifter(370);
		items[1353] = new Item("Stibnite Shifter").setShifter(385);
		items[1354] = new Item("Autunite Shifter").setShifter(400);
		items[1355] = new Item("Sphene Shifter").setShifter(415);
		items[1356] = new Item("Vanadinite Shifter").setShifter(430);
		items[1357] = new Item("Molybdenite Shifter").setShifter(445);
		items[1358] = new Item("Powellite Shifter").setShifter(460);
		items[1359] = new Item("Manganite Shifter").setShifter(480);
		items[1360] = new Item("Malachite Shifter").setShifter(500);
		items[1361] = new Item("Hematite Shifter").setShifter(520);
		items[1362] = new Item("Goethite Shifter").setShifter(540);
		items[1363] = new Item("Obsidian Shifter").setShifter(560);
		items[1364] = new Item("Temporal Shifter").setShifter(600);
		items[1365] = new Item("Cobalt Shifter").setShifter(620);
		items[1366] = new Item("Blanite Shifter").setShifter(640);
		items[1367] = new Item("Lunar Shifter").setShifter(659);
		items[1368] = new Item("Solar Shifter").setShifter(660);
		items[1369] = new Item("Eclipse Shifter").setShifter(661);
		items[1370] = new Item("Ethereal Shifter").setShifter(666);
		items[1371] = new Item("Pyrozium Shifter").setShifter(670);
		items[1372] = new Item("Erythium Shifter").setShifter(700);
		items[1373] = new Item("Charged Erythium Shifter").setShifter(730);
		items[1374] = new Item("Dragonbone Shifter").setShifter(760);
		items[1375] = new Item("Ichor Shifter").setShifter(780);
		items[1376] = new Item("Ambrosia Shifter").setShifter(800);
		items[1377] = new Item("Nectar Shifter").setShifter(810);
		items[1378] = new Item("Golden Ichor Shifter").setShifter(820);
		items[1379] = new Item("Void-forge Shifter").setShifter(900);
		items[1380] = new Item("Tortured Shifter").setShifter(920);
		items[1381] = new Item("Tormented Shifter").setShifter(940);
		items[1382] = new Item("Soulium Shifter").setShifter(960);
		items[1383] = new Item("Glorium Shifter").setShifter(980);
		items[1384] = new Item("Reapera Shifter").setShifter(1000);
		items[1385] = new Item("Ectoplasmic Shifter").setShifter(1250);
		items[1386] = new Item("Matrii Shifter").setShifter(1500);
		items[1387] = new Item("Quaggon Shifter").setShifter(1750);
		items[1388] = new Item("Cataclysmic Shifter").setShifter(2000);
		items[1389] = new Item("Aeochryn Shifter").setShifter(2800);
		items[1390] = new Item("Pangeon Shifter").setShifter(4000);

		items[1391] = new Item("Mystic Stick").setOwner(845);
		items[1392] = new Item("Acacia Wood").setOwner(1042);
		items[1393] = new Item("Oak Wood").setOwner(1042); //Deprecated, not used
		items[1394] = new Item("Redwood Wood").setOwner(1042);
		items[1395] = new Item("Spruce Wood").setOwner(1042);
		items[1396] = new Item("Fir Wood").setOwner(1042);
		items[1397] = new Item("Larch Wood").setOwner(1042);
		items[1398] = new Item("Pine Wood").setOwner(1042);
		items[1399] = new Item("Yew Wood").setOwner(1042);
		items[1400] = new Item("Cedar Wood").setOwner(1042);
		items[1401] = new Item("Cypress Wood").setOwner(1042);

		items[1402] = new ItemMelee("Rift Blade", 127);
		items[1403] = new ItemMelee("Ahketahnic Blade", 53);

		items[1404] = new Item("Mysterious Wood").setOwner(1042);
		items[1405] = new Item("Mahogany Wood").setOwner(1042);
		items[1406] = new Item("Birch Wood").setOwner(1042);
		items[1407] = new Item("Beech Wood").setOwner(1042);
		items[1408] = new Item("Cherry Wood").setOwner(1042);

		items[1409] = new Item("Exotic Wood").setOwner(1042);
		items[1410] = new Item("Exotic Mystic Wood").setOwner(1042);
		items[1411] = new Item("Exotic Acacia Wood").setOwner(1042);
		items[1412] = new Item("Exotic Redwood Wood").setOwner(1042);
		items[1413] = new Item("Exotic Spruce Wood").setOwner(1042);
		items[1414] = new Item("Exotic Fir Wood").setOwner(1042);
		items[1415] = new Item("Exotic Larch Wood").setOwner(1042);
		items[1416] = new Item("Exotic Pine Wood").setOwner(1042);
		items[1417] = new Item("Exotic Yew Wood").setOwner(1042);
		items[1418] = new Item("Exotic Cedar Wood").setOwner(1042);
		items[1419] = new Item("Exotic Cypress Wood").setOwner(1042);
		items[1420] = new Item("Exotic Mysterious Wood").setOwner(1042);
		items[1421] = new Item("Exotic Mahogany Wood").setOwner(1042);
		items[1422] = new Item("Exotic Birch Wood").setOwner(1042);
		items[1423] = new Item("Exotic Beech Wood").setOwner(1042);
		items[1424] = new Item("Exotic Cherry Wood").setOwner(1042);
		items[1425] = new Item("Rich Wood").setOwner(1042);
		items[1426] = new Item("Rich Mystic Wood").setOwner(1042);
		items[1427] = new Item("Rich Acacia Wood").setOwner(1042);
		items[1428] = new Item("Rich Redwood Wood").setOwner(1042);
		items[1429] = new Item("Rich Spruce Wood").setOwner(1042);
		items[1430] = new Item("Rich Fir Wood").setOwner(1042);
		items[1431] = new Item("Rich Larch Wood").setOwner(1042);
		items[1432] = new Item("Rich Pine Wood").setOwner(1042);
		items[1433] = new Item("Rich Yew Wood").setOwner(1042);
		items[1434] = new Item("Rich Cedar Wood").setOwner(1042);
		items[1435] = new Item("Rich Cypress Wood").setOwner(1042);
		items[1436] = new Item("Rich Mysterious Wood").setOwner(1042);
		items[1437] = new Item("Rich Mahogany Wood").setOwner(1042);
		items[1438] = new Item("Rich Birch Wood").setOwner(1042);
		items[1439] = new Item("Rich Beech Wood").setOwner(1042);
		items[1440] = new Item("Rich Cherry Wood").setOwner(1042);
		items[1441] = new Item("Bamboo").setOwner(1042);
		items[1442] = new Item("Polished Bamboo").setOwner(1042);
		items[1443] = new Item("Wood Plank");
		items[1444] = new Item("Mystic Wood Plank").setOwner(1443);
		items[1445] = new Item("Acacia Wood Plank").setOwner(1443);
		items[1446] = new Item("Redwood Wood Plank").setOwner(1443);
		items[1447] = new Item("Spruce Wood Plank").setOwner(1443);
		items[1448] = new Item("Fir Wood Plank").setOwner(1443);
		items[1449] = new Item("Larch Wood Plank").setOwner(1443);
		items[1450] = new Item("Pine Wood Plank").setOwner(1443);
		items[1451] = new Item("Yew Wood Plank").setOwner(1443);
		items[1452] = new Item("Cedar Wood Plank").setOwner(1443);
		items[1453] = new Item("Cypress Wood Plank").setOwner(1443);
		items[1454] = new Item("Mysterious Wood Plank").setOwner(1443);
		items[1455] = new Item("Mahogany Wood Plank").setOwner(1443);
		items[1456] = new Item("Birch Wood Plank").setOwner(1443);
		items[1457] = new Item("Beech Wood Plank").setOwner(1443);
		items[1458] = new Item("Cherry Wood Plank").setOwner(1443);
		items[1459] = new Item("Bamboo Plank").setOwner(1443);
		items[1460] = new Item("Wooden Platform").setTile(44, false);
		items[1461] = new Item("Mystic Platform").setTile(168, false);
		items[1462] = new Item("Acacia Platform").setTile(169, false);
		items[1463] = new Item("Redwood Platform").setTile(170, false);
		items[1464] = new Item("Spruce Platform").setTile(171, false);
		items[1465] = new Item("Fir Platform").setTile(172, false);
		items[1466] = new Item("Larch Platform").setTile(173, false);
		items[1467] = new Item("Pine Platform").setTile(174, false);
		items[1468] = new Item("Yew Platform").setTile(175, false);
		items[1469] = new Item("Cedar Platform").setTile(176, false);
		items[1470] = new Item("Cypress Platform").setTile(177, false);
		items[1471] = new Item("Mysterious Platform").setTile(178, false);
		items[1472] = new Item("Mahogany Platform").setTile(179, false);
		items[1473] = new Item("Birch Platform").setTile(180, false);
		items[1474] = new Item("Beech Platform").setTile(181, false);
		items[1475] = new Item("Cherry Platform").setTile(182, false);
		items[1476] = new Item("Bamboo Platform").setTile(183, false);
		items[1477] = new Item("Pack of Cards");
		items[1478] = new Item("Green Poker Chip");
		items[1479] = new Item("Red Poker Chip");
		items[1480] = new Item("Blue Poker Chip");
		items[1481] = new Item("Black Poker Chip");
		items[1482] = new Item("Ace of Hearts");
		items[1483] = new Item("Deuce of Hearts");
		items[1484] = new Item("3 of Hearts");
		items[1485] = new Item("4 of Hearts");
		items[1486] = new Item("5 of Hearts");
		items[1487] = new Item("6 of Hearts");
		items[1488] = new Item("7 of Hearts");
		items[1489] = new Item("8 of Hearts");
		items[1490] = new Item("9 of Hearts");
		items[1491] = new Item("10 of Hearts");
		items[1492] = new Item("Jack of Hearts");
		items[1493] = new Item("Queen of Hearts");
		items[1494] = new Item("King of Hearts");
		items[1495] = new Item("Ace of Spades");
		items[1496] = new Item("Deuce of Spades");
		items[1497] = new Item("3 of Spades");
		items[1498] = new Item("4 of Spades");
		items[1499] = new Item("5 of Spades");
		items[1500] = new Item("6 of Spades");
		items[1501] = new Item("7 of Spades");
		items[1502] = new Item("8 of Spades");
		items[1503] = new Item("9 of Spades");
		items[1504] = new Item("10 of Spades");
		items[1505] = new Item("Jack of Spades");
		items[1506] = new Item("Queen of Spades");
		items[1507] = new Item("King of Spades");
		items[1508] = new Item("Ace of Clubs");
		items[1509] = new Item("Deuce of Clubs");
		items[1510] = new Item("3 of Clubs");
		items[1511] = new Item("4 of Clubs");
		items[1512] = new Item("5 of Clubs");
		items[1513] = new Item("6 of Clubs");
		items[1514] = new Item("7 of Clubs");
		items[1515] = new Item("8 of Clubs");
		items[1516] = new Item("9 of Clubs");
		items[1517] = new Item("10 of Clubs");
		items[1518] = new Item("Jack of Clubs");
		items[1519] = new Item("Queen of Clubs");
		items[1520] = new Item("King of Clubs");
		items[1521] = new Item("Ace of Diamonds");
		items[1522] = new Item("Deuce of Diamonds");
		items[1523] = new Item("3 of Diamonds");
		items[1524] = new Item("4 of Diamonds");
		items[1525] = new Item("5 of Diamonds");
		items[1526] = new Item("6 of Diamonds");
		items[1527] = new Item("7 of Diamonds");
		items[1528] = new Item("8 of Diamonds");
		items[1529] = new Item("9 of Diamonds");
		items[1530] = new Item("10 of Diamonds");
		items[1531] = new Item("Jack of Diamonds");
		items[1532] = new Item("Queen of Diamonds");
		items[1533] = new Item("King of Diamonds");
		items[1534] = new Item("Joker");
		items[1535] = new Item("Ornate Pack of Cards");
		items[1536] = new Item("Ornate Green Poker Chip");
		items[1537] = new Item("Ornate Red Poker Chip");
		items[1538] = new Item("Ornate Blue Poker Chip");
		items[1539] = new Item("Ornate Black Poker Chip");
		items[1540] = new Item("Ornate Ace of Hearts");
		items[1541] = new Item("Ornate Deuce of Hearts");
		items[1542] = new Item("Ornate 3 of Hearts");
		items[1543] = new Item("Ornate 4 of Hearts");
		items[1544] = new Item("Ornate 5 of Hearts");
		items[1545] = new Item("Ornate 6 of Hearts");
		items[1546] = new Item("Ornate 7 of Hearts");
		items[1547] = new Item("Ornate 8 of Hearts");
		items[1548] = new Item("Ornate 9 of Hearts");
		items[1549] = new Item("Ornate 10 of Hearts");
		items[1550] = new Item("Ornate Jack of Hearts");
		items[1551] = new Item("Ornate Queen of Hearts");
		items[1552] = new Item("Ornate King of Hearts");
		items[1553] = new Item("Ornate Ace of Spades");
		items[1554] = new Item("Ornate Deuce of Spades");
		items[1555] = new Item("Ornate 3 of Spades");
		items[1556] = new Item("Ornate 4 of Spades");
		items[1557] = new Item("Ornate 5 of Spades");
		items[1558] = new Item("Ornate 6 of Spades");
		items[1559] = new Item("Ornate 7 of Spades");
		items[1560] = new Item("Ornate 8 of Spades");
		items[1561] = new Item("Ornate 9 of Spades");
		items[1562] = new Item("Ornate 10 of Spades");
		items[1563] = new Item("Ornate Jack of Spades");
		items[1564] = new Item("Ornate Queen of Spades");
		items[1565] = new Item("Ornate King of Spades");
		items[1566] = new Item("Ornate Ace of Clubs");
		items[1567] = new Item("Ornate Deuce of Clubs");
		items[1568] = new Item("Ornate 3 of Clubs");
		items[1569] = new Item("Ornate 4 of Clubs");
		items[1570] = new Item("Ornate 5 of Clubs");
		items[1571] = new Item("Ornate 6 of Clubs");
		items[1572] = new Item("Ornate 7 of Clubs");
		items[1573] = new Item("Ornate 8 of Clubs");
		items[1574] = new Item("Ornate 9 of Clubs");
		items[1575] = new Item("Ornate 10 of Clubs");
		items[1576] = new Item("Ornate Jack of Clubs");
		items[1577] = new Item("Ornate Queen of Clubs");
		items[1578] = new Item("Ornate King of Clubs");
		items[1579] = new Item("Ornate Ace of Diamonds");
		items[1580] = new Item("Ornate Deuce of Diamonds");
		items[1581] = new Item("Ornate 3 of Diamonds");
		items[1582] = new Item("Ornate 4 of Diamonds");
		items[1583] = new Item("Ornate 5 of Diamonds");
		items[1584] = new Item("Ornate 6 of Diamonds");
		items[1585] = new Item("Ornate 7 of Diamonds");
		items[1586] = new Item("Ornate 8 of Diamonds");
		items[1587] = new Item("Ornate 9 of Diamonds");
		items[1588] = new Item("Ornate 10 of Diamonds");
		items[1589] = new Item("Ornate Jack of Diamonds");
		items[1590] = new Item("Ornate Queen of Diamonds");
		items[1591] = new Item("Ornate King of Diamonds");
		items[1592] = new Item("Ornate Joker");
		items[1593] = new Item("Campfire").setTile(158, false);
		items[1594] = new Item("Brush");
		items[1595] = new Item("Kindling");
		items[1596] = new Item("Twigs");
		items[1597] = new Item("Pitch");
		items[1598] = new Item("Steam Boiler").setTile(159, false);
		items[1599] = new Item("Basic Tent").setTile(160, false);
		items[1600] = new Item("Steampunk Brick").setTile(164, false);
		items[1601] = new Item("Golden Steampunk Brick").setTile(165, false);
		items[1602] = new Item("Palm Wood").setOwner(1042);
		items[1603] = new Item("Exotic Palm Wood").setOwner(1042);
		items[1604] = new Item("Rich Palm Wood").setOwner(1042);
		items[1605] = new Item("Palm Platform").setTile(184, false);
		items[1606] = new Item("Rich Palm Wood Plank").setOwner(1443);
		items[1607] = new Item("Rich Wooden Platform").setTile(185, false);
		items[1608] = new Item("Rich Mystic Platform").setTile(186, false);
		items[1609] = new Item("Rich Acacia Platform").setTile(187, false);
		items[1610] = new Item("Rich Redwood Platform").setTile(188, false);
		items[1611] = new Item("Rich Spruce Platform").setTile(189, false);
		items[1612] = new Item("Rich Fir Platform").setTile(190, false);
		items[1613] = new Item("Rich Larch Platform").setTile(191, false);
		items[1614] = new Item("Rich Pine Platform").setTile(192, false);
		items[1615] = new Item("Rich Yew Platform").setTile(193, false);
		items[1616] = new Item("Rich Cedar Platform").setTile(194, false);
		items[1617] = new Item("Rich Cypress Platform").setTile(195, false);
		items[1618] = new Item("Rich Mysterious Platform").setTile(196, false);
		items[1619] = new Item("Rich Mahogany Platform").setTile(197, false);
		items[1620] = new Item("Rich Birch Platform").setTile(198, false);
		items[1621] = new Item("Rich Beech Platform").setTile(199, false);
		items[1622] = new Item("Rich Cherry Platform").setTile(200, false);
		items[1623] = new Item("Rich Bamboo Platform").setTile(201, false);
		items[1624] = new Item("Rich Palm Platform").setTile(202, false);
		items[1625] = new Item("Exotic Wooden Platform").setTile(203, false);
		items[1626] = new Item("Exotic Mystic Platform").setTile(204, false);
		items[1627] = new Item("Exotic Acacia Platform").setTile(205, false);
		items[1628] = new Item("Exotic Redwood Platform").setTile(206, false);
		items[1629] = new Item("Exotic Spruce Platform").setTile(207, false);
		items[1630] = new Item("Exotic Fir Platform").setTile(208, false);
		items[1631] = new Item("Exotic Larch Platform").setTile(209, false);
		items[1632] = new Item("Exotic Pine Platform").setTile(210, false);
		items[1633] = new Item("Exotic Yew Platform").setTile(211, false);
		items[1634] = new Item("Exotic Cedar Platform").setTile(212, false);
		items[1635] = new Item("Exotic Cypress Platform").setTile(213, false);
		items[1636] = new Item("Exotic Mysterious Platform").setTile(214, false);
		items[1637] = new Item("Exotic Mahogany Platform").setTile(215, false);
		items[1638] = new Item("Exotic Birch Platform").setTile(216, false);
		items[1639] = new Item("Exotic Beech Platform").setTile(217, false);
		items[1640] = new Item("Exotic Cherry Platform").setTile(218, false);
		items[1641] = new Item("Exotic Bamboo Platform").setTile(219, false);
		items[1642] = new Item("Exotic Palm Platform").setTile(220, false);
		items[1643] = new Item("Wooden Chair").setTile(221, false);
		items[1644] = new Item("Mystic Chair").setTile(222, false);
		items[1645] = new Item("Acacia Chair").setTile(223, false);
		items[1646] = new Item("Redwood Chair").setTile(224, false);
		items[1647] = new Item("Spruce Chair").setTile(225, false);
		items[1648] = new Item("Fir Chair").setTile(226, false);
		items[1649] = new Item("Larch Chair").setTile(227, false);
		items[1650] = new Item("Pine Chair").setTile(228, false);
		items[1651] = new Item("Yew Chair").setTile(229, false);
		items[1652] = new Item("Cedar Chair").setTile(230, false);
		items[1653] = new Item("Cypress Chair").setTile(231, false);
		items[1654] = new Item("Mysterious Chair").setTile(232, false);
		items[1655] = new Item("Mahogany Chair").setTile(233, false);
		items[1656] = new Item("Birch Chair").setTile(234, false);
		items[1657] = new Item("Beech Chair").setTile(235, false);
		items[1658] = new Item("Cherry Chair").setTile(236, false);
		items[1659] = new Item("Bamboo Chair").setTile(237, false);
		items[1660] = new Item("Palm Chair").setTile(238, false);
		items[1661] = new Item("Rich Wooden Chair").setTile(239, false);
		items[1662] = new Item("Rich Mystic Chair").setTile(240, false);
		items[1663] = new Item("Rich Acacia Chair").setTile(241, false);
		items[1664] = new Item("Rich Redwood Chair").setTile(242, false);
		items[1665] = new Item("Rich Spruce Chair").setTile(243, false);
		items[1666] = new Item("Rich Fir Chair").setTile(244, false);
		items[1667] = new Item("Rich Larch Chair").setTile(245, false);
		items[1668] = new Item("Rich Pine Chair").setTile(246, false);
		items[1669] = new Item("Rich Yew Chair").setTile(247, false);
		items[1670] = new Item("Rich Cedar Chair").setTile(248, false);
		items[1671] = new Item("Rich Cypress Chair").setTile(249, false);
		items[1672] = new Item("Rich Mysterious Chair").setTile(250, false);
		items[1673] = new Item("Rich Mahogany Chair").setTile(251, false);
		items[1674] = new Item("Rich Birch Chair").setTile(252, false);
		items[1675] = new Item("Rich Beech Chair").setTile(253, false);
		items[1676] = new Item("Rich Cherry Chair").setTile(254, false);
		items[1677] = new Item("Rich Bamboo Chair").setTile(255, false);
		items[1678] = new Item("Rich Palm Chair").setTile(256, false);
		items[1679] = new Item("Exotic Wooden Chair").setTile(257, false);
		items[1680] = new Item("Exotic Mystic Chair").setTile(258, false);
		items[1681] = new Item("Exotic Acacia Chair").setTile(259, false);
		items[1682] = new Item("Exotic Redwood Chair").setTile(260, false);
		items[1683] = new Item("Exotic Spruce Chair").setTile(261, false);
		items[1684] = new Item("Exotic Fir Chair").setTile(262, false);
		items[1685] = new Item("Exotic Larch Chair").setTile(263, false);
		items[1686] = new Item("Exotic Pine Chair").setTile(264, false);
		items[1687] = new Item("Exotic Yew Chair").setTile(265, false);
		items[1688] = new Item("Exotic Cedar Chair").setTile(266, false);
		items[1689] = new Item("Exotic Cypress Chair").setTile(267, false);
		items[1690] = new Item("Exotic Mysterious Chair").setTile(268, false);
		items[1691] = new Item("Exotic Mahogany Chair").setTile(269, false);
		items[1692] = new Item("Exotic Birch Chair").setTile(270, false);
		items[1693] = new Item("Exotic Beech Chair").setTile(271, false);
		items[1694] = new Item("Exotic Cherry Chair").setTile(272, false);
		items[1695] = new Item("Exotic Bamboo Chair").setTile(273, false);
		items[1696] = new Item("Exotic Palm Chair").setTile(274, false);
		items[1697] = new Item("Exotic Bamboo").setOwner(1042);
		items[1698] = new Item("Rich Bamboo").setOwner(1042);
		items[1699] = new Item("Wooden Table").setTile(275, false);
		items[1700] = new Item("Mystic Table").setTile(276, false);
		items[1701] = new Item("Acacia Table").setTile(277, false);
		items[1702] = new Item("Redwood Table").setTile(278, false);
		items[1703] = new Item("Spruce Table").setTile(279, false);
		items[1704] = new Item("Fir Table").setTile(280, false);
		items[1705] = new Item("Larch Table").setTile(281, false);
		items[1706] = new Item("Pine Table").setTile(282, false);
		items[1707] = new Item("Yew Table").setTile(283, false);
		items[1708] = new Item("Cedar Table").setTile(284, false);
		items[1709] = new Item("Cypress Table").setTile(285, false);
		items[1710] = new Item("Mysterious Table").setTile(286, false);
		items[1711] = new Item("Mahogany Table").setTile(287, false);
		items[1712] = new Item("Birch Table").setTile(288, false);
		items[1713] = new Item("Beech Table").setTile(289, false);
		items[1714] = new Item("Cherry Table").setTile(290, false);
		items[1715] = new Item("Bamboo Table").setTile(291, false);
		items[1716] = new Item("Palm Table").setTile(292, false);
		items[1717] = new Item("Rich Wooden Table").setTile(293, false);
		items[1718] = new Item("Rich Mystic Table").setTile(294, false);
		items[1719] = new Item("Rich Acacia Table").setTile(295, false);
		items[1720] = new Item("Rich Redwood Table").setTile(296, false);
		items[1721] = new Item("Rich Spruce Table").setTile(297, false);
		items[1722] = new Item("Rich Fir Table").setTile(298, false);
		items[1723] = new Item("Rich Larch Table").setTile(299, false);
		items[1724] = new Item("Rich Pine Table").setTile(300, false);
		items[1725] = new Item("Rich Yew Table").setTile(301, false);
		items[1726] = new Item("Rich Cedar Table").setTile(302, false);
		items[1727] = new Item("Rich Cypress Table").setTile(303, false);
		items[1728] = new Item("Rich Mysterious Table").setTile(304, false);
		items[1729] = new Item("Rich Mahogany Table").setTile(305, false);
		items[1730] = new Item("Rich Birch Table").setTile(306, false);
		items[1731] = new Item("Rich Beech Table").setTile(307, false);
		items[1732] = new Item("Rich Cherry Table").setTile(308, false);
		items[1733] = new Item("Rich Bamboo Table").setTile(309, false);
		items[1734] = new Item("Rich Palm Table").setTile(310, false);
		items[1735] = new Item("Exotic Wooden Table").setTile(311, false);
		items[1736] = new Item("Exotic Mystic Table").setTile(312, false);
		items[1737] = new Item("Exotic Acacia Table").setTile(313, false);
		items[1738] = new Item("Exotic Redwood Table").setTile(314, false);
		items[1739] = new Item("Exotic Spruce Table").setTile(315, false);
		items[1740] = new Item("Exotic Fir Table").setTile(316, false);
		items[1741] = new Item("Exotic Larch Table").setTile(317, false);
		items[1742] = new Item("Exotic Pine Table").setTile(318, false);
		items[1743] = new Item("Exotic Yew Table").setTile(319, false);
		items[1744] = new Item("Exotic Cedar Table").setTile(320, false);
		items[1745] = new Item("Exotic Cypress Table").setTile(321, false);
		items[1746] = new Item("Exotic Mysterious Table").setTile(322, false);
		items[1747] = new Item("Exotic Mahogany Table").setTile(323, false);
		items[1748] = new Item("Exotic Birch Table").setTile(324, false);
		items[1749] = new Item("Exotic Beech Table").setTile(325, false);
		items[1750] = new Item("Exotic Cherry Table").setTile(326, false);
		items[1751] = new Item("Exotic Bamboo Table").setTile(327, false);
		items[1752] = new Item("Exotic Palm Table").setTile(328, false);
		
		items[1753] = new Item("Wooden Couch").setTile(329, false);
		items[1754] = new Item("Mystic Couch").setTile(330, false);
		items[1755] = new Item("Acacia Couch").setTile(331, false);
		items[1756] = new Item("Redwood Couch").setTile(332, false);
		items[1757] = new Item("Spruce Couch").setTile(333, false);
		items[1758] = new Item("Fir Couch").setTile(334, false);
		items[1759] = new Item("Larch Couch").setTile(335, false);
		items[1760] = new Item("Pine Couch").setTile(336, false);
		items[1761] = new Item("Yew Couch").setTile(337, false);
		items[1762] = new Item("Cedar Couch").setTile(338, false);
		items[1763] = new Item("Cypress Couch").setTile(339, false);
		items[1764] = new Item("Mysterious Couch").setTile(340, false);
		items[1765] = new Item("Mahogany Couch").setTile(341, false);
		items[1766] = new Item("Birch Couch").setTile(342, false);
		items[1767] = new Item("Beech Couch").setTile(343, false);
		items[1768] = new Item("Cherry Couch").setTile(344, false);
		items[1769] = new Item("Bamboo Couch").setTile(345, false);
		items[1770] = new Item("Palm Couch").setTile(346, false);
		items[1771] = new Item("Rich Wooden Couch").setTile(347, false);
		items[1772] = new Item("Rich Mystic Couch").setTile(348, false);
		items[1773] = new Item("Rich Acacia Couch").setTile(349, false);
		items[1774] = new Item("Rich Redwood Couch").setTile(350, false);
		items[1775] = new Item("Rich Spruce Couch").setTile(351, false);
		items[1776] = new Item("Rich Fir Couch").setTile(352, false);
		items[1777] = new Item("Rich Larch Couch").setTile(353, false);
		items[1778] = new Item("Rich Pine Couch").setTile(354, false);
		items[1779] = new Item("Rich Yew Couch").setTile(355, false);
		items[1780] = new Item("Rich Cedar Couch").setTile(356, false);
		items[1781] = new Item("Rich Cypress Couch").setTile(357, false);
		items[1782] = new Item("Rich Mysterious Couch").setTile(358, false);
		items[1783] = new Item("Rich Mahogany Couch").setTile(359, false);
		items[1784] = new Item("Rich Birch Couch").setTile(360, false);
		items[1785] = new Item("Rich Beech Couch").setTile(361, false);
		items[1786] = new Item("Rich Cherry Couch").setTile(362, false);
		items[1787] = new Item("Rich Bamboo Couch").setTile(363, false);
		items[1788] = new Item("Rich Palm Couch").setTile(364, false);
		items[1789] = new Item("Exotic Wooden Couch").setTile(365, false);
		items[1790] = new Item("Exotic Mystic Couch").setTile(366, false);
		items[1791] = new Item("Exotic Acacia Couch").setTile(367, false);
		items[1792] = new Item("Exotic Redwood Couch").setTile(368, false);
		items[1793] = new Item("Exotic Spruce Couch").setTile(369, false);
		items[1794] = new Item("Exotic Fir Couch").setTile(370, false);
		items[1795] = new Item("Exotic Larch Couch").setTile(371, false);
		items[1796] = new Item("Exotic Pine Couch").setTile(372, false);
		items[1797] = new Item("Exotic Yew Couch").setTile(373, false);
		items[1798] = new Item("Exotic Cedar Couch").setTile(374, false);
		items[1799] = new Item("Exotic Cypress Couch").setTile(375, false);
		items[1800] = new Item("Exotic Mysterious Couch").setTile(376, false);
		items[1801] = new Item("Exotic Mahogany Couch").setTile(377, false);
		items[1802] = new Item("Exotic Birch Couch").setTile(378, false);
		items[1803] = new Item("Exotic Beech Couch").setTile(379, false);
		items[1804] = new Item("Exotic Cherry Couch").setTile(380, false);
		items[1805] = new Item("Exotic Bamboo Couch").setTile(381, false);
		items[1806] = new Item("Exotic Palm Couch").setTile(382, false);
		items[1807] = new Item("Steampunk Chair").setTile(383, false);
		items[1808] = new Item("Steampunk Table").setTile(384, false);
		items[1809] = new Item("Steampunk Couch").setTile(385, false);
		items[1810] = new Item("Ornate Goblet");
	}

	public Item setMaxAngle(double d)
	{
		this.maxAngle = d;
		return this;
	}
	
	public Item setMaxStack(int i) {
		this.maxStack = i;
		return this;
	}

	public Item setShifter(int i) {
		shiftPower = i;
		return this;
	}

	public boolean useItem(EntityPlayer p, int posX, int posY) {
		// p.useItem = this;
		if (p.pos.distanceTo(new Vector2(posX, posY)) <= 3.4 + this.range || p.noClip) {
			if (placeTile > -1) {
				boolean b = Tile.tiles[placeTile].canPlaceTile(posX, posY,
						Main.world);
				Tile.tiles[placeTile].onPlaceTile(posX, posY, Main.world);
				return b;
			}
			if (this.id == 855) {
				Main.world.explode(posX, posY, 13.5);
				return true;
			}
			if (this.hammerPower > Tile.tiles[Main.world.world[posX][posY]].minPick
					&& this.pickPower == 0
					&& this.axePower == 0
					&& this.shiftPower == 0) {
				Main.world.slopeStyle[posX][posY]++;
				if (Main.world.slopeStyle[posX][posY] > 4) {
					Main.world.slopeStyle[posX][posY] = 0;
				}
				return true;
			}
			if (this.shiftPower > Tile.tiles[Main.world.world[posX][posY]].minPick
					&& this.pickPower == 0
					&& this.axePower == 0
					&& this.hammerPower == 0) {
				if (Main.world.world[posX][posY] != 0
						&& Main.world.foreground[posX][posY] <= 0) {
					Main.world.foreground[posX][posY] = Main.world.world[posX][posY];
					Main.world.world[posX][posY] = 0;
				}
				if (Main.world.foreground[posX][posY] != 0
						&& Main.world.wall[posX][posY] <= 0) {
					Main.world.wall[posX][posY] = Main.world.foreground[posX][posY];
					Main.world.foreground[posX][posY] = 0;
				}
				if (Main.world.wall[posX][posY] != 0
						&& Main.world.world[posX][posY] <= 0) {
					Main.world.world[posX][posY] = Main.world.wall[posX][posY];
					Main.world.wall[posX][posY] = 0;
				}
				return true;
			}
			//if (this.projectile != null) {
			//	ProjectileHandler.spawn(new EntityProjectile(this.projectile.name, p.pos.X, p.pos.Y, posX, posY).setGrav(this.projectile.gravity).setTexture(this.t));
			//}
			return true;
		}
		return false;
	}

	public static void newItem(int i, int posX, int posY, int amt) {
		float devX = World.rand.nextFloat() * 0.75f;
		float devY = World.rand.nextFloat() * 0.75f;
		if (World.rand.nextBoolean()) {
			devX *= -1;
		}
		if (World.rand.nextBoolean()) {
			devY *= -1;
		}
		DroppedItem.dropItems.add(new DroppedItem(Item.items[i], new Vector2(
				posX + devX, posY + devY), amt));
	}

	public static void newItem(Item i, int posX, int posY, int amt) {
		DroppedItem.dropItems.add(new DroppedItem(i, new Vector2(posX, posY),
				amt));
	}

	public Item setTile(int p, boolean b) {
		placeTile = p;
		tileSprite = b;
		consumable = true;
		return this;
	}

	public Item setOwner(int i) {
		owner = i;
		return this;
	}

	public void onEquip(EntityLiving e) {
	}

	public void onUnEquip(EntityLiving e) {
	}

	public void onUseItem(EntityLiving p, int x, int y, boolean leftSlot) {
	}

	public void onSecondary(EntityLiving p, int x, int y, boolean leftSlot) {
	}
}
