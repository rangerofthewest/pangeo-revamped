package pangeo.midi;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Track;

import pangeo.main.Util;

public class PlayTune 
{
	public static final int NOTE_ON = 0x90;
	public static final int NOTE_OFF = 0x80;
    public static final String[] NOTE_NAMES = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

  
    
	public static void playMidi(String path, int playingChannel, int speed)
	{
        Sequence sequence;
		try {
			URL url = PlayTune.class.getClass().getResource("/assets/midi/" + path + ".mid");
			File file = new File(url.getPath());
			sequence = MidiSystem.getSequence(file);
			Synthesizer synth = MidiSystem.getSynthesizer();
			synth.open();
			MidiChannel[] channels = synth.getChannels();
			int trackNumber = 0;
	        for (Track track :  sequence.getTracks()) {
	            trackNumber++;
	            Util.print("Track " + trackNumber + ": size = " + track.size());
	            for (int i=0; i < track.size(); i++) { 
	                MidiEvent event = track.get(i);
	                System.out.print("@" + event.getTick() + " ");
	                MidiMessage message = event.getMessage();
	                if (message instanceof ShortMessage) {
	                    ShortMessage sm = (ShortMessage) message;
	                    System.out.print("Channel: " + sm.getChannel() + " ");
	                    if (sm.getCommand() == NOTE_ON) {
	                        int key = sm.getData1();
	                        int octave = (key / 12)-1;
	                        int note = key % 12;
	                        String noteName = NOTE_NAMES[note];
	                        int velocity = sm.getData2();
	                        channels[playingChannel].noteOn(key, velocity);
	                        Util.print("Note on, " + noteName + octave + " key=" + key + " velocity: " + velocity);
	                    } else if (sm.getCommand() == NOTE_OFF) {
	                        int key = sm.getData1();
	                        int octave = (key / 12)-1;
	                        int note = key % 12;
	                        String noteName = NOTE_NAMES[note];
	                        int velocity = sm.getData2();
	                        channels[playingChannel].noteOff(key);
	                        Util.print("Note off, " + noteName + octave + " key=" + key + " velocity: " + velocity);
	                    } else {
	                        Util.print("Command:" + sm.getCommand());
	                    }
	    	            Thread.sleep(speed);
	                } else {
	                    Util.print("Other message: " + message.getClass());
	                }
	            }
	        }
	        synth.close();
		} catch (InvalidMidiDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}   
}
