package pangeo.mobs;

import pangeo.main.EntityLiving;
import pangeo.main.EntityProjectile;
import pangeo.main.Main;
import pangeo.main.ProjectileHandler;
import pangeo.main.Vector2;

public class MobLokiTurret extends EntityLiving 
{
	public boolean charge = false;
	public boolean targetPlayer = false;
	public Vector2 targetPos;
	public MobLokiTurret() 
	{
		super("Lokiite Turret");
		targetPos = new Vector2(0, 0);
	}

	public MobLokiTurret(int x, int y) 
	{
		super("Lokiite Turret", x, y);
		targetPos = new Vector2(x, y);
	}
	
	public void onUpdate()
	{
		super.onUpdate();
		if ((rand.nextInt(6) == 0) || (rand.nextBoolean() && this.health < this.maxHealth / 2))
		{
			if (rand.nextFloat() < 0.0009f)
			{
				targetPlayer = !targetPlayer;
			}
			if (targetPlayer)
			{
				ProjectileHandler.spawn(new EntityProjectile("LokiShot", pos.X, pos.Y, Main.player.pos.X, Main.player.pos.Y).setGrav(false).setTexture(Main.lokiShot));	
			}
			ProjectileHandler.spawn(new EntityProjectile("LokiShot", (int)pos.X, (int)pos.Y, rand.nextFloat() * 360).setGrav(false).setTexture(Main.lokiShot));
			for (int x = 0; x < (100 - this.health); x++)
			{
				if (rand.nextBoolean())
				{
					ProjectileHandler.spawn(new EntityProjectile("LokiShot", (int)pos.X, (int)pos.Y, rand.nextFloat() * 360).setGrav(false).setTexture(Main.lokiShot));
				}
				else
				{
					break;
				}
			}
		}
	}
}
