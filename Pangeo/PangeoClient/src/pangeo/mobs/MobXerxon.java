package pangeo.mobs;

import pangeo.main.EntityLiving;
import pangeo.main.Main;
import pangeo.main.MobHandler;
import pangeo.main.Vector2;
import pangeo.main.World;

public class MobXerxon extends EntityLiving 
{
	public boolean charge = false;
	public boolean clone = false;
	public int cloneSpawned = 0;
	
	public MobXerxon() 
	{
		super("Xerxon");
		this.maxHealth = 38000;
		this.health = 38000;
		//targetPos = new Vector2(0, 0);
	}

	public MobXerxon(int x, int y) 
	{
		super("Xerxon", x, y);
		this.maxHealth = 38000;
		this.health = 38000;
		//targetPos = new Vector2(x, y);
	}
	
	public MobXerxon setClone(boolean b)
	{
		clone = b;
		return this;
	}
	
	public void onUpdate()
	{
		super.onUpdate();
		if (this.maxHealth < 100)
		{
			setClone(true);
		}
		if (World.rand.nextInt(5) == 0)
		{
			charge = !charge;
		}
		if (charge)
		{
			pos.X += motionX;
			pos.Y += motionY;
			if (pos.X <= Main.player.pos.X)
			{
				motionX += 0.02;
			}
			if (pos.X >= Main.player.pos.X)
			{
				motionX -= 0.02;
			}
			if (pos.Y <= Main.player.pos.Y)
			{
				motionY += 0.02;
			}
			if (pos.Y >= Main.player.pos.Y)
			{
				motionY -= 0.02;
			}
		}
		else
		{
			if (World.rand.nextInt(970000) == 0)
			{
				pos = new Vector2(pos.X + (25 - World.rand.nextInt(50)), pos.Y + (25 - World.rand.nextInt(50)));
				charge = true;
			}
			else
			{
				if (pos.X <= Main.player.pos.X)
				{
					pos.X += moveSpeed * 0.5;
				}
				if (pos.X >= Main.player.pos.X)
				{
					pos.X -= moveSpeed * 0.5;
				}
				if (pos.Y <= Main.player.pos.Y)
				{
					pos.Y += moveSpeed * 0.5;
				}
				if (pos.Y >= Main.player.pos.Y)
				{
					pos.Y -= moveSpeed * 0.5;
				}
			}
		}
		if ((Main.player.pos.distanceTo(pos) < 1.5 || World.rand.nextInt() == 0))
		{
			pos = new Vector2(pos.X + (30 - World.rand.nextInt(60)), pos.Y + (30 - World.rand.nextInt(60)));
			charge = World.rand.nextBoolean();
		}
		if (rand.nextInt(1250) == 0 && !clone)
		{
			for (int x = 0; x < 10; x++)
			{
				if (rand.nextBoolean() && cloneSpawned < 600)
				{
					cloneSpawned++;
					MobHandler.spawnMob(new MobXerxon().setMaxHealth(World.rand.nextInt(20) + 10), (int)pos.X + (30 - World.rand.nextInt(60)), (int)pos.Y + (30 - World.rand.nextInt(60)));
				}
				else
				{
					break;
				}
			}
		}
	}
}
