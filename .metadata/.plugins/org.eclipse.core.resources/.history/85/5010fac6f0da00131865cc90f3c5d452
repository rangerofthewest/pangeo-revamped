package pangeo.main;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.Sys;

public class Recipe 
{
	public Tile craftingTable;
	public Item[] ingredients;
	public int[] ingredientAmt;
	public Item itemProduced;
	public int amtProduced;
	public int difficulty;
	
	public static List<Recipe> recipes = new ArrayList<Recipe>();
	
	public static void exportRecipes()
	{
		OutputStream file;			
		int amt = 0;
		try {
			file = new FileOutputStream("recipes.recipepkg");
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);
			//output.write(amt); //How many items to load
			for (int x = 0; x < recipes.size(); x++)
			{
				try
				{
					output.writeObject(recipes.get(x));
					Util.print("Recipe #" + x + " exported to recipe package");
					amt++;
				}
				catch (Exception e)
				{
					Sys.alert("Recipe Package", x + " recipes exported to recipe package");
					break;
				}
			}
			output.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sys.alert("Export", "Export complete! (" + amt + " exported)");
	}
	
	public Recipe(Tile t, Item[] i, int[] stack, Item pr, int aP, int d) 
	{
		craftingTable = t;
		ingredients = i;
		ingredientAmt = stack;
		itemProduced = pr;
		amtProduced = aP;
		difficulty = d;
		if (Main.player.survival)
		{
			for (int x = 0; x < ingredientAmt.length; x++)
			{
				ingredientAmt[x] *= 1.25; //25% more required
				if (ingredients[x] == Item.items[1042])
				{
					ingredientAmt[x] *= 1.15; //an additional 15% more wood required
				}
			}
			amtProduced *= 1.05; //5% more produced
		}
	}
	
	public static void init()
	{
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[506] }, new int[] { 3 }, Item.items[842], 1, 1));		
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[509] }, new int[] { 3 }, Item.items[853], 1, 1));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[516] }, new int[] { 3 }, Item.items[850], 1, 1));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[518] }, new int[] { 3 }, Item.items[848], 1, 1));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[845], Item.items[946] }, new int[] { 5, 1 }, Item.items[948], 5, 0));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[845], Item.items[947] }, new int[] { 5, 1 }, Item.items[948], 5, 0));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040] }, new int[] { 4 }, Item.items[1041], 1, 1));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[1443], Item.items[845] }, new int[] { 15, 8 }, Item.items[1043], 1, 0));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 33 }, Item.items[858], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 27 }, Item.items[950], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1042] }, new int[] { 30 }, Item.items[1050], 1, 1));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[1042] }, new int[] { 3 }, Item.items[845], 5, 0));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040], Item.items[845] }, new int[] { 30, 8 }, Item.items[860], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040], Item.items[845] }, new int[] { 27, 7 }, Item.items[952], 1, 1));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[1040], Item.items[845] }, new int[] { 29, 7 }, Item.items[1052], 1, 1));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[505], Item.items[506] }, new int[] { 1, 2 }, Item.items[511], 2, 2));
		recipes.add(new Recipe(Tile.tiles[123], new Item[] { Item.items[509], Item.items[1138] }, new int[] { 1, 4 }, Item.items[513], 2, 2));
		recipes.add(new Recipe(Tile.tiles[64], new Item[] { Item.items[845], Item.items[1042] }, new int[] { 5, 3 }, Item.items[1301], 3, 0));
		recipes.add(new Recipe(Tile.tiles[0], new Item[] { Item.items[548] }, new int[] { 3 }, Item.items[1391], 5, 1));
	}
	
	public boolean canCraft(EntityPlayer p)
	{
		if (Main.canInfiniCraft)
		{
			return true;
		}
		for (int x = 0; x < ingredients.length; x++)
		{
			if (p.inventory.hasItem(ingredients[x], ingredientAmt[x]))
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return Tile.isNear(p, craftingTable.address);
	}
	
	public void craft(EntityPlayer p)
	{
		if (canCraft(p))
		{
			for (int x = 0; x < ingredients.length; x++)
			{
				if (!Main.canInfiniCraft)
				{
					p.inventory.removeItem(ingredients[x], ingredientAmt[x]);
				}
			}
		}
		Item.newItem(itemProduced, (int)p.pos.X, (int)p.pos.Y, amtProduced);
	}
}
