package pangeo.main;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.Color;

public class Tile extends Item {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4790602064737031854L;

	public int amtDestroyed = 0;

	public boolean canBePowered = false;
	public boolean providesPower = false;
	public boolean isSpriteSheet = false;
	
	public static int tileAmt = 32000;
	public static Tile[] tiles = new Tile[tileAmt];

	public boolean solid = true;
	public boolean track = false;
	public boolean gravity = false;
	public boolean canAutoOrient = false;
	public boolean canSlope = true;
	public boolean pistonPushable = true;
	public boolean isDiscovered = false;
	public boolean chest = false;
	public boolean lightFlicker = false;
	public boolean solidTop = false;
	public boolean climbable = false;
	public boolean isBook = false;
	public boolean validTree = false;
	public String bookTitle, bookAuthor;
	public double minVoltage = 5.1;
	public short durability = 12; // 12% durability, punching does 1% damage
	public int id, address;
	public int minPick = 0;
	public int dropId = -1;
	public int xSize = 1;
	public int ySize = 1;
	public int tileOwner = -1;
	public int spriteId = -1;
	public int dropAmt = 1;
	public double lightIntensity = 0;
	public double lightResistance = 0;
	public org.newdawn.slick.Color lightColor = new Color(255, 255, 255, 255);
	
	public List<Texture> altSprites = new ArrayList<Texture>();
	public byte amtAltSprites = 0;
	
	public Texture t;

	public int ownsEntity;

	public static int baseAddr = -1;

	public String lore = "";

	//Animations
	public int animationLength = 1; //How many "frames" in between each individual animation frame
	public int animationFrames = 0; //How many frames in animation
	public List<List<Texture>> animationTextures = new ArrayList<List<Texture>>();
	
	public Tile setAnimation(int l, int f)
	{
		animationLength = l;
		animationFrames = f;
		return this;
	}
	
	public Tile setFlicker(boolean b) {
		lightFlicker = b;
		return this;
	}

	public Tile setClimbable(boolean b) {
		climbable = b;
		return this;
	}

	public Tile setAmtAltSprites(int i)
	{
		amtAltSprites = (byte)i;
		return this;
	}
	
	public Tile setValidTree(boolean b)
	{
		validTree = b;
		return this;
	}
	
	public Tile isSpriteSheet(boolean b)
	{
		isSpriteSheet = b;
		return this;
	}
	
	public Tile setDrop(int i, int d) {
		dropId = i;
		dropAmt = d;
		return this;
	}

	public Tile setMinPick(int p) {
		minPick = p;
		return this;
	}

	public Tile setSize(int x, int y) {
		xSize = x;
		ySize = y;
		if (xSize > 1 || ySize > 1) {
			canSlope = false;
		}
		return this;
	}

	public Tile setLightColor(org.newdawn.slick.Color c) {
		lightColor = c;
		// Sys.alert("New Color", c.r + "/" + c.g + "/" + c.b + "/" + c.a);
		return this;
	}

	public Tile setDurability(int i) {
		durability = (short) i;
		return this;
	}

	public Tile setLight(double d) {
		lightIntensity = d;
		return this;
	}

	public Tile setBook(boolean b) {
		isBook = b;
		return this;
	}

	public Tile setTitle(String s) {
		this.bookTitle = s;
		return this;
	}

	public Tile setAuthor(String s) {
		this.bookAuthor = s;
		return this;
	}

	public Tile setLore(String s) {
		lore = s;
		return this;
	}

	public Tile setMinVoltage(double d) {
		minVoltage = d;
		return this;
	}

	public Tile setGravity(boolean b) {
		gravity = b;
		return this;
	}

	public static boolean isNear(EntityPlayer p, int i) {
		for (int x = -5; x < 5; x++) {
			for (int y = -5; y < 5; y++) {
				if (p.pos.distanceTo(new Vector2(p.pos.X + x, p.pos.Y + y)) < 4.7) {
					if (Main.world.world[(int) p.pos.X + x][(int) p.pos.Y + y] == i
							|| (Tile.tiles[Main.world.world[(int) p.pos.X + x][(int) p.pos.Y
									+ y]].tileOwner == i && Tile.tiles[Main.world.world[(int) p.pos.X
									+ x][(int) p.pos.Y + y]].tileOwner != -1)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static int decTypes = 1;

	public static void init() {
		tiles[0] = new Tile("Air", 0).setSolid(false).setDurability(0);
		tiles[1] = new Tile("Dirt Block", 1).setDrop(1044, 1).setDurability(6).setValidTree(true);
		tiles[2] = new Tile("Stone Block", 2).setDrop(1040, 1).setDurability(18).setMinPick(4);
		tiles[3] = new Tile("Grass Block", 3).setDrop(1044, 1).setDurability(6).setValidTree(true);
		tiles[4] = new Tile("Copper Ore", 4).setDurability(20).setMinPick(6);
		tiles[5] = new Tile("Iron Ore", 5).setDurability(28).setMinPick(8);
		tiles[6] = new Tile("Silver Ore", 6).setDurability(36).setMinPick(8);
		tiles[7] = new Tile("Gold Ore", 7).setDurability(46).setMinPick(10);
		tiles[8] = new Tile("Ash Block", 8);
		tiles[9] = new Tile("Soot Block", 9);
		tiles[10] = new Tile("Piston", 10);
		tiles[11] = new Tile("PistonExtension", 11);
		tiles[12] = new Tile("Railtrack", 12);
		tiles[13] = new Tile("Laser", 13);
		tiles[14] = new Tile("Insulator", 14);
		tiles[15] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[16] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[17] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[18] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[19] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[20] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[21] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[22] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[23] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[24] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[25] = new Tile("Grass", 15).setSolid(false).setDurability(1);
		tiles[26] = new Tile("Decoration0", 16).setSolid(false)
				.setDurability(1);
		tiles[27] = new Tile("Decoration1", 16).setSolid(false)
				.setDurability(1);
		tiles[28] = new Tile("Decoration2", 16).setSolid(false).setDurability(1);
		tiles[29] = new Tile("Titanium Ore", 17);
		tiles[30] = new Tile("Sand", 18);
		tiles[31] = new Tile("Landmine", 19).setSolid(false);
		tiles[32] = new TilePot("Pot", 20).setSize(2, 2).setSolid(false);
		tiles[33] = new TilePot("Pot", 20).setSize(2, 2).setSolid(false);
		tiles[34] = new TilePot("Pot", 20).setSize(2, 2).setSolid(false).setLore("It appears to be infused with some unholy magic...");
		tiles[35] = new Tile("Coal", 21);
		tiles[36] = new Tile("Amethyst Brick", 22);
		tiles[37] = new Tile("Diamond Brick", 22);
		tiles[38] = new Tile("Emerald Brick", 22);
		tiles[39] = new Tile("Onyx Brick", 22);
		tiles[40] = new Tile("Opal Brick", 22);
		tiles[41] = new Tile("Ruby Brick", 22);
		tiles[42] = new Tile("Sapphire Brick", 22);
		tiles[43] = new Tile("Topaz Brick", 22);
		tiles[44] = new TilePlatform("Wooden Platform", 23).setDrop(1460, 1);
		tiles[45] = new Tile("Amethyst Brick Wall", 24);
		tiles[46] = new Tile("Diamond Brick Wall", 24);
		tiles[47] = new Tile("Emerald Brick Wall", 24);
		tiles[48] = new Tile("Onyx Brick Wall", 24);
		tiles[49] = new Tile("Opal Brick Wall", 24);
		tiles[50] = new Tile("Ruby Brick Wall", 24);
		tiles[51] = new Tile("Sapphire Brick Wall", 24);
		tiles[52] = new Tile("Topaz Brick Wall", 24);
		tiles[53] = new TilePlatform("Amethyst Platform", 25);
		tiles[54] = new TilePlatform("Diamond Platform", 25);
		tiles[55] = new TilePlatform("Emerald Platform", 25);
		tiles[56] = new TilePlatform("Onyx Platform", 25);
		tiles[57] = new TilePlatform("Opal Platform", 25);
		tiles[58] = new TilePlatform("Ruby Platform", 25);
		tiles[59] = new TilePlatform("Sapphire Platform", 25);
		tiles[60] = new TilePlatform("Topaz Platform", 25);
		tiles[61] = new TileDoor("Wooden Door", 26, 62, false).setLore("It's a door. Huh.");
		tiles[62] = new TileDoor("Wooden Door", 26, 61, true).setLore("It's a door. Huh.");
		tiles[63] = new Tile("Sign", 27).setLore("It's a sign. Huh.");
		tiles[64] = new Tile("Workbench", 28).setSize(2, 1).setLore("It's a desk. Huh.");
		tiles[65] = new TileBook("Open Book", 29, "The Art of Combat","Sir Christopher Knight");
		tiles[66] = new Tile("Inkwell", 30).setLore("A glass jar of ink");
		tiles[67] = new Tile("Naktiite Ore", 31).setLore("Pulsing with darkness...");
		tiles[68] = new Tile("Bed", 32).setSize(3, 1).setLore("A place to rest my weary head");
		tiles[69] = new Tile("Chair", 33).setLore("Doesn't look very comfortable...");
		tiles[70] = new TileBook("Open Book", 30, "The Might of Magic","Froobin Johannis");
		tiles[71] = new TileBook("Open Book", 31,"Adventures in the Bloodcrags", "Sinter Reish");
		tiles[72] = new TileBook("Open Book", 32,"Diaries of a Thief, Volume I", "Benito Akard");
		tiles[73] = new TileBook("Open Book", 33,"Diaries of a Thief, Volume II", "Benito Akard");
		tiles[74] = new TileBook("Open Book", 34,"Diaries of a Thief, Volume III", "Benito Akard");
		tiles[75] = new TileBook("Open Book", 35, "The Enigma Muse","Zepheniah Jones");
		tiles[76] = new TileBook("Open Book", 36,"Basic Training for Warriors", "Sir Mycroft Tamrin");
		tiles[77] = new TileBook("Open Book", 37, "Rudimentary Spellcraft",	"Whynted Shatner");
		tiles[78] = new TileBook("Open Book", 38, "Elementary Potioneering","Newton Findle");
		tiles[79] = new TileBook("Open Book", 39,"Ingredients in Basic Potions, Volume I", "M. Tahjarha");
		tiles[80] = new TileBook("Open Book", 40,"Ingredients in Basic Potions, Volume II", "M. Tahjarha");
		tiles[81] = new TileBook("Open Book", 41,"Ingredients in Basic Potions, Volume III", "M. Tahjarha");
		tiles[82] = new TileBook("Open Book", 42, "The Mahoon Bazaar","A. Sholin");
		tiles[83] = new TileDoor("Locked Wooden Door", 26, 62, false).setKey(Item.items[796]).setLore("It seems to be locked...");
		tiles[84] = new TileBook("Open Book", 43,"Survivor: Journies Through the Esundra", "K. Karakanakiva");
		tiles[85] = new TileBook("Open Book", 44, "A Summoner's Handbook","Newt Sjastra");
		tiles[86] = new Tile("Coal Brick", 45);
		tiles[87] = new Tile("Copper Brick", 45);
		tiles[88] = new Tile("Lead Brick", 45);
		tiles[89] = new Tile("Pyrite Brick", 45);
		tiles[90] = new Tile("Silicon Brick", 45);
		tiles[91] = new Tile("Silver Brick", 45);
		tiles[92] = new Tile("Titanium Brick", 45);
		tiles[93] = new Tile("Uranium Brick", 45);
		tiles[94] = new Tile("Gold Brick", 45);
		tiles[95] = new Tile("Iron Brick", 45);
		tiles[96] = new Tile("Coal Brick Wall", 46);
		tiles[97] = new Tile("Copper Brick Wall", 46);
		tiles[98] = new Tile("Lead Brick Wall", 46);
		tiles[99] = new Tile("Pyrite Brick Wall", 46);
		tiles[100] = new Tile("Silicon Brick Wall", 46);
		tiles[101] = new Tile("Silver Brick Wall", 46);
		tiles[102] = new Tile("Titanium Brick Wall", 46);
		tiles[103] = new Tile("Uranium Brick Wall", 46);
		tiles[104] = new Tile("Gold Brick Wall", 46);
		tiles[105] = new Tile("Iron Brick Wall", 46);
		tiles[106] = new TileBook("Open Book", 47,"Conversing with Wilderness", "H. Rand");
		tiles[107] = new Tile("Stone Brick", 48).setDrop(1041, 1);
		tiles[108] = new Tile("Stone Brick Wall", 49);
		tiles[109] = new Tile("Torch", 50).setLight(10.8).setSolid(false);
		tiles[110] = new Tile("Iron Bars", 51);
		tiles[111] = new TileBook("Dungeon Ledger", 52, "Book of Records", "");
		tiles[112] = new TileBook("Dungeon Ledger", 53, "Prison Records", "");
		tiles[113] = new Tile("Iron Anvil", 54);
		tiles[114] = new Tile("Item Rack", 55).setSolid(false);
		tiles[115] = new Tile("Snow", 56);
		tiles[116] = new Tile("Groomed Snow", 57);
		tiles[117] = new TileChest("Chest", 58);
		tiles[118] = new Tile("Copper Goblet", 59).setSolid(false).setDrop(842,1);
		tiles[119] = new Tile("Iron Goblet", 59).setSolid(false).setDrop(853, 1);
		tiles[120] = new Tile("Silver Goblet", 59).setSolid(false).setDrop(850,1);
		tiles[121] = new Tile("Gold Goblet", 59).setSolid(false).setDrop(848, 1);
		tiles[122] = new Tile("Platinum Goblet", 59).setSolid(false).setDrop(852, 1);
		tiles[123] = new Tile("Amalganator", 60).setSolid(false).setSize(3, 2).setDrop(856, 1).setLight(8.4).setLore("A glowing forge created specifically for the discovery of new metals");
		tiles[124] = new Tile("Metalforge", 61).setSolid(false).setSize(5, 5).setDrop(857, 1).setLight(14.4).setLore("A hot, fiery furnace. Even hotter than my last one!");
		tiles[125] = new Tile("Potion Bench", 62);
		tiles[126] = new Tile("Cauldron", 63);
		tiles[127] = new Tile("Tree Trunk", 64).setDrop(1042, 1).setSolid(false);
		tiles[128] = new Tile("Leaves", 65);
		tiles[129] = new Tile("Wooden Ladder", 66).setDrop(1301, 1).setClimbable(true).setSolid(false);
		tiles[130] = new Tile("Vine", 67).setClimbable(true);
		tiles[131] = new Tile("Bamboo", 68).setDrop(1441, 1);
		tiles[132] = new TilePlatform("Wooden Platform", 69).setDrop(1460, 1);
		tiles[133] = new TilePlatform("Wooden Platform", 69).setDrop(1461, 1);
		tiles[134] = new TilePlatform("Wooden Platform", 69).setDrop(1462, 1);
		tiles[135] = new TilePlatform("Wooden Platform", 69).setDrop(1463, 1);
		tiles[136] = new TilePlatform("Wooden Platform", 69).setDrop(1464, 1);
		tiles[137] = new TilePlatform("Wooden Platform", 69).setDrop(1465, 1);
		tiles[138] = new TilePlatform("Wooden Platform", 69).setDrop(1466, 1);
		tiles[139] = new TilePlatform("Wooden Platform", 69).setDrop(1467, 1);
		tiles[140] = new TilePlatform("Wooden Platform", 69).setDrop(1468, 1);
		tiles[141] = new TilePlatform("Wooden Platform", 69).setDrop(1469, 1);
		tiles[142] = new TilePlatform("Wooden Platform", 69).setDrop(1470, 1);
		tiles[143] = new TilePlatform("Wooden Platform", 69).setDrop(1471, 1);
		tiles[144] = new TilePlatform("Wooden Platform", 69).setDrop(1472, 1);
		tiles[145] = new TilePlatform("Wooden Platform", 69).setDrop(1473, 1);
		tiles[146] = new TilePlatform("Wooden Platform", 69).setDrop(1474, 1);
		tiles[147] = new TilePlatform("Wooden Platform", 69).setDrop(1475, 1);
		tiles[148] = new TilePlatform("Wooden Platform", 69).setDrop(1476, 1);
		tiles[149] = new TileBook("Book", 70, "Sink the Bismark", "Johnathan Harton");
		tiles[150] = new TileBook("Book", 71, "The Battle of New Orland","Johnathan Harton");
		tiles[151] = new TileBook("Book", 72, "The Northern Convoy","William French");
		tiles[152] = new TileBook("Book", 73, "Diaries of a Gambler","Kennedy Roberts");
		tiles[153] = new TileBook("Book", 74, "Anthology of a Rubber Duck","William French");
		tiles[154] = new TileBook("Book", 75, "Manneheim Spirit","William French");
		tiles[155] = new TileBook("Book", 76, "Diaries of a Highwayman","Kris Kirsten");
		tiles[156] = new TileBook("Book", 77, "I Saw Ghostriders in the Sky","Johnny Coin");
		tiles[157] = new TileCampfire("Campfire", 78, true).setLore("Crackling merrily").setSize(3, 2).setDrop(1593, 1);
		tiles[158] = new TileCampfire("Campfire", 78, false).setLore("Dead cold...").setSize(3, 2).setDrop(1593, 1);
		tiles[159] = new Tile("Steam Boiler", 79).setLore("It's hissing steam at me...").setSize(3, 2);
		tiles[160] = new TileTent("Basic Tent", 80).setLore("A basic tent").setSize(3, 1);
		tiles[161] = new Tile("Ice", 81);
		tiles[162] = new Tile("Snow", 81);
		tiles[163] = new Tile("Sandstone", 82);
		tiles[164] = new Tile("Steampunk Brick", 83).setLore("Ingrained with the machine's spirit").setAmtAltSprites(7).setDrop(1600, 1);
		tiles[165] = new Tile("Golden Steampunk Brick", 83).setLore("Ingrained with the machine's spirit").setAmtAltSprites(2).setDrop(1601, 1);
		tiles[166] = new TileDoor("Steampunk Door", 26, 167, false).setLore("It's a steampunky door. Huh.");
		tiles[167] = new TileDoor("Steampunk Door", 26, 166, true).setLore("It's a steampunky door. Huh.");
		tiles[168] = new TilePlatform("Mystic Platform", 23).setDrop(1461, 1);
		tiles[169] = new TilePlatform("Acacia Platform", 23).setDrop(1462, 1);
		tiles[170] = new TilePlatform("Redwood Platform", 23).setDrop(1463, 1);
		tiles[171] = new TilePlatform("Spruce Platform", 23).setDrop(1464, 1);
		tiles[172] = new TilePlatform("Fir Platform", 23).setDrop(1465, 1);
		tiles[173] = new TilePlatform("Larch Platform", 23).setDrop(1466, 1);
		tiles[174] = new TilePlatform("Pine Platform", 23).setDrop(1467, 1);
		tiles[175] = new TilePlatform("Yew Platform", 23).setDrop(1468, 1);
		tiles[176] = new TilePlatform("Cedar Platform", 23).setDrop(1469, 1);
		tiles[177] = new TilePlatform("Cypress Platform", 23).setDrop(1470, 1);
		tiles[178] = new TilePlatform("Mysterious Platform", 23).setDrop(1471, 1);
		tiles[179] = new TilePlatform("Mahogany Platform", 23).setDrop(1472, 1);
		tiles[180] = new TilePlatform("Birch Platform", 23).setDrop(1473, 1);
		tiles[181] = new TilePlatform("Beech Platform", 23).setDrop(1474, 1);
		tiles[182] = new TilePlatform("Cherry Platform", 23).setDrop(1475, 1);
		tiles[183] = new TilePlatform("Bamboo Platform", 23).setDrop(1476, 1);
		tiles[184] = new TilePlatform("Palm Platform", 23).setDrop(1605, 1);
		tiles[185] = new TilePlatform("Rich Wood Platform", 23).setDrop(1607, 1);
		tiles[186] = new TilePlatform("Rich Mystic Platform", 23).setDrop(1608, 1);
		tiles[187] = new TilePlatform("Rich Acacia Platform", 23).setDrop(1609, 1);
		tiles[188] = new TilePlatform("Rich Redwood Platform", 23).setDrop(1610, 1);
		tiles[189] = new TilePlatform("Rich Spruce Platform", 23).setDrop(1611, 1);
		tiles[190] = new TilePlatform("Rich Fir Platform", 23).setDrop(1612, 1);
		tiles[191] = new TilePlatform("Rich Larch Platform", 23).setDrop(1613, 1);
		tiles[192] = new TilePlatform("Rich Pine Platform", 23).setDrop(1614, 1);
		tiles[193] = new TilePlatform("Rich Yew Platform", 23).setDrop(1615, 1);
		tiles[194] = new TilePlatform("Rich Cedar Platform", 23).setDrop(1616, 1);
		tiles[195] = new TilePlatform("Rich Cypress Platform", 23).setDrop(1617, 1);
		tiles[196] = new TilePlatform("Rich Mysterious Platform", 23).setDrop(1618, 1);
		tiles[197] = new TilePlatform("Rich Mahogany Platform", 23).setDrop(1619, 1);
		tiles[198] = new TilePlatform("Rich Birch Platform", 23).setDrop(1620, 1);
		tiles[199] = new TilePlatform("Rich Beech Platform", 23).setDrop(1621, 1);
		tiles[200] = new TilePlatform("Rich Cherry Platform", 23).setDrop(1622, 1);
		tiles[201] = new TilePlatform("Rich Bamboo Platform", 23).setDrop(1623, 1);
		tiles[202] = new TilePlatform("Exotic Palm Platform", 23).setDrop(1624, 1);
		tiles[203] = new TilePlatform("Exotic Wood Platform", 23).setDrop(1625, 1);
		tiles[204] = new TilePlatform("Exotic Mystic Platform", 23).setDrop(1626, 1);
		tiles[205] = new TilePlatform("Exotic Acacia Platform", 23).setDrop(1627, 1);
		tiles[206] = new TilePlatform("Exotic Redwood Platform", 23).setDrop(1628, 1);
		tiles[207] = new TilePlatform("Exotic Spruce Platform", 23).setDrop(1629, 1);
		tiles[208] = new TilePlatform("Exotic Fir Platform", 23).setDrop(1630, 1);
		tiles[209] = new TilePlatform("Exotic Larch Platform", 23).setDrop(1631, 1);
		tiles[210] = new TilePlatform("Exotic Pine Platform", 23).setDrop(1632, 1);
		tiles[211] = new TilePlatform("Exotic Yew Platform", 23).setDrop(1633, 1);
		tiles[212] = new TilePlatform("Exotic Cedar Platform", 23).setDrop(1634, 1);
		tiles[213] = new TilePlatform("Exotic Cypress Platform", 23).setDrop(1635, 1);
		tiles[214] = new TilePlatform("Exotic Mysterious Platform", 23).setDrop(1636, 1);
		tiles[215] = new TilePlatform("Exotic Mahogany Platform", 23).setDrop(1637, 1);
		tiles[216] = new TilePlatform("Exotic Birch Platform", 23).setDrop(1638, 1);
		tiles[217] = new TilePlatform("Exotic Beech Platform", 23).setDrop(1639, 1);
		tiles[218] = new TilePlatform("Exotic Cherry Platform", 23).setDrop(1640, 1);
		tiles[219] = new TilePlatform("Exotic Bamboo Platform", 23).setDrop(1641, 1);
		tiles[220] = new TilePlatform("Exotic Palm Platform", 23).setDrop(1642, 1);
		tiles[221] = new Tile("Wood Chair", 84).setDrop(1643, 1);
		tiles[222] = new Tile("Mystic Chair", 84).setDrop(1644, 1);
		tiles[223] = new Tile("Acacia Chair", 84).setDrop(1645, 1);
		tiles[224] = new Tile("Redwood Chair", 84).setDrop(1646, 1);
		tiles[225] = new Tile("Spruce Chair", 84).setDrop(1647, 1);
		tiles[226] = new Tile("Fir Chair", 84).setDrop(1648, 1);
		tiles[227] = new Tile("Larch Chair", 84).setDrop(1649, 1);
		tiles[228] = new Tile("Pine Chair", 84).setDrop(1650, 1);
		tiles[229] = new Tile("Yew Chair", 84).setDrop(1651, 1);
		tiles[230] = new Tile("Cedar Chair", 84).setDrop(1652, 1);
		tiles[231] = new Tile("Cypress Chair", 84).setDrop(1653, 1);
		tiles[232] = new Tile("Mysterious Chair", 84).setDrop(1654, 1);
		tiles[233] = new Tile("Mahogany Chair", 84).setDrop(1655, 1);
		tiles[234] = new Tile("Birch Chair", 84).setDrop(1656, 1);
		tiles[235] = new Tile("Beech Chair", 84).setDrop(1657, 1);
		tiles[236] = new Tile("Cherry Chair", 84).setDrop(1658, 1);
		tiles[237] = new Tile("Bamboo Chair", 84).setDrop(1659, 1);
		tiles[238] = new Tile("Palm Chair", 84).setDrop(1660, 1);
		tiles[239] = new Tile("Rich Wood Chair", 84).setDrop(1661, 1);
		tiles[240] = new Tile("Rich Mystic Chair", 84).setDrop(1662, 1);
		tiles[241] = new Tile("Rich Acacia Chair", 84).setDrop(1663, 1);
		tiles[242] = new Tile("Rich Redwood Chair", 84).setDrop(1664, 1);
		tiles[243] = new Tile("Rich Spruce Chair", 84).setDrop(1665, 1);
		tiles[244] = new Tile("Rich Fir Chair", 84).setDrop(1666, 1);
		tiles[245] = new Tile("Rich Larch Chair", 84).setDrop(1667, 1);
		tiles[246] = new Tile("Rich Pine Chair", 84).setDrop(1668, 1);
		tiles[247] = new Tile("Rich Yew Chair", 84).setDrop(1669, 1);
		tiles[248] = new Tile("Rich Cedar Chair", 84).setDrop(1670, 1);
		tiles[249] = new Tile("Rich Cypress Chair", 84).setDrop(1671, 1);
		tiles[250] = new Tile("Rich Mysterious Chair", 84).setDrop(1672, 1);
		tiles[251] = new Tile("Rich Mahogany Chair", 84).setDrop(1673, 1);
		tiles[252] = new Tile("Rich Birch Chair", 84).setDrop(1674, 1);
		tiles[253] = new Tile("Rich Beech Chair", 84).setDrop(1675, 1);
		tiles[254] = new Tile("Rich Cherry Chair", 84).setDrop(1676, 1);
		tiles[255] = new Tile("Rich Bamboo Chair", 84).setDrop(1677, 1);
		tiles[256] = new Tile("Rich Palm Chair", 84).setDrop(1678, 1);
		tiles[257] = new Tile("Exotic Wood Chair", 84).setDrop(1661, 1);
		tiles[258] = new Tile("Exotic Mystic Chair", 84).setDrop(1662, 1);
		tiles[259] = new Tile("Exotic Acacia Chair", 84).setDrop(1663, 1);
		tiles[260] = new Tile("Exotic Redwood Chair", 84).setDrop(1664, 1);
		tiles[261] = new Tile("Exotic Spruce Chair", 84).setDrop(1665, 1);
		tiles[262] = new Tile("Exotic Fir Chair", 84).setDrop(1666, 1);
		tiles[263] = new Tile("Exotic Larch Chair", 84).setDrop(1667, 1);
		tiles[264] = new Tile("Exotic Pine Chair", 84).setDrop(1668, 1);
		tiles[265] = new Tile("Exotic Yew Chair", 84).setDrop(1669, 1);
		tiles[266] = new Tile("Exotic Cedar Chair", 84).setDrop(1670, 1);
		tiles[267] = new Tile("Exotic Cypress Chair", 84).setDrop(1671, 1);
		tiles[268] = new Tile("Exotic Mysterious Chair", 84).setDrop(1672, 1);
		tiles[269] = new Tile("Exotic Mahogany Chair", 84).setDrop(1673, 1);
		tiles[270] = new Tile("Exotic Birch Chair", 84).setDrop(1674, 1);
		tiles[271] = new Tile("Exotic Beech Chair", 84).setDrop(1675, 1);
		tiles[272] = new Tile("Exotic Cherry Chair", 84).setDrop(1676, 1);
		tiles[273] = new Tile("Exotic Bamboo Chair", 84).setDrop(1677, 1);
		tiles[274] = new Tile("Exotic Palm Chair", 84).setDrop(1678, 1);
		// tiles[60] = new Tile("Sand", 23).setGravity(true);
	}
	
	public Tile(String s, int i) {
		super(s);
		// TODO Auto-generated constructor stub
		id = i;
		// dropId = i;
		baseAddr++;
		address = baseAddr;
	}

	public Tile setDropId(int i) {
		dropId = i;
		return this;
	}

	public Tile setCanBePowered(boolean b) {
		canBePowered = b;
		return this;
	}

	public Tile setSolid(boolean b) {
		solid = b;
		return this;
	}

	public void onCollide(int x, int y) {
		if (id == 19) {
			Main.world.explode(x, y, World.rand.nextInt(5) + 4);
		}
	}

	public static EnumTileDirection directionToEnum(int dir) {
		switch (dir) {
		case 0:
			return EnumTileDirection.RIGHT;
		case 1:
			return EnumTileDirection.LEFT;
		case 2:
			return EnumTileDirection.DOWN;
		default:
			return EnumTileDirection.UP;
		}
	}

	public static boolean isPistonPushable(int x, int y, World w) {
		return Tile.tiles[w.world[x][y]].pistonPushable;
	}

	public void onPower(int x, int y, World w) {
		EnumTileDirection e = directionToEnum(w.direction[x][y]);

		// Piston!
		if (id == 10) {
			if (e == EnumTileDirection.RIGHT) {
				if (Tile.tiles[w.world[x + 1][y]].solid
						&& isPistonPushable(x + 1, y, w)
						&& w.world[x + 2][y] == 0) {
					w.world[x + 2][y] = w.world[x + 1][y];
					w.world[x + 1][y] = 11;
					w.direction[x + 1][y] = 0;
				}
			}
			if (e == EnumTileDirection.LEFT) {
				if (Tile.tiles[w.world[x - 1][y]].solid
						&& isPistonPushable(x - 1, y, w)
						&& w.world[x - 2][y] == 0) {
					w.world[x - 2][y] = w.world[x - 1][y];
					w.world[x - 1][y] = 11;
					w.direction[x - 1][y] = 1;
				}
			}
			if (e == EnumTileDirection.DOWN) {
				if (Tile.tiles[w.world[x][y - 1]].solid
						&& isPistonPushable(x, y - 1, w)
						&& w.world[x][y - 2] == 0) {
					w.world[x][y - 2] = w.world[x][y - 1];
					w.world[x][y - 1] = 11;
					w.direction[x][y - 1] = 2;
				}
			}
			if (e == EnumTileDirection.UP) {
				if (Tile.tiles[w.world[x][y + 1]].solid
						&& isPistonPushable(x, y + 1, w)
						&& w.world[x][y + 2] == 0) {
					w.world[x][y + 2] = w.world[x][y + 1];
					w.world[x][y + 1] = 11;
					w.direction[x][y + 1] = 3;
				}
			}
		}
		if (id == 13) {
			// Entity.addNewEntity(new EntityLaser)
		}
	}

	public void onUnpower(int x, int y, World w) {
		EnumTileDirection e = directionToEnum(w.direction[x][y]);

		// Piston!
		if (id == 10) {
			if (e == EnumTileDirection.RIGHT) {
				w.world[x + 1][y] = 0;
				w.direction[x + 1][y] = 0;
			}
			if (e == EnumTileDirection.LEFT) {
				w.world[x - 1][y] = 0;
				w.direction[x - 1][y] = 0;
			}
			if (e == EnumTileDirection.DOWN) {
				w.world[x][y + 1] = 0;
				w.direction[x][y + 1] = 0;
			}
			if (e == EnumTileDirection.UP) {
				w.world[x][y - 1] = 0;
				w.direction[x][y - 1] = 0;
			}
		}

	}

	public boolean canPlaceTile(int x, int y, World w) {
		for (int i = 0; i < xSize; i++) {
			for (int j = 0; j < ySize; j++) {
				if (w.world[x + i][y + j] == 0) {
					continue;
				} else {
					return false;
				}
			}
		}
		return true;
	}

	public void onPlaceTile(int x, int y, World w) {
		w.slopeStyle[x][y] = 0;
		if (canPlaceTile(x, y, w) || Main.godMode) {
			if (canBePowered) {
				if (Wire.isActive(x, y, w)) {
					onPower(x, y, w);
				}
			}
			if (providesPower) {
				Wire.triggerActive(x, y, w, 10);
				Wire.triggerActive(x + 1, y, w);
				Wire.triggerActive(x - 1, y, w);
				Wire.triggerActive(x, y + 1, w);
				Wire.triggerActive(x, y - 1, w);
			}

			if (address == 115) {
				w.metaId[x][y] = 1;
			}
			int selectedSprite = World.rand.nextInt(this.amtAltSprites + 1);
			for (int i = 0; i < xSize; i++) {
				for (int j = 0; j < ySize; j++) {
					w.world[x + i][y + j] = (short) this.address;
					w.xSlot[x + i][y + j] = (byte) i;
					w.ySlot[x + i][y + j] = (byte) j;
					w.altSprite[x + i][y + j] = (byte)selectedSprite;
					w.slopeStyle[x + i][y + j] = 0;
				}
			}
		}
	}

	public void onOverwriteTile(int x, int y, World w) {
		w.slopeStyle[x][y] = 0;
		if (true) {
			if (canBePowered) {
				if (Wire.isActive(x, y, w)) {
					onPower(x, y, w);
				}
			}
			if (providesPower) {
				Wire.triggerActive(x, y, w, 10);
				Wire.triggerActive(x + 1, y, w);
				Wire.triggerActive(x - 1, y, w);
				Wire.triggerActive(x, y + 1, w);
				Wire.triggerActive(x, y - 1, w);
			}

			if (address == 115) {
				w.metaId[x][y] = 1;
			}

			for (int i = 0; i < xSize; i++) {
				for (int j = 0; j < ySize; j++) {
					w.world[x + i][y + j] = (short) this.address;
					w.xSlot[x + i][y + j] = (byte) i;
					w.ySlot[x + i][y + j] = (byte) j;
					w.slopeStyle[x + i][y + j] = 0;
				}
			}
		}
	}

	public void onActivate(int x, int y, World w, EntityPlayer p) {

	}

	public void onKillWall(int x, int y, World w) {
		amtDestroyed++;
		if (dropId > -1) {
			Item.newItem(dropId, x, y, dropAmt);
		}
		Util.print(this.name + "(s) destroyed: " + amtDestroyed);
		isDiscovered = true;
		// This will destroy the entire tile if it is a multi-block tile by
		// resetting it to it's left most top most point
		// and systematically destroying it piece by piece
		w.wall[x][y] = -1;
	}

	public void onKillFore(int x, int y, World w) {
		amtDestroyed++;
		if (dropId > -1) {
			Item.newItem(dropId, x, y, dropAmt);
		}
		Util.print(this.name + "(s) destroyed: " + amtDestroyed);
		isDiscovered = true;
		// This will destroy the entire tile if it is a multi-block tile by
		// resetting it to it's left most top most point
		// and systematically destroying it piece by piece
		w.foreground[x][y] = -1;
	}

	public void onKillTile(int x, int y, World w) {
		amtDestroyed++;
		if (dropId > -1) {
			Item.newItem(dropId, x, y, dropAmt);
		}
		Util.print(this.name + "(s) destroyed: " + amtDestroyed);
		isDiscovered = true;
		if (providesPower) {
			Wire.triggerActive(x, y, w);
			Wire.triggerActive(x + 1, y, w);
			Wire.triggerActive(x - 1, y, w);
			Wire.triggerActive(x, y + 1, w);
			Wire.triggerActive(x, y - 1, w);
		}
		// This will destroy the entire tile if it is a multi-block tile by
		// resetting it to it's left most top most point
		// and systematically destroying it piece by piece
		for (int n = 0; n < this.xSize; n++) {
			for (int m = 0; m < this.ySize; m++) {
				w.world[(x - w.xSlot[x][y]) + n][(y - w.ySlot[x][y]) + m] = 0;
				w.slopeStyle[(x - w.xSlot[x][y]) + n][(y - w.ySlot[x][y]) + m] = 0;
				w.altSprite[x][y] = 0;
				w.animationFrame[x][y] = 0;
				// w.xSlot[(x - w.xSlot[x][y]) 1+ n][(y - w.ySlot[x][y]) + m] =
				// 0;
				// w.ySlot[(x - w.xSlot[x][y]) + n][(y - w.ySlot[x][y]) + m] =
				// 0;
			}
		}
		for (int n = 0; n < this.xSize; n++) {
			for (int m = 0; m < this.ySize; m++) {
				w.xSlot[(x - w.xSlot[x][y]) + n][(y - w.ySlot[x][y]) + m] = 0;
				w.ySlot[(x - w.xSlot[x][y]) + n][(y - w.ySlot[x][y]) + m] = 0;
			}
		}
	}
}
